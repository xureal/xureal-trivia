const { execSync } = require('child_process');
var zip = require('bestzip');

const envFile = process.argv[2] || '.env';
require('dotenv').config({ path: envFile });

const appVersion = process.env.APP_VERSION;
const envString = process.env.ENV_NAME;
const clientString = process.env.CLIENT_NAME;
const commitHash = execSync('git rev-parse --short HEAD').toString().trim();
const zipName = `${clientString}-trivia-${envString}-${appVersion}-${commitHash}`;
// const command = `bestzip ./builds/${zipName}.zip ../build/*`;

zip({
  source: 'build/*',
  destination: `./builds/${zipName}.zip`
})
  .then(function () {
    console.log('Zipped up ' + zipName + '.zip');
  })
  .catch(function (err) {
    console.error(err.stack);
    process.exit(1);
  });

// execSync(command, { stdio: 'inherit' });
