import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  nextButton: {
    width: theme.spacing(25),
    padding: theme.spacing(2),
    marginBottom: theme.spacing(4),
    background: 'white',
    '&:hover': {
      background: 'white'
    }
  }
}));

export default useStyles;
