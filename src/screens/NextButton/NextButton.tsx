import { Button, Grid } from '@mui/material';
import { Text } from 'common';
import useStyles from './styles';
import { StepComponentProps } from 'hooks/useStep';

const NextButton: React.FC<{
  onClick: StepComponentProps['next'];
  children?: React.ReactNode;
}> = ({ onClick, children }) => {
  const classes = useStyles();

  return (
    <Grid container justifyContent={'center'} alignItems={'center'}>
      <Grid item>
        <Button onClick={onClick} className={classes.nextButton}>
          <Text type={'info'} weight={'bold'} size={16} color={'dark'}>
            {children}
          </Text>
        </Button>
      </Grid>
    </Grid>
  );
};

export default NextButton;
