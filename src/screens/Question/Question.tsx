import React, { useCallback, useEffect, useState } from 'react';
import { StepComponentProps } from 'hooks/useStep';
import useSelector from 'store/useSelector';
import { getCurrentTopic } from 'store/selectors';
import { Grid, useMediaQuery, useTheme } from '@mui/material';
import Timer from 'common/Timer';
import { useLocation } from 'react-router-dom';
import Progress from 'common/ProgressBar';
import { Text } from 'common';
import useStyles from './styles';
import Button from '@mui/material/Button';
import ResultScreenContainer from 'screens/ResultScreen';
import { useQuiz } from 'store/useQuiz';
import { setWait, setProgressLevel, setCurrentMedals, setAnsweredQuestions } from 'store/actions';
import { getMedalArray, getNextMedalFromString } from 'utils/medal';
import { useSFX } from 'hooks';

type QuestionProps = StepComponentProps;

const Question: React.FC<QuestionProps> = ({ prev, next }) => {
  const urlParams = new URLSearchParams(window.location.search);
  const test = urlParams.get('test');
  const TEST_ENV = process.env.REACT_APP_TEST_ENV || 'false';

  const { dispatch, quizContainer, state } = useQuiz();
  const { progress, answerCalculated, answerCorrect, currentQuestion, answeredQuestions, medals } = state;

  const currentTopic = useSelector(getCurrentTopic);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const xurealId = searchParams.get('pid');
  const [answeredId, setAnsweredId] = useState('');
  const [play, setPlay] = useState(true);
  const [timesUp, setTimesUp] = useState(false);

  const quizPolicies = quizContainer.getPolicies();
  const repo = quizContainer.getRepo();
  const progressNeededToEarnMedal = quizPolicies.getProgressRequiredToEarMedal();
  const isBonusRound = currentQuestion.bonus;

  const classes = useStyles({ currentColor: currentTopic.questionBg });
  const answers = currentQuestion?.answers;

  const theme = useTheme();
  const isXs = useMediaQuery(theme.breakpoints.down('xs'));

  const matchesMobile = useMediaQuery('(max-width:400px)');
  const matchesXLMobile = useMediaQuery('(max-width:512px)');
  const matchesTabletLandscape = useMediaQuery('(max-width:1280px)');

  const getTitleFontSize = () => {
    if (matchesMobile) return 14;
    if (matchesXLMobile) return 18;
    if (matchesTabletLandscape) return 24;
    return 32;
  };

  const getAnswerBGColor = (hasAnswered: boolean, answerID: string, isCorrect: boolean) => {
    if (hasAnswered) {
      if (answerID === answeredId) {
        return isCorrect ? '#47C683!important' : '#FB543F!important';
      }
    } else {
      if (TEST_ENV === 'true' && test) {
        return isCorrect ? '#47C683!important' : 'white';
      }
    }

    return 'white';
  };

  const { playAtlas, stopAllSounds, playQuestion } = useSFX();

  useEffect(() => {
    playQuestion();
  }, [playQuestion]);

  const handleTimerEnd = useCallback(async () => {
    if (!answerCalculated) {
      setTimesUp(true);
      stopAllSounds();
      playAtlas({ id: 'wrong' });
      setPlay(false);
      try {
        if (!isBonusRound) {
          await repo.postNewAnswer({
            xurealID: xurealId as string,
            triviaID: 'trivia-demo',
            topics: answeredQuestions,
            levels: [],
            points: 0,
            correctQuestions: [],
            timestamp: new Date().toDateString(),
            reset: true
          });
          dispatch(setProgressLevel(0));
          dispatch(setWait(true));
          localStorage.setItem('wait', new Date().toDateString());
        }
      } catch (error) {
        console.error('Tiner End error', error);
      } finally {
        dispatch({
          type: 'SET_ANSWER_CALCULATED',
          payload: {
            answerCalculated: true,
            answerCorrect: false
          }
        });
      }
    }
  }, [dispatch, repo, isBonusRound, playAtlas, stopAllSounds, answerCalculated, answeredQuestions, xurealId]);

  // The goal is to calculate everything right as the player answers the question, in order to divy points out as soon as possible
  const handleAnswerClick = useCallback(
    async (answerID: string) => {
      stopAllSounds();
      setAnsweredId(answerID);
      setPlay(false);
      const nextMedalAward = getNextMedalFromString(medals);

      // correct answer logic
      const correctlyAnswered = quizPolicies.answeredCorrect(answers, answerID);

      if (correctlyAnswered) playAtlas({ id: 'correct' });
      else playAtlas({ id: 'wrong' });

      dispatch({
        type: 'SET_ANSWER_CALCULATED',
        payload: {
          answerCalculated: true,
          answerCorrect: correctlyAnswered
        }
      });
      // end correct answer logic

      // medal earning logic
      const hasEarnedMedal = correctlyAnswered && progress + 1 === progressNeededToEarnMedal;

      if (hasEarnedMedal) {
        const medalAwardString = `${medals},${nextMedalAward?.toUpperCase()}`;

        dispatch(setCurrentMedals(medalAwardString));
      }
      // end medal earning logic

      // create a topic progress object to pass to API to store users progrees
      // do not store the question the user answers on bonus round, so it is easier to parse the user's actual progress
      const updatedTopicsProgress =
        correctlyAnswered && !isBonusRound
          ? answeredQuestions.map((topic) => {
              return topic.topicID === currentTopic.key
                ? {
                    ...topic,
                    progress: [...topic.progress, currentQuestion.id]
                  }
                : topic;
            })
          : answeredQuestions;

      dispatch(setAnsweredQuestions(updatedTopicsProgress));

      repo.postNewAnswer({
        xurealID: xurealId as string,
        triviaID: 'trivia-demo',
        topics: updatedTopicsProgress,
        levels: hasEarnedMedal ? getMedalArray(`${medals},${nextMedalAward?.toUpperCase()}`) : [],
        points: correctlyAnswered ? currentQuestion.miles : 0,
        correctQuestions: [],
        timestamp: new Date().toDateString(),
        reset: correctlyAnswered || isBonusRound ? false : true
      });

      if (correctlyAnswered && !isBonusRound) {
        dispatch(setProgressLevel(progress + 1));
      }
      if (correctlyAnswered && isBonusRound) {
        dispatch(setProgressLevel(0));
      }

      if (correctlyAnswered === false && !isBonusRound) {
        dispatch(setProgressLevel(0));
        localStorage.setItem('wait', new Date().toDateString());
        dispatch(setWait(true));
      }

      if (correctlyAnswered === false && isBonusRound) {
        dispatch(setProgressLevel(0));
      }
    },
    [
      dispatch,
      currentQuestion,
      answers,
      quizPolicies,
      currentTopic,
      repo,
      isBonusRound,
      progressNeededToEarnMedal,
      playAtlas,
      stopAllSounds,
      progress,
      answeredQuestions,
      xurealId,
      medals
    ]
  );

  return (
    <Grid
      container
      direction='column'
      justifyContent='space-between'
      alignItems='center'
      wrap='nowrap'
      sx={{ backgroundColor: '#ECECF3', minHeight: '100vh', width: '100vw' }}
    >
      <Grid style={{ height: '100%', width: '100%' }}>
        <Grid
          className={isBonusRound ? 'color-change' : ''}
          sx={{
            paddingTop: theme.spacing(3),
            width: '100%',
            background: isBonusRound ? 'inherit' : currentTopic.questionBg
          }}
        >
          <Grid
            container
            justifyContent={'space-around'}
            alignItems={'center'}
            direction={'column'}
            flexWrap={'nowrap'}
            sx={{
              position: 'relative',
              padding: '0 5% 5%',
              height: '360px',
              [theme.breakpoints.down('lg')]: {
                height: '340px',
                padding: '0 5% 12%'
              },
              [theme.breakpoints.down('md')]: {
                height: '400px'
              },
              [theme.breakpoints.down('sm')]: {
                padding: '0 5% 20%'
              },
              [theme.breakpoints.between(0, 390)]: {
                height: '320px'
              }
            }}
          >
            <Grid
              item
              sx={{
                marginBottom: '20pxh',
                [theme.breakpoints.down('md')]: {
                  marginBottom: '16px'
                }
              }}
            >
              <Timer play={play} onEnd={handleTimerEnd} />
            </Grid>
            <Grid item sx={{ textAlign: 'center', maxWidth: '1140px' }}>
              <Text type={isXs ? 'sm' : 'large'} component={'div'} style={{ marginBottom: '32px' }}>
                {state.progress < 4 ? `Question ${state.progress + 1}` : 'Bonus Question'}
              </Text>
              <Text size={getTitleFontSize()} weight='bold' component={'div'} xs={24} style={{ marginBottom: '32px' }}>
                {currentQuestion?.title}
              </Text>
            </Grid>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              viewBox='0 0 100 100'
              preserveAspectRatio='none'
              className={classes.rookedLine}
            >
              <polygon fill='#ECECF3' points='0,100 100,100 0,0' />
            </svg>
          </Grid>
        </Grid>
        <Grid
          container
          sx={{
            margin: theme.spacing(4, 'auto', 0),
            width: '100%',
            maxWidth: '85vw',
            height: '100%',
            maxHeight: '35vh',
            gap: '16px',
            [theme.breakpoints.down('sm')]: {
              margin: theme.spacing(1, 3, 0)
            },
            [theme.breakpoints.between(0, 390)]: {
              maxHeight: '30vh'
            }
          }}
          justifyContent={'center'}
          alignItems={'center'}
        >
          <Grid container justifyContent={'center'} alignItems={'center'} spacing={2}>
            {currentQuestion.answers?.map((answer) => {
              return (
                <Grid xs={6} lg={3} key={answer.id} container item justifyContent='center'>
                  <Button
                    sx={{
                      color: answeredId === answer.id || (answer.correct && answerCalculated) ? 'white' : 'black',
                      background: getAnswerBGColor(!!answeredId, answer.id, answer.correct),
                      padding: '16px 40px !important',
                      width: '100%',
                      maxWidth: '300px',
                      height: '100px',
                      fontSize: `18px!important`,
                      fontWeight: 'bold',
                      borderRadius: '20px!important',
                      boxShadow: '0 0 20px 0 rgba(101,101,101,0.5)',
                      pointerEvents: answeredId ? 'none' : 'auto',
                      '&:hover': {
                        background: `${currentTopic.questionBg} !important`,
                        color: 'white!important'
                      },
                      textTransform: 'none',
                      [theme.breakpoints.down('sm')]: {
                        padding: '18px 15px !important',
                        fontSize: `16px!important`,
                        borderRadius: '19px!important',
                        boxShadow: 'none'
                      },
                      [theme.breakpoints.between(0, 450)]: {
                        fontSize: `12px!important`,
                        height: '90px'
                      }
                    }}
                    onClick={() => handleAnswerClick(answer.id)}
                  >
                    {answer.text}
                  </Button>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <div style={{ width: 'fit-content', margin: '0 auto 7vh' }}>
          <Progress />
        </div>
        {answerCalculated && (
          <ResultScreenContainer
            back={prev}
            goFinalScreen={next}
            correct={answerCorrect}
            points={currentQuestion.miles}
            timesUp={timesUp}
            desc={currentQuestion.description}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default Question;
