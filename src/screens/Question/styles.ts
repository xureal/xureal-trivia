import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    backgroundColor: '#ECECF3',
    minHeight: '100vh',
    width: '100vw'
  },
  questionContainer: {
    paddingTop: theme.spacing(3),
    width: '100%'
  },
  titleContainer: {
    position: 'relative',
    padding: '0 5% 5%',
    height: '400px',
    [theme.breakpoints.down('lg')]: {
      padding: '0 5% 12%'
    },
    [theme.breakpoints.down('sm')]: {
      padding: '0 5% 20%'
    }
  },
  timer: {
    marginBottom: '20pxh',
    [theme.breakpoints.down('md')]: {
      marginBottom: '64px'
    },
    [theme.breakpoints.down('sm')]: {
      marginBottom: '16px'
    }
  },
  question: {},
  questionTitle: {},
  answered: {
    color: 'white!important'
  },
  correct: {
    background: '#47C683!important',
    color: 'white!important'
  },
  wrong: {
    background: '#FB543F!important'
  },

  rookedLine: {
    position: 'absolute',
    bottom: -1,
    width: '100%',
    height: '100px',
    [theme.breakpoints.down('xs')]: {
      height: '40px'
    }
  },
  disabled: {
    pointerEvents: 'none'
  },
  progress: {
    width: 'fit-content',
    margin: '0 auto 7vh',
    [theme.breakpoints.down('sm')]: {},
    [theme.breakpoints.down('xs')]: {}
  },
  answerContainer: {
    height: '30vh'
  },
  answers: {},
  answerItem: {},
  answerButton: {}
}));

export default useStyles;
