import CenteredContainer from 'common/CenteredContainer/CenteredContainer';
import Slider from 'react-slick';
import styled from 'styled-components';

export const StartIconScreenStyles = {
  Container: styled(CenteredContainer)`
    height: 100vh;
    width: 100vw;
    display: flex;
    justify-content: center;
    flex-direction: column;

    @media (max-width: 375px) {
      gap: 80px;
    }
  `,
  TriviaLogo: styled.img`
    width: auto;
    max-height: 600px;
    height: 50vh;
    margin: -60px 0;

    @media (max-width: 1180px) {
      height: 55vh;
    }

    @media (max-width: 1024px) and (max-height: 1366px) {
      height: 55vh;
      max-height: unset;
    }

    @media (max-width: 820px) {
      height: 55vh;
    }

    @media (max-width: 600px) {
      height: 420px;
      margin: -40px 0 -40px;
    }
    @media (max-width: 450px) {
      margin: -40px 0 -100px;
      height: 400px;
    }
    @media (max-width: 375px) {
      height: 360px;
    }
  `,

  Spacer: styled.div`
    height: 100%;
    width: 20px;
    min-height: 160px;
    max-height: 380px;
  `,

  Cards: styled(CenteredContainer)`
    flex-direction: row;
    height: 30vh;
    /* max-height: 280px; */
    width: 100%;
    max-width: 30vw;
    justify-content: center;

    @media (max-width: 1024px) {
      height: 30vh;
      max-height: 480px;
    }
    @media (max-width: 820px) {
      max-height: 300px;
      width: 80%;
    }
    /* @media (max-width: 600px) {
      height: 400px;
      width: 40%;
    } */
    @media (max-width: 600px) {
      height: 400px;
      width: 100%;
    }
    @media (max-width: 450px) {
      height: 65vw;
    }
  `,
  Card: styled.img`
    width: 100%;
    height: auto;
    max-height: 100%;
    object-fit: contain;

    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -o-user-select: none;
    user-select: none;
    pointer-events: none;
    @media (max-width: 600px) {
      width: 200px;
      height: 300px;
    }
    /* @media (max-width: 820px) {
      width: 80%;
    }
    @media (max-width: 600px) {
      max-height: 320px;
      max-width: 300px;
    } */
  `,
  StyledSlider: styled(Slider)`
    height: 100%;
    width: 100%;
    .slick-dots li button:before {
      font-size: 10px;
      line-height: 24px;
    }
  `,
  SliderContainer: styled.div`
    height: 100%;
    width: 80vw;
  `,
  MobileCardContainer: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: auto;
  `,
  PlayButtonWrapper: styled(CenteredContainer)`
    margin-top: 60px;
    height: 100%;
    max-height: 180px;

    @media (max-width: 1366px) and (max-height: 1024px) {
      margin-top: 80px;
    }

    @media (max-width: 1180px) {
      margin-top: 20px;
    }

    @media (max-width: 1024px) and (max-height: 1366px) {
      margin-top: 60px;
    }

    @media (max-width: 1024px) and (max-height: 768px) {
      margin-top: 20px;
    }

    @media (max-width: 820px) {
      margin-top: 20px;
    }

    @media (max-width: 820px) and (max-height: 1180px) {
      margin-top: 80px;
    }

    @media (max-width: 820px) and (max-height: 1080px) {
      margin-top: 20px;
    }

    @media (max-width: 768px) {
      margin-top: 20px;
    }
    @media (max-width: 450px) {
      margin-top: 40px;
    }
  `,
  PlayButton: styled.img`
    cursor: pointer;
    width: 160px;
    height: auto;

    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -o-user-select: none;
    user-select: none;

    @media (max-width: 600px) {
      width: 140px;
    }
    @media (max-width: 375px) {
      width: 120px;
    }
  `
};
