import React, { useEffect } from 'react';
import { StepComponentProps } from 'hooks/useStep';

import useMediaQuery from '@mui/material/useMediaQuery';

import Instruction1 from 'assets/instructions-1.png';
import Instruction2 from 'assets/instructions-2.png';
import Instruction3 from 'assets/instructions-3.png';

import PlayButton from 'assets/play-blue.png';
import SlantedBackground from 'core/StandardBackground';
import TriviaLogo from 'assets/images/staticlogo.png';

import BackButton from 'common/BackButton';
import ClearData from 'utils/ClearData';
import { StartIconScreenStyles } from './styles';
import { useSFX } from 'hooks';

const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false
};

type startSpecProps = {
  loading: boolean;
};

type startScreenProps = StepComponentProps & startSpecProps;

export const StartScreen = ({ prev, next, getStep, loading }: startScreenProps) => {
  const { playIntro } = useSFX();
  const isBelowMediumWidth = useMediaQuery('(max-width:600px)');
  const isBelowXSWidth = useMediaQuery('(max-width:375px)');

  const handleStart = () => {
    next();
  };
  const currentStep = getStep();

  useEffect(() => {
    playIntro();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderCards = () => (
    <>
      {isBelowMediumWidth ? (
        <StartIconScreenStyles.SliderContainer>
          <StartIconScreenStyles.StyledSlider {...sliderSettings}>
            <div>
              <StartIconScreenStyles.MobileCardContainer>
                <StartIconScreenStyles.Card src={Instruction1} />
              </StartIconScreenStyles.MobileCardContainer>
            </div>
            <div>
              <StartIconScreenStyles.MobileCardContainer>
                <StartIconScreenStyles.Card src={Instruction2} />
              </StartIconScreenStyles.MobileCardContainer>
            </div>
            <div>
              <StartIconScreenStyles.MobileCardContainer>
                <StartIconScreenStyles.Card src={Instruction3} />
              </StartIconScreenStyles.MobileCardContainer>
            </div>
          </StartIconScreenStyles.StyledSlider>
        </StartIconScreenStyles.SliderContainer>
      ) : (
        <>
          <StartIconScreenStyles.Card src={Instruction1} />
          <StartIconScreenStyles.Card src={Instruction2} />
          <StartIconScreenStyles.Card src={Instruction3} />
        </>
      )}
    </>
  );

  return (
    <StartIconScreenStyles.Container>
      <SlantedBackground slantHeight={-150} />
      <StartIconScreenStyles.TriviaLogo src={TriviaLogo} />

      {isBelowXSWidth ? <></> : <StartIconScreenStyles.Cards>{renderCards()}</StartIconScreenStyles.Cards>}

      <StartIconScreenStyles.PlayButtonWrapper>
        {loading ? 'Loading...' : <StartIconScreenStyles.PlayButton onClick={handleStart} src={PlayButton} />}
      </StartIconScreenStyles.PlayButtonWrapper>
      {currentStep > 0 && <BackButton back={prev} />}
      <ClearData />
    </StartIconScreenStyles.Container>
  );
};
