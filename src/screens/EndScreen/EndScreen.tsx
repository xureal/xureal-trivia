import React, { useCallback } from 'react';
import { Grid } from '@mui/material';
import useStyles from './styles';
import { Conditional, Img, Text } from 'common';
import gold from 'assets/gold-active.png';
import silver from 'assets/silver-active.png';
import bronze from 'assets/bronze-active.png';
import checkMarkIcon from 'assets/checkmark.png';
import inactiveGold from 'assets/inactiveMedals/gold-inactive.png';
import inactiveSilver from 'assets/inactiveMedals/silver-inactive.png';
import inactiveBronze from 'assets/inactiveMedals/bronze-inactive.png';
import { useQuiz } from 'store/useQuiz';
// import useSelector from "store/useSelector";
// import { getWait } from "store/selectors";
import DateUntil from 'common/DateUntil';
import { StepComponentProps } from 'hooks/useStep';
import ClearData from 'utils/ClearData';
import { getMedalFromString } from 'utils/medal';

const medalMap = new Map([
  [
    true,
    [
      { icon: bronze, height: 220, xsHeight: 112, checkmark: true },
      { icon: silver, height: 220, xsHeight: 112, checkmark: true },
      { icon: gold, height: 220, xsHeight: 112, checkmark: true }
    ]
  ],
  [
    false,
    [
      { icon: inactiveBronze, height: 174, xsHeight: 96, checkmark: false },
      { icon: inactiveSilver, height: 174, xsHeight: 96, checkmark: false },
      { icon: inactiveGold, height: 174, xsHeight: 96, checkmark: false }
    ]
  ]
]);

const EndScreen: React.FC<StepComponentProps> = ({ goTo }) => {
  const classes = useStyles();
  // const wait = useSelector(getWait)
  const { state } = useQuiz();

  const medal = getMedalFromString(state.medals);
  const place = medal === 'Gold' ? 3 : medal === 'Silver' ? 2 : medal === 'Bronze' ? 1 : 0;
  const handleGoBack = useCallback(() => {
    goTo(3);
  }, [goTo]);

  return (
    <Grid container className={classes.root} justifyContent={'space-evenly'} alignItems={'center'} direction={'column'}>
      <Grid item className={classes.title}>
        <Conditional
          cond={medal === 'Gold'}
          onFalse={
            <Text size={36} weight={'bold'}>
              Try again tomorrow! Come back to go for the gold after the timer reaches zero!
            </Text>
          }
        >
          <Text size={40}>
            <b>CONGRATULATIONS!</b>
            <br />
            You have earned all the medals for the final round of Xureal Trivia.
            <br />
            Thank you for playing!
          </Text>
        </Conditional>
      </Grid>
      <Grid item className={classes.medals}>
        <Grid container direction={'row'} justifyContent={'center'} alignItems={'center'} spacing={4}>
          {Array(3)
            .fill(0)
            .map((medal, index) => {
              const active = place > index;
              const medalMapProps = medalMap.get(active)![index];
              return (
                <Grid item key={index} className={classes.medalContainer}>
                  <Img
                    src={medalMapProps.icon}
                    width={400}
                    height={medalMapProps.height}
                    xsWidth={216}
                    xsHeight={medalMapProps.xsHeight}
                  />
                  <Conditional cond={medalMapProps.checkmark}>
                    <Img src={checkMarkIcon} className={classes.checkmark} height={60} width={60} size={72} xs={56} />
                  </Conditional>
                </Grid>
              );
            })}
        </Grid>
      </Grid>
      <Grid item className={classes.timerContainer}>
        <DateUntil goBack={handleGoBack} />
      </Grid>
      {/* <BackButton back={() => goTo(3)} /> */}
      <Grid item>
        <ClearData />
      </Grid>
    </Grid>
  );
};

export default EndScreen;
