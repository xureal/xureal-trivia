import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import BlackBackground from 'assets/images/Loading.png';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: '100vh',
    backgroundImage: `url(${BlackBackground})`,
    backgroundSize: '100vw 100vh'
  },
  title: {
    textAlign: 'center',
    padding: '0 5%',
    maxWidth: '1000px'
  },
  medals: {},
  medalContainer: {
    position: 'relative'
  },
  checkmark: {
    position: 'absolute',
    top: theme.spacing(4),
    right: theme.spacing(2),
    [theme.breakpoints.down('xs')]: {
      top: theme.spacing(3),
      right: theme.spacing(0)
    }
  },
  timerContainer: {},
  timer: {
    marginLeft: theme.spacing(4)
  }
}));

export default useStyles;
