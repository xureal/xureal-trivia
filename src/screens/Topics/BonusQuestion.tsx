import { Img } from 'common/Image';
import BonusRoundHeaderImage from 'assets/bonus-question-header.png';
import BonusRoundSunburstImage from 'assets/shine-isolated.png';

interface Props {
  bonusQualified: boolean;
  style?: React.CSSProperties;
}

const BonusQuestion = ({ bonusQualified, style }: Props) => {
  if (!bonusQualified) {
    return <div style={{ height: '1px', width: '1px' }}></div>;
  }

  return (
    <div style={style}>
      <Img
        src={BonusRoundHeaderImage}
        style={{
          maxWidth: '350px',
          minWidth: '135px',
          width: '28vw'
        }}
      />
      <Img
        src={BonusRoundSunburstImage}
        style={{
          position: 'absolute',
          maxWidth: '350px',
          zIndex: -1,
          top: -40
        }}
        className='spin-image'
      />
    </div>
  );
};

export default BonusQuestion;
