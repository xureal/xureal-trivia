import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import CenteredContainer from 'common/CenteredContainer/CenteredContainer';
import styled from 'styled-components';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: '90vh',
    margin: '32px 0',
    [theme.breakpoints.down('md')]: {
      transform: 'translateY(-5vh) scale(1)'
    },
    [theme.breakpoints.down('sm')]: {
      transform: 'translateY(10vh) scale(.83)',
      marginTop: '6vh'
    },
    [theme.breakpoints.down('xs')]: {
      transform: 'translateY(-5vh)',
      marginTop: '4vh'
    }
  },
  roulette: {
    transform: 'translateY(-25vh)',
    [theme.breakpoints.down('md')]: {
      transform: 'translateY(-20vh)'
    },
    [theme.breakpoints.down('sm')]: {
      transform: 'translateY(-24vh)',
      marginTop: '6vh'
    },
    [theme.breakpoints.down(400)]: {
      marginTop: '4vh'
    }
  },
  bonus: {
    transform: 'translateY(-15vh)',
    [theme.breakpoints.down('lg')]: {
      transform: 'translateY(-10vh)'
    },
    [theme.breakpoints.down('md')]: {
      transform: 'translateY(-20vh)'
    },
    [theme.breakpoints.down('sm')]: {
      transform: 'translateY(-10vh)',
      marginTop: '6vh'
    },
    [theme.breakpoints.down(400)]: {
      marginTop: '4vh'
    }
  },
  text: {
    marginBottom: '16px'
  }
}));

export const StyledTopics = {
  Root: styled(CenteredContainer)`
    flex-direction: column;
    justify-content: space-around;
  `,
  RouletteContainer: styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    max-width: 1200px;
    margin-bottom: 128px;
    gap: 36px;
    @media (max-width: 1400px) {
    }
    @media (max-width: 1080px) {
    }
    @media (max-width: 820px) {
    }
    @media (max-width: 600px) {
    }
    @media (max-width: 490px) {
      margin-bottom: 32px;
    }
    @media (max-width: 320px) {
    }
  `
};

export default useStyles;
