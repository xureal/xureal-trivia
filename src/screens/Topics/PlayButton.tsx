import PlayButtonImage from 'assets/play-button.png';
import styled from 'styled-components';

interface Props {
  onPlay: () => void;
  playing: boolean;
  wait: boolean;
}

const StyledButtonImage = styled.img<{ $disabled: boolean }>`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 184px;
  height: 184px;
  cursor: ${(props) => (props.$disabled ? 'not-allowed' : 'pointer')};
  pointer-events: ${(props) => (props.$disabled ? 'none' : 'auto')};
  z-index: 9999999999;
  @media (max-width: 1440px) {
    width: 172px;
    height: 172px;
  }
  @media (max-width: 1080px) {
    width: 132px;
    height: 132px;
  }
  @media (max-width: 820px) {
    width: 192px;
    height: 192px;
  }
  @media (max-width: 600px) {
  }
  @media (max-width: 490px) {
    width: 136px;
    height: 136px;
  }
  @media (max-width: 420px) {
    width: 100px;
    height: 100px;
  }
  @media (max-width: 320px) {
  }
`;

const StyledButtonBackup = styled.div<{ $disabled: boolean }>`
  position: relative;
  height: 144px;
  width: 144px;
  background-color: #333;
  border-radius: 50%;
  border: 2px solid #fff;
  cursor: ${(props) => (props.$disabled ? 'not-allowed' : 'pointer')};
  pointer-events: ${(props) => (props.$disabled ? 'none' : 'auto')};
  div {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    font-size: 24px;
    font-weight: 600;
    color: white;
  }

  @media (max-width: 1440px) {
    width: 136px;
    height: 136px;
  }
  @media (max-width: 1080px) {
    width: 102px;
    height: 102px;
  }
  @media (max-width: 820px) {
    width: 152px;
    height: 152px;
  }

  @media (max-width: 490px) {
    width: 106px;
    height: 106px;
    div {
      font-size: 20px;
    }
  }
  @media (max-width: 420px) {
    width: 72px;
    height: 72px;
    div {
      font-size: 16px;
    }
  }
`;

const PlayButton = ({ onPlay, playing, wait }: Props) => {
  return (
    <StyledButtonBackup onClick={onPlay} $disabled={playing || wait}>
      <div>Play!</div>
      <StyledButtonImage src={PlayButtonImage} $disabled={playing || wait} />
    </StyledButtonBackup>
  );
};

export default PlayButton;
