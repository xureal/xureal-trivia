import styled from 'styled-components';

export const StyledRoutette = {
  Container: styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width: 1440px) {
      width: 1030px;
    }
    @media (max-width: 1080px) {
      width: 700px;
    }
    @media (max-width: 820px) {
      width: 550px;
    }
    @media (max-width: 600px) {
      width: 400px;
    }
    @media (max-width: 490px) {
      width: 300px;
    }
    @media (max-width: 320px) {
      width: 270px;
    }
  `,
  RouletteWrapperImage: styled.div<{ $image: string }>`
    background-image: url(${(props) => props.$image});
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 1350px;
    height: 240px;
    padding: 22px 42px 22px 42px;
    @media (max-width: 1440px) {
      height: 176px;
      width: 1030px;
    }
    @media (max-width: 1080px) {
      height: 112px;
      width: 700px;
    }
    @media (max-width: 820px) {
      height: 480px;
      width: 550px;
    }
    @media (max-width: 600px) {
      width: 400px;
    }
    @media (max-width: 490px) {
      padding: 22px 0 22px 0;
      height: 346px;
      width: 300px;
    }
    @media (max-width: 420px) {
      padding: 22px 0 22px 0;
      width: 300px;
    }
    @media (max-width: 320px) {
      width: 270px;
    }
  `,

  RouletteSquareWrapper: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    gap: 0 6px;
    flex-wrap: nowrap;
    padding-left: 4px;
    width: 1320px;

    @media (max-width: 1080px) {
      gap: 0 4px;
    }
    @media (max-width: 820px) {
      padding-left: 0;
      gap: 6px;
      flex-wrap: wrap;
      max-width: 460px;
    }
    @media (max-width: 600px) {
      gap: 4px;
    }
  `,

  TopicSquare: styled.div<{
    $wait: boolean;
    $playing: boolean;
  }>`
    ${({ $wait, $playing }) => `opacity: ${$wait || $playing ? 0.3 : 1};`}

    width: 220px;
    height: 216px;
    @media (max-width: 1440px) {
      width: 168px;
      height: 168px;
    }
    @media (max-width: 1080px) {
      width: 118px;
      height: 118px;
    }
    @media (max-width: 820px) {
      width: 184px;
      height: 162px;
    }
    @media (max-width: 600px) {
      width: 184px;
      height: 162px;
    }
    @media (max-width: 490px) {
      width: 136px;
      height: 116px;
    }
    @media (max-width: 320px) {
      width: 120px;
      height: 106px;
    }
  `,
  TopicTextContainer: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
  `,
  TopicSquareText: styled.div`
    text-align: center;
    font-size: 18px;
    color: white;
    font-weight: 500;
    padding: 16px;
    @media (max-width: 1080px) {
      font-size: 16px;
    }
    @media (max-width: 820px) {
      font-size: 20px;
    }

    @media (max-width: 490px) {
      font-size: 18px;
    }
    @media (max-width: 320px) {
      font-size: 16px;
    }
  `,

  TopicSquareWrapper: styled.div<{ $image: string }>`
    width: 100%;
    height: 100%;

    background-image: url(${(props) => props.$image});
    background-size: cover;
    background-position: center;
  `,

  TopicName: styled.div`
    z-index: 1;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    transform: translatey(55%);
    max-height: 100px;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    @media (max-width: 1280px) {
      transform: translatey(45%);
    }
    @media (max-width: 960px) {
      transform: translatey(25%);
    }
    @media (max-width: 600px) {
      transform: translatey(35%);
    }
    @media (max-width: 400px) {
      transform: translatey(15%);
    }
  `,

  PlayButtonPositioner: styled.div`
    position: absolute;
    bottom: -25%;
    left: 50%;
    transform: translateX(-50%);
    z-index: 999999;
    @media (max-width: 1440px) {
      bottom: -80px;
    }
    @media (max-width: 1080px) {
      bottom: -80px;
    }
    @media (max-width: 820px) {
      bottom: -25%;
    }
    @media (max-width: 600px) {
    }
    @media (max-width: 490px) {
      bottom: -25%;
    }
    @media (max-width: 420px) {
      bottom: -15%;
    }
    @media (max-width: 320px) {
      bottom: -10%;
    }
  `
};
