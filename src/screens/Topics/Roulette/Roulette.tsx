import React, { memo } from 'react';
import { Topic } from 'core/types';
import { useMediaQuery } from '@mui/material';
import { StyledRoutette } from './styles';
import useSelector from 'store/useSelector';
import { getWait } from 'store/selectors';
// import { Howler } from "howler";

import bgImage from 'assets/bar-bg.png';
import bgImageMobile from 'assets/spinner-mobile-bg.png';

interface RouletteProps {
  topics: Topic[];
  playing: boolean;
  selectedTopicId: string;
  children?: React.ReactNode;
}

const Roulette: React.FC<RouletteProps> = ({ topics, selectedTopicId, playing, children }) => {
  const wait = useSelector(getWait);

  const matches = useMediaQuery('(max-width:820px)');

  return (
    <StyledRoutette.Container>
      <StyledRoutette.RouletteWrapperImage $image={matches ? bgImageMobile : bgImage}>
        <StyledRoutette.RouletteSquareWrapper>
          {topics.map((topic, index) => {
            return (
              <StyledRoutette.TopicSquare
                key={'roulette-square' + index}
                $wait={wait}
                $playing={playing && selectedTopicId !== topic.id}
              >
                <StyledRoutette.TopicSquareWrapper $image={matches ? topic.bgImageXs : topic.bgImage}>
                  <StyledRoutette.TopicTextContainer>
                    <StyledRoutette.TopicSquareText>{topic.name}</StyledRoutette.TopicSquareText>
                  </StyledRoutette.TopicTextContainer>
                </StyledRoutette.TopicSquareWrapper>
              </StyledRoutette.TopicSquare>
            );
          })}
        </StyledRoutette.RouletteSquareWrapper>
      </StyledRoutette.RouletteWrapperImage>
      <StyledRoutette.PlayButtonPositioner>{children}</StyledRoutette.PlayButtonPositioner>
    </StyledRoutette.Container>
  );
};

export const MemoizedRoulette = memo(Roulette);
