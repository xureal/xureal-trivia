import { useCallback, useState } from 'react';
import { Grid } from '@mui/material';
import useSelector from 'store/useSelector';
import { getTopics, getWait } from 'store/selectors';
import { StepComponentProps } from 'hooks/useStep';
import { Text } from 'common';
import { StyledTopics } from './styles';
import Progress from 'common/ProgressBar';
import { useQuiz } from 'store/useQuiz';
// import BackButton from "../BackButton";
import DateUntil from 'common/DateUntil';

import SlantedBackground from 'core/StandardBackground';
import BonusQuestion from './BonusQuestion';
import PlayButton from './PlayButton';
import sleep from 'utils/sleep';
import { getRandomNumberInclusive } from 'utils/random';
import { useSFX } from 'hooks';
import { getNextMedalFromString, parseMedalString } from 'utils/medal';
import { MemoizedRoulette } from './Roulette/Roulette';

export const Topics = ({ next, prev }: StepComponentProps) => {
  // const DEV_MODE = process.env.NODE_ENV === "development";
  const { state, getNewQuestion } = useQuiz();
  const { playAtlas, stopAllSounds } = useSFX();

  const [playing, setPlaying] = useState(false);
  const [selectedTopicId, setSelectedTopicId] = useState('');

  const topics = useSelector(getTopics);
  const wait = useSelector(getWait);
  const bonus = state.progress >= 4;
  const currentMedal = parseMedalString(state.medals);
  const nextMedalLevel = getNextMedalFromString(currentMedal);

  const onPressPlayButton = useCallback(async () => {
    // used in random number generator to indicate cycles to spin
    const ITERATION_COUNT_MAX = 22;

    setPlaying(true);
    stopAllSounds();
    playAtlas({ id: 'spin' });

    // filters out topics that are finished or empty
    let availableTopics = topics.filter((topic) => {
      const emptyArrayCheck =
        topic.questions[nextMedalLevel].filter((question) => question.answeredCorrect === undefined).length > 0;
      return !topic.finished && emptyArrayCheck;
    });

    const topicLength = availableTopics.length;
    if (topicLength === 0) availableTopics = topics;

    const iterationCount = getRandomNumberInclusive(ITERATION_COUNT_MAX - topicLength, ITERATION_COUNT_MAX);

    for (let i = 0; i < iterationCount; i++) {
      const sleepTime = 35 + i * i;
      await sleep(sleepTime);
      const index = i >= topicLength ? i % topicLength : i;
      let localTopicId = availableTopics.find((_, topicIndex) => topicIndex === index)?.id!;

      setSelectedTopicId(localTopicId);

      if (i === iterationCount - 1) {
        playAtlas({ id: 'spinStop' });
        getNewQuestion(availableTopics.find((topic) => topic.id === localTopicId)!);
        setTimeout(() => next(), 1500);
      }
    }
  }, [topics, setPlaying, nextMedalLevel, playAtlas, stopAllSounds, getNewQuestion, next]);

  if (wait) {
    return (
      <StyledTopics.Root>
        <Grid container item direction={'column'} alignItems={'center'} justifyContent={'center'} className='fade-in'>
          <Text type={'large'}>Next topic will be available in</Text>
          <DateUntil goBack={prev} />
        </Grid>
      </StyledTopics.Root>
    );
  }

  return (
    <>
      <SlantedBackground slantHeight={-320} />

      <StyledTopics.Root>
        <BonusQuestion bonusQualified={bonus} />
        <StyledTopics.RouletteContainer>
          <Text type={'large'} style={{ textAlign: 'center' }}>
            Hit `Play` to select a topic
          </Text>

          <MemoizedRoulette topics={topics} selectedTopicId={selectedTopicId} playing={playing}>
            <PlayButton playing={playing} onPlay={onPressPlayButton} wait={wait} />
          </MemoizedRoulette>
        </StyledTopics.RouletteContainer>
        <Progress />
      </StyledTopics.Root>
    </>
  );
};
