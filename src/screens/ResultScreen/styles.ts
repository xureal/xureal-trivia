import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface UseStylesProps {
  background: string;
  medalTopPadding?: number;
}

const useStyles = ({ background, medalTopPadding }: UseStylesProps) =>
  makeStyles((theme: Theme) => ({
    root: {
      position: 'relative'
    },
    image: {
      position: 'absolute',
      top: '0',
      left: '50%',
      transform: `translate(-50% , -50%)`,
      zIndex: 9999,
      width: '130px',
      height: 'auto',
      [theme.breakpoints.down('sm')]: {
        width: '96px'
      }
    },
    medalImage: {
      position: 'absolute',
      top: '0',
      left: '50%',
      zIndex: 9999,
      transform: `translate(-50% , -25%)`,
      width: '338px',
      height: 'auto',
      [theme.breakpoints.down('sm')]: {
        width: '241px'
      }
    },
    containerRoot: {
      zIndex: 99,
      position: 'fixed',
      top: '50%',
      left: '50%',
      background,
      transform: `translate(-50% , -50%)`,
      borderRadius: theme.spacing(2),
      padding: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        width: '90vw'
      }
    },
    container: {
      color: 'white',
      textAlign: 'center',
      background: 'linear-gradient(to bottom, #2D2D34, #0D0D0F)',
      borderRadius: theme.spacing(2),
      padding: theme.spacing(medalTopPadding || 10, 10, 0, 10),
      [theme.breakpoints.down('md')]: {
        padding: theme.spacing(medalTopPadding || 10, 3, 0, 3)
      }
    },
    title: {
      marginBottom: theme.spacing(3)
    },
    line: {
      height: 1,
      width: '100%',
      background: '#5A5A5B',
      margin: theme.spacing(5, 'auto')
    },
    collectButton: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(9),
      borderRadius: theme.spacing(4),
      background: 'white',
      padding: theme.spacing(2, 10),
      color: '#2B9CD8',
      '&:hover': {
        background: 'grey',
        color: 'white'
      }
    }
  }));

export const useContentStyles = makeStyles((theme: Theme) => ({
  title: {
    marginBottom: theme.spacing(3)
  },
  line: {
    height: 1,
    width: '100%',
    background: '#5A5A5B',
    margin: theme.spacing(5, 'auto')
  },
  collectButton: {
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(9),
    borderRadius: theme.spacing(4),
    background: 'white',
    padding: theme.spacing(2, 10),
    color: '#2B9CD8',
    '&:hover': {
      background: 'grey',
      color: 'white'
    },
    fontSize: 20,
    fontWeight: 'bold'
  },
  miles: {
    marginTop: theme.spacing(6)
  }
}));

export const useTourStyles = makeStyles((theme: Theme) => ({
  container: {
    marginTop: theme.spacing(5)
  },
  tourMilesContainer: {
    borderRadius: theme.spacing(5),
    background: 'white',
    padding: theme.spacing(0.5),
    width: theme.spacing(29),
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down('sm')]: {
      marginBottom: 'theme.spacing(5)'
    }
  },
  tourMiles: {
    background: 'black',
    borderRadius: theme.spacing(5),
    padding: theme.spacing(2, 3)
  },
  tourMilesIconContainer: {
    marginRight: theme.spacing(4)
  },
  tourMilesIcon: {
    width: '80px!important',
    height: '40px!important'
  },
  collectButtonContainer: {
    padding: theme.spacing(0.5),
    borderRadius: theme.spacing(4),
    background: 'white',
    marginBottom: theme.spacing(8),
    '&:hover': {
      background: '#1F69FF'
    },
    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(6)
    }
  },
  collectText: {
    marginBottom: '32px',
    [theme.breakpoints.down('sm')]: {
      marginBottom: '0'
    }
  },
  collectButton: {
    background: 'linear-gradient(#5FD1A1, #2FA05E)',
    borderRadius: theme.spacing(4),
    width: '100%',
    color: 'white',
    padding: theme.spacing(2, 11),
    '&:hover': {
      '& span ': {
        color: 'white!important'
      },
      background: '#1F69FF'
    }
  }
}));

export default useStyles;
