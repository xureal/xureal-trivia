import { Medal } from '../../core/types';

export interface ResultScreenProps {
  onCollect: () => void;
  miles: number;
  desc?: string;
}

export interface MedalScreenProps extends ResultScreenProps {
  text: string;
  medal: Medal | string;
}
