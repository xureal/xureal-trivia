import { Button, Grid, useTheme } from '@mui/material';
import { Text } from 'common';
import { useContentStyles } from './styles';
// import Miles from "./Miles";

interface TimesUpScreenProps {
  onContinue: () => void;
  desc: string;
}

const TimesUpScreen: React.FC<TimesUpScreenProps> = ({ onContinue, desc }) => {
  const classes = useContentStyles();
  const theme = useTheme();

  return (
    <>
      <Grid item className={classes.title}>
        <Text type={'xLarge'} weight={'bold'}>
          TIME'S UP...
        </Text>
      </Grid>
      <Grid item>
        <Text type={'info'}>{desc}</Text>
      </Grid>
      <Grid item></Grid>
      <Grid item>
        <Button
          className={classes.collectButton}
          onClick={onContinue}
          sx={{
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(9),
            borderRadius: theme.spacing(4),
            background: 'white',
            padding: theme.spacing(2, 10),
            color: '#2B9CD8',
            '&:hover': {
              background: 'grey',
              color: 'white'
            },
            fontSize: 20,
            fontWeight: 'bold'
          }}
        >
          Continue
        </Button>
      </Grid>
    </>
  );
};

export default TimesUpScreen;
