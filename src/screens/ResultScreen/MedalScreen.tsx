import { Grid } from '@mui/material';
import TourMiles from './TourMiles';
import { Text } from 'common';
import { MedalScreenProps } from './types';

const MedalScreen: React.FC<MedalScreenProps> = ({ text, medal, onCollect, miles }) => {
  return (
    <>
      <Grid item>
        <Text type={'info'}>YOU'VE EARNED A</Text>
      </Grid>
      <Grid item>
        <Text type={'large'} weight={'bold'} textTransform={'uppercase'} size={48} xs={35}>
          {`${medal} Medal!`}
        </Text>
        <br />
        <Text xs={15}>Keep playing to earn a Gold Medal!</Text>
      </Grid>
      <Grid item>
        <TourMiles text={text} miles={miles} onCollect={onCollect} />
      </Grid>
    </>
  );
};

export default MedalScreen;
