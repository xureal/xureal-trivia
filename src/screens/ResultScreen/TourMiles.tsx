import { Button, Grid, useTheme } from '@mui/material';
import { Text } from 'common';
import { ResultScreenProps } from './types';

interface TourMilesProps extends ResultScreenProps {
  text: string;
  miles: number;
}

const TourMiles: React.FC<TourMilesProps> = ({ text, miles, onCollect }) => {
  const theme = useTheme();

  return (
    <Grid
      container
      direction={'column'}
      alignItems={'center'}
      sx={{
        marginTop: theme.spacing(5)
      }}
    >
      <Grid
        item
        sx={{
          marginBottom: '32px',
          [theme.breakpoints.down('sm')]: {
            marginBottom: '0'
          }
        }}
      >
        <Text type={'info'} xs={15}>
          YOU'VE EARNED {miles} XP!
        </Text>
      </Grid>
      <Grid
        item
        sx={{
          padding: theme.spacing(0.5),
          borderRadius: theme.spacing(4),
          background: 'white',
          marginBottom: theme.spacing(8),
          '&:hover': {
            background: '#1F69FF'
          },
          [theme.breakpoints.down('sm')]: {
            marginBottom: theme.spacing(6)
          }
        }}
      >
        <Button
          onClick={onCollect}
          sx={{
            background: 'linear-gradient(#5FD1A1, #2FA05E)',
            borderRadius: theme.spacing(4),
            width: '100%',
            color: 'white',
            padding: theme.spacing(2, 11),
            '&:hover': {
              '& span ': {
                color: 'white!important'
              },
              background: '#1F69FF'
            }
          }}
        >
          <Text type={'large'} weight={'bold'} size={20}>
            Collect!
          </Text>
        </Button>
      </Grid>
    </Grid>
  );
};

export default TourMiles;
