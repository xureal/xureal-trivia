import { Button, Grid, useTheme } from '@mui/material';
import { Text } from 'common';

interface IncorrectScreenProps {
  onContinue: () => void;
  desc: string;
}

const IncorrectScreen: React.FC<IncorrectScreenProps> = ({ onContinue, desc }) => {
  const theme = useTheme();

  return (
    <>
      <Grid
        item
        sx={{
          marginBottom: theme.spacing(3)
        }}
      >
        <Text type={'xLarge'} weight={'bold'}>
          INCORRECT...
        </Text>
      </Grid>
      <Grid item>
        <Text type={'info'}>{desc}</Text>
      </Grid>
      <Grid item>
        <Button
          onClick={onContinue}
          sx={{
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(9),
            borderRadius: theme.spacing(4),
            background: 'white',
            padding: theme.spacing(2, 10),
            color: '#2B9CD8',
            '&:hover': {
              background: 'grey',
              color: 'white'
            },
            fontSize: 20,
            fontWeight: 'bold'
          }}
        >
          Continue
        </Button>
      </Grid>
    </>
  );
};

export default IncorrectScreen;
