import React, { useCallback, useState } from 'react';
import useStyles from './styles';
import { Grid, useMediaQuery, useTheme } from '@mui/material';
import { Conditional, Img, Overlay } from 'common';
import Gold from 'assets/gold-shine.png';
import Bronze from 'assets/bronze-shine.png';
import Silver from 'assets/silver-shine.png';
import correct from 'assets/checkmark.png';
import incorrect from 'assets/incorrect.png';
import bronzeMedalBg from 'assets/bronze-solid-border.png';
import silverMedalBg from 'assets/silver-solid-border.png';
import goldMedalBg from 'assets/gold-solid-border.png';
import IncorrectScreen from './IncorrectScreen';
import CorrectScreen from './CorrectScreen';
import MedalScreen from './MedalScreen';
import { useQuiz } from 'store/useQuiz';
import TimesUpScreen from './TimesUpScreen';
import { useSFX } from 'hooks';
import { isSafari } from 'react-device-detect';

import BonusRoundVideo from 'assets/bonus-question.webm';
import BonusRoundHeaderImage from 'assets/bonus-question-header.png';
import BonusRoundSunburstImage from 'assets/shine-isolated.png';
import { getMedalFromString } from 'utils/medal';

interface ResultScreenProps {
  correct: boolean;
  timesUp: boolean;
  back: () => void;
  points: number;
  desc: string;
  goFinalScreen: () => void;
}

const iconMap: any = {
  Bronze: {
    medal: Bronze,
    bg: bronzeMedalBg
  },
  Silver: {
    medal: Silver,
    bg: silverMedalBg
  },
  Gold: {
    medal: Gold,
    bg: goldMedalBg
  }
};

const medalTextMap: any = {
  Bronze: 'Keep playing to earn a Silver Medal',
  Silver: 'Keep playing to earn a Gold Medal',
  Gold: 'Great Job! You’ve collected all your medals!'
};

const correctMap: any = new Map([
  [true, '#46C683'],
  [false, '#F6603E']
]);
const correctIconMap: any = new Map([
  [true, correct],
  [false, incorrect]
]);

const ResultScreenContainer: React.FC<ResultScreenProps> = ({
  correct,
  back,
  points,
  goFinalScreen,
  timesUp,
  desc
}) => {
  const [showBonus, setShowBonus] = useState(false);
  const [hasCollected, setHasCollected] = useState(false);
  const { quizContainer, state, dispatch } = useQuiz();
  const { progress, currentQuestion } = state;
  const policies = quizContainer.getPolicies();
  const progressRequiredToEarnMedal = policies.getProgressRequiredToEarMedal();
  const { playAtlas } = useSFX();
  const theme = useTheme();
  const isSm = useMediaQuery(theme.breakpoints.down('sm'));
  const medal = state.medals;
  const currentMedal = getMedalFromString(medal);

  const isBonusRound = currentQuestion.bonus;

  const canObtainMedal = !isBonusRound && progress % progressRequiredToEarnMedal === 0;

  const medalScreen = correct && canObtainMedal;

  const color = medalScreen ? `url(${iconMap[currentMedal ?? 'Bronze'].bg})` : correctMap.get(correct)!;

  const icon = medalScreen ? iconMap[currentMedal ?? 'Bronze'].medal : correctIconMap.get(correct)!;

  const classes = useStyles({
    background: color,
    medalTopPadding: medalScreen ? (isSm ? 22 : 30) : 0
  })();

  // if the user has earned a gold medal
  // and is on the bonus round
  const winConditionAchieved = currentMedal === 'Gold' && isBonusRound;

  // this should just clear state and route player to bonus screen, back to Roulette, or to end screen
  const handleCollect = useCallback(() => {
    if (hasCollected) return;
    setHasCollected(true);
    playAtlas({ id: 'select' });

    if (winConditionAchieved) {
      dispatch({
        type: 'SET_ANSWER_CALCULATED',
        payload: {
          answerCalculated: false,
          answerCorrect: false
        }
      });

      goFinalScreen();
      return;
    }

    if (!correct && !isBonusRound) {
      dispatch({
        type: 'SET_ANSWER_CALCULATED',
        payload: {
          answerCalculated: false,
          answerCorrect: false
        }
      });

      goFinalScreen();
      return;
    }

    if (!correct && isBonusRound) {
      dispatch({
        type: 'SET_ANSWER_CALCULATED',
        payload: {
          answerCalculated: false,
          answerCorrect: false
        }
      });

      back();
      return;
    }

    if (correct && progress === progressRequiredToEarnMedal) {
      playAtlas({ id: 'bonus' });
      setShowBonus(true);
      setTimeout(() => {
        dispatch({
          type: 'SET_ANSWER_CALCULATED',
          payload: {
            answerCalculated: false,
            answerCorrect: false
          }
        });
        back();
      }, 2000);
    } else {
      dispatch({
        type: 'SET_ANSWER_CALCULATED',
        payload: {
          answerCalculated: false,
          answerCorrect: false
        }
      });
      back();
    }
  }, [
    back,
    goFinalScreen,
    isBonusRound,
    correct,
    progress,
    progressRequiredToEarnMedal,
    winConditionAchieved,
    playAtlas,
    dispatch,
    hasCollected
  ]);

  return (
    <>
      <Overlay />
      <div className={classes.containerRoot}>
        <Conditional
          cond={medalScreen}
          onFalse={
            <>
              {correct && (
                <Img
                  src={BonusRoundSunburstImage}
                  style={{
                    position: 'absolute',
                    width: '414px',
                    top: 0,
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    zIndex: 999
                  }}
                />
              )}
              <Img src={icon} className={classes.image} />
            </>
          }
        >
          <Img src={icon} className={classes.medalImage} />
        </Conditional>
        <Grid
          container
          className={classes.container}
          direction={'column'}
          alignItems={'center'}
          justifyContent={'space-around'}
          wrap='nowrap'
          sx={{
            color: 'white',
            textAlign: 'center',
            background: 'linear-gradient(to bottom, #2D2D34, #0D0D0F)',
            borderRadius: theme.spacing(2),
            padding: theme.spacing(medalScreen ? (isSm ? 22 : 30) : 0 || 10, 10, 0, 10),
            [theme.breakpoints.down('md')]: {
              padding: theme.spacing(medalScreen ? (isSm ? 22 : 30) : 0 || 10, 3, 0, 3)
            }
          }}
        >
          <Conditional cond={!correct && !timesUp}>
            <IncorrectScreen onContinue={handleCollect} desc={desc} />
          </Conditional>
          <Conditional cond={!correct && timesUp}>
            <TimesUpScreen onContinue={handleCollect} desc={desc} />
          </Conditional>
          <Conditional cond={correct}>
            <Conditional
              cond={medalScreen}
              onFalse={<CorrectScreen onCollect={handleCollect} desc={desc} miles={points} />}
            >
              <MedalScreen
                text={medalTextMap[medal]}
                medal={currentMedal}
                onCollect={handleCollect}
                miles={points}
                desc={desc}
              />
            </Conditional>
          </Conditional>
        </Grid>
      </div>
      {showBonus && (
        <>
          {isSafari && (
            <div
              style={{
                position: 'fixed',
                left: '50%',
                right: 0,
                top: '50%',
                transform: 'translate(-50%, -50%)'
              }}
            >
              <div
                style={{
                  position: 'relative',
                  left: '50%',
                  right: 0,
                  top: '50%',
                  transform: 'translate(-50%, -50%)'
                }}
              >
                <div
                  style={{
                    maxWidth: '900px',
                    minWidth: '280px',
                    width: '70%',
                    position: 'absolute',
                    zIndex: 99999,
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)'
                  }}
                >
                  <Img src={BonusRoundSunburstImage} style={{ width: '100%' }} className='spin-image' />
                </div>
                <div
                  style={{
                    maxWidth: '1000px',
                    minWidth: '280px',
                    width: '75%',
                    position: 'absolute',
                    zIndex: 999999,
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)'
                  }}
                >
                  <Img src={BonusRoundHeaderImage} style={{ width: '100%' }} className='bounce-in' />
                </div>
              </div>
            </div>
          )}
          {!isSafari && (
            <video
              style={{
                maxWidth: '1000px',
                minWidth: '280px',
                width: '80%',
                position: 'fixed',
                zIndex: 999999,
                left: '50%',
                right: 0,
                top: '50%',
                transform: 'translate(-50%, -50%)'
              }}
              src={BonusRoundVideo}
              id='player'
              muted
              autoPlay
              playsInline
            ></video>
          )}
        </>
      )}
    </>
  );
};

export default ResultScreenContainer;
