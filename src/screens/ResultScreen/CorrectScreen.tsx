import { useContentStyles } from './styles';
import { Grid, useTheme } from '@mui/material';
import { Text } from '../../common';
import TourMiles from './TourMiles';
import { ResultScreenProps } from './types';

const CorrectScreen: React.FC<ResultScreenProps> = ({ onCollect, miles, desc }) => {
  const classes = useContentStyles();
  const theme = useTheme();

  return (
    <>
      <Grid
        item
        className={classes.title}
        sx={{
          marginBottom: theme.spacing(3)
        }}
      >
        <Text type={'xLarge'} weight={'bold'} xs={30}>
          CORRECT!
        </Text>
      </Grid>
      <Grid item>
        <Text type={'info'} xs={15}>
          {desc}
        </Text>
      </Grid>
      <Grid item>
        <TourMiles text={''} miles={miles} onCollect={onCollect} />
      </Grid>
    </>
  );
};

export default CorrectScreen;
