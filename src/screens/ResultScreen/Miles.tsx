import { Grid } from '@mui/material';
import { Img, Text } from 'common';
import { useTourStyles } from './styles';
import tourMilesIcon from 'assets/tourmiles.png';

interface TourMilesProps {
  miles: number;
}

const Miles: React.FC<TourMilesProps> = ({ miles }) => {
  const classes = useTourStyles();

  return (
    <Grid item className={classes.tourMilesContainer}>
      <Grid container direction={'row'} alignItems={'center'} justifyContent={'center'} className={classes.tourMiles}>
        <Grid item className={classes.tourMilesIconContainer}>
          <Img src={tourMilesIcon} size={60} className={classes.tourMilesIcon} />
        </Grid>
        <Grid item>
          <Text type={'large'} weight={'bold'} size={32}>
            {miles}
          </Text>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Miles;
