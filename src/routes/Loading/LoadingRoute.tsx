import { useEffect, useState } from 'react';
import { Typography, LinearProgress } from '@mui/material';
import { useSFX } from 'hooks';

import TriviaLogoVideo from 'assets/tc-logo-final.webm';
import BlackBackground from 'assets/images/Loading.png';
import useStyles from './styles';
import { useHistory } from 'react-router-dom';

export const Loading = () => {
  const history = useHistory();
  const { playIntro } = useSFX();
  const classes = useStyles();
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    return () => {
      playIntro();
    };
  }, [playIntro]);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        if (oldProgress === 100) {
          return 100;
        }
        const diff = Math.random() * 55;
        return Math.min(oldProgress + diff, 100);
      });
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, []);

  useEffect(() => {
    if (progress === 100) {
      setTimeout(() => {
        history.push('/quiz');
      }, 1000);
    }
  }, [progress, history]);

  return (
    <div className={classes.root}>
      <div className={classes.main}>
        <div style={{ height: '100vh', width: '100vw', overflow: 'hidden' }}>
          <img alt='background' className={classes.backgroundImg} src={BlackBackground} />
          <div
            style={{
              display: 'flex',
              justifyContent: 'center'
            }}
          >
            <div
              style={{
                position: 'absolute',
                bottom: -128,
                height: '350px',
                width: '200vw',
                backgroundColor: '#E2E6FF',
                zIndex: 999,
                transform: 'skewY(4deg)'
              }}
            >
              <div className={classes.loadingContainer}>
                <>
                  <LinearProgress variant='determinate' className={classes.loadingBar} value={progress} />
                  <Typography variant='h5'>Writing quiz questions...</Typography>
                </>
              </div>
            </div>
          </div>
        </div>
        <div className={classes.logoContainer}>
          <video
            className={classes.videoLogo}
            src={TriviaLogoVideo}
            id='videoLogo'
            muted
            loop
            autoPlay
            playsInline
          ></video>
        </div>
      </div>
    </div>
  );
};
