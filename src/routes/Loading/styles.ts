import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import XfinityLoadingImage from 'assets/loading-image.png';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    height: '100vh',
    width: '100vw',
    backgroundColor: 'black'
  },
  main: {
    position: 'relative',
    width: '100vw',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    [theme.breakpoints.down('sm')]: {
      width: 'auto'
    }
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    top: 0,
    position: 'absolute',
    zIndex: 99,
    [theme.breakpoints.down('sm')]: {
      alignItems: 'center',
      height: '70vh'
    }
  },
  olympicsLogo: {
    zIndex: 99,
    maxWidth: '640px',
    width: '60%',
    margin: '0 auto 16px',
    [theme.breakpoints.down('md')]: {
      minWidth: '180px'
    },
    [theme.breakpoints.down('sm')]: {
      width: '60%'
    }
  },
  videoLogo: {
    width: '80%',
    maxWidth: '830px',
    maxHeight: '810px',
    zIndex: 99
  },
  backgroundImg: {
    height: '100%',
    width: '100vw'
  },
  rookedLine: {
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute',
    bottom: '19.5%',
    width: '100%',
    height: '100px',
    zIndex: 999,
    [theme.breakpoints.down('xs')]: {
      height: '40px'
    }
  },
  mobileContainer: {},
  xfinityMobileLogo: {
    zIndex: 99,
    maxWidth: '60%',
    margin: '0 auto',
    [theme.breakpoints.down('sm')]: {
      maxWidth: '300px',
      width: '215px'
    }
  },
  mobileXfinityContainer: {
    zIndex: 99,
    borderTop: '1px solid rgba(139,139,151,0.5)',
    paddingTop: '56px',
    maxWidth: '80%',
    [theme.breakpoints.down('sm')]: {
      margin: '56px auto'
    }
  },
  loadingContainer: {
    zIndex: 89,
    color: '#474D5D',
    width: '80vw',
    maxWidth: '430px',
    textAlign: 'center',
    marginTop: '70px',
    fontSize: '18px',
    margin: '0 auto',
    transform: 'skewY(-4deg)'
  },
  loadingBar: {
    border: '1px #000000 solid',
    zIndex: 99,
    margin: '16px auto',
    borderRadius: 5,
    backgroundColor: 'black',
    height: '24px',
    width: '100%',
    maxWidth: '481px',
    '& .MuiLinearProgress-barColorPrimary': {
      background: 'linear-gradient(#9A69FB, #6138F5, #3F26F6)',
      backgroundColor: 'none',
      borderRadius: '6px'
    }
  },
  desktopRightSide: {
    width: '50%',
    maxWidth: '836px'
  },
  desktopPhoneImage: {
    zIndex: 99,
    maxWidth: '820px',
    width: '100%',
    marginLeft: '-160px',
    [theme.breakpoints.down('lg')]: {
      marginLeft: '-100px'
    },
    [theme.breakpoints.down('md')]: {
      marginLeft: '-60px'
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: '-60px'
    }
  },
  bgImageContainer: {
    position: 'fixed',
    backgroundImage: `url(${XfinityLoadingImage})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'auto 110vh',
    height: '100%',
    width: '100%',
    zIndex: 0,
    right: '-40%',
    top: 0,
    [theme.breakpoints.down('lg')]: {
      right: '-33%'
    },
    [theme.breakpoints.down('md')]: {
      right: '-30%'
    }
  },
  bgImageRight: {
    position: 'fixed',
    backgroundColor: 'black',
    height: '100%',
    width: '100%',
    right: 0,
    top: 0,
    clipPath: 'polygon(55% 0, 100% 0%, 100% 100%, 45% 100%)',
    zIndex: 0,
    [theme.breakpoints.down('lg')]: {
      clipPath: 'polygon(58% 0, 100% 0%, 100% 100%, 48% 100%)'
    }
  }
}));

export default useStyles;
