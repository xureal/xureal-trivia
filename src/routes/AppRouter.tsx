import React from 'react';
import { Route, Switch } from 'react-router-dom';
// import { Loading } from "./Loading/LoadingRoute";
import { Quiz } from 'routes/Quiz/QuizRoute';

const AppRouter: React.FC = () => {
  return (
    <Switch>
      <Route path={'/'}>
        <Quiz />
      </Route>
    </Switch>
  );
};

export default AppRouter;
