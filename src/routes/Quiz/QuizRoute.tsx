import { useEffect, useMemo, useReducer, useState } from 'react';
import { useSFX, useStep } from 'hooks';
import reducer from 'store';
import { initialQuizData } from 'core/initialQuizData';
import { QuizCtx } from 'store/useQuiz';
import QuizContainerFactory from 'core/QuizContainerFactory';
import Repository from 'core/Repository';
import { quiz } from 'core/mockData';
import {
  setQuiz,
  setWait,
  setCurrentMedals,
  setAnsweredQuestions,
  setCurrentQuestion,
  setTimestamp,
  setCurrentTopic,
  setProgressLevel
} from 'store/actions';
import { useLocation } from 'react-router-dom';
import Policies from 'core/Policies';
import Specs from 'core/Specs';
import { Topic } from 'core/types';
import { StartScreen } from 'screens/Start/StartScreen';
import { Topics } from 'screens/Topics/Topics';
import Question from 'screens/Question';
import EndScreen from 'screens/EndScreen';

import * as dateUtils from 'utils/date';
import Analytics from 'core/Analytics';
import { CircularProgress, Grid } from '@mui/material';
import { getMedalFromString, getNextMedalFromString, parseMedalString } from 'utils/medal';
import { StepComponentProps } from 'hooks/useStep';

export const Quiz = () => {
  const DEV_MODE = process.env.NODE_ENV === 'development';
  const { playIntro } = useSFX();

  const api = window.foatOlympicsTriviaAPI;
  const spec = useMemo(() => new Specs(dateUtils), []);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const [isLoading, setLoading] = useState(true);
  const xurealId = searchParams.get('pid');

  // This is too cognitively complex
  // need to simplify process
  const quizContainer = useMemo(() => {
    return QuizContainerFactory.create(new Repository(), new Policies(spec), new Specs(dateUtils), new Analytics(api));
  }, [spec, api]);

  const repo = quizContainer.getRepo();
  const progressToEarnMedal = quizContainer.getPolicies().getProgressRequiredToEarMedal();

  const [state, dispatch] = useReducer(reducer, initialQuizData);
  const { progress, answeredQuestions } = state;
  const [init, setInit] = useState(false);
  const quizError = false;
  const [initialIndex, setInitialIndex] = useState(0);

  // useStep hook to render the correct component based on the componentMap passed in
  // this is causin all images to load slowly though
  const {
    StepComponent: RenderStepComponent,
    length: stepComponentLength,
    currentIndex: currentStepComponentIndex
  } = useStep(
    useMemo(
      () => [
        (props: StepComponentProps) => <StartScreen loading={isLoading} {...props} />,
        (props: StepComponentProps) => <Topics {...props} />,
        (props: StepComponentProps) => <Question {...props} />,
        (props: StepComponentProps) => <EndScreen {...props} />
      ],
      [isLoading]
    ),
    { initialIndex }
  );

  const END_SCREEN_INDEX = stepComponentLength - 1;

  useEffect(() => {
    if (currentStepComponentIndex !== 0) return;
    playIntro();
  }, [playIntro, currentStepComponentIndex]);

  useEffect(() => {
    const initialize = async () => {
      if (!xurealId || init) return;
      setInit(true);
      // initialize API repository
      await repo.initApiClient();
      await repo.initApiTraining();

      // set this data in the reducer
      // we can set the whole quiz here without filtering the questions
      // the function to get questions will auto filter already answered questions
      //  based on the initUserProgress set in setAnsweredQuestions above
      dispatch(setQuiz(quiz));

      // get user progress
      const userProgress = await repo.getProgress(xurealId as string);

      const userMedalLevel = userProgress?.levels ? userProgress.levels[userProgress.levels.length - 1] : '';

      const userQuizProgress = userProgress?.topics.reduce((acc, topic) => acc + topic.progress.length, 0) ?? 0;

      // create the user progress number based on the following
      //   if the user has a progress that is a multiple of the medal definition, set it to 0
      //   if the user has a medal progress between 1 and the medal definition, set it to that number
      //   if the user has a medal progress greater than the medal definition and is not a multiple of 4, set it the modulus of the medal definition

      const userProgressNumber =
        userQuizProgress % progressToEarnMedal === 0 ? 0 : userQuizProgress % progressToEarnMedal;

      // setup the progress
      // if the progress arrays dont match up or exist, set an initial progress array
      if (userProgress?.topics && userProgress?.topics.length === quiz.topics.length) {
        dispatch(setAnsweredQuestions(userProgress.topics));
      } else {
        const initUserProgress: { topicID: string; progress: string[] }[] = quiz.topics.map((topic) => {
          return {
            topicID: topic.key,
            progress: userProgress?.topics.find((prog) => prog.topicID === topic.key)?.progress ?? []
          };
        });
        dispatch(setAnsweredQuestions(initUserProgress));
      }

      dispatch(setProgressLevel(userProgressNumber));

      // define the medal strings
      // split them into an array

      // if medals exist or not, set them in state
      dispatch(setCurrentMedals(userMedalLevel ?? ''));

      // this used the api user timestamp, but we are using local storage for now
      // dispatch(setTimestamp(userProgress ? userProgress.timestamp : ""));

      // this method uses a timestamp on the user progress object, but this is getting updated when a user answers a question correctly
      // const wait =
      //   !userProgress?.timestamp && !localWaitTime
      //     ? false
      //     : spec.shouldWait(
      //         new Date(userProgress?.timestamp ?? localWaitTime ?? "")
      //       );

      const localWaitTime = localStorage.getItem('wait');
      dispatch(setTimestamp(localWaitTime ?? ''));

      const highestEarnedMedal = getMedalFromString(userMedalLevel ?? '');

      const wait = highestEarnedMedal.toLowerCase() === 'gold' || spec.shouldWait(new Date(localWaitTime ?? ''));

      // if player should wait, set wait in state
      if (wait) {
        dispatch(setWait(true));
      }

      // this sets player to end wait screen if this is the case
      if (wait) {
        setInitialIndex(END_SCREEN_INDEX);
      }

      setLoading(false);
    };
    setTimeout(initialize, 2000);
  }, [repo, spec, quizContainer, xurealId, END_SCREEN_INDEX, init, progressToEarnMedal]);

  const getNewQuestion = (topic: Topic) => {
    if (!topic) {
      console.error('No topic selected');
      return;
    }
    dispatch(setCurrentTopic(topic));
    let currentMedalString = parseMedalString(state.medals);
    const isBonusRound = progress >= progressToEarnMedal;

    // if it is a bonus round, the medal level has just increased
    // do not use the next medal level for questions, use the current
    let medalLevelForQuestions = isBonusRound
      ? getMedalFromString(currentMedalString)
      : getNextMedalFromString(currentMedalString);

    let quizPolicies = quizContainer.getPolicies();

    // when getting a questionm, we pass in the already answered IDs here
    // this will prevent already answered questions from being asked again
    // if there are no questions left, we will just pick from the whole list of questions in that topic
    let newQuestion = quizPolicies.getQuestion(
      topic,
      medalLevelForQuestions,
      answeredQuestions.filter((el) => el.topicID === topic.key)
    );
    if (!newQuestion) {
      console.error('No question found');
      return;
    }

    dispatch(
      setCurrentQuestion({
        ...newQuestion,
        bonus: progress >= progressToEarnMedal
      })
    );
  };

  // if an error occurs and repo is not initialized, show error message
  if (quizError && !init) {
    return (
      <div>
        <h1>Quiz Error</h1>
        <p>There was an error initializing the quiz. Please try again.</p>
        <button onClick={() => window.location.reload()}>Reload</button>
      </div>
    );
  }

  return (
    <QuizCtx.Provider
      value={{
        state,
        quizContainer,
        dispatch,
        getNewQuestion
      }}
    >
      <>
        {init || DEV_MODE ? (
          RenderStepComponent
        ) : (
          <Grid
            container
            justifyContent='center'
            alignItems='center'
            color='secondary'
            style={{ height: '100vh', width: '100%' }}
          >
            <CircularProgress size={200} />
          </Grid>
        )}
      </>
    </QuizCtx.Provider>
  );
};
