// Situations where we want a medal string returned
// 1. When we want to display the medal string
// 2. When we want to display the next medal string
// 3. When we want to get the question level for the player based on the medal level

// 1 this will parse the string and return a medal if one is in the string, else returns an empty string
export const getMedalFromString = (medals: string) => {
  return parseMedalString(medals);
};

// 2 this will parse the string and return the next medal level
// this satisfies scenario 3 as well
export const getNextMedalFromString = (medals: string) => {
  const parsedMedalString = parseMedalString(medals);
  if (parsedMedalString === 'Gold' || parsedMedalString === 'Silver') return 'Gold';
  if (parsedMedalString === 'Bronze') return 'Silver';
  return 'Bronze';
};

export const getMedalArray = (medals: string) => {
  const medalArray = medals.split(',');
  return medalArray.map((medal) => parseMedalString(medal)).filter(Boolean);
};

// Util to parse the string and return a valid medal string
export const parseMedalString = (medalString: string) => {
  if (medalString === undefined || medalString === '') return '';

  if (medalString.toLowerCase().includes('gold')) return 'Gold';
  if (medalString.toLowerCase().includes('silver')) return 'Silver';
  if (medalString.toLowerCase().includes('bronze')) return 'Bronze';
  return '';
};
