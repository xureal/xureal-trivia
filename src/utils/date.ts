const oneDayMS = 86400000; // one day in milliseconds
const currentTime = new Date();
//   new Date().toLocaleString("en-US", { timeZone: "America/New_York" }) // this was to get the date in Eastern time
const expirationTimeToday = new Date(
  new Date(new Date().setHours(3, 0, 0, 0)).toLocaleString('en-US', {
    timeZone: 'America/New_York'
  })
);
const expirationTimeTomorrow = new Date(
  new Date(new Date().setHours(27, 0, 0, 0)).toLocaleString('en-US', {
    timeZone: 'America/New_York'
  })
);

export const millisecondsUntilNextMonth = () => {
  return new Date(new Date().getFullYear(), 9, 31, 3).getTime(); // R3
};

export const setTomorrowTimestamp = () => {
  return expirationTimeTomorrow.getTime() - currentTime.getTime() > oneDayMS
    ? expirationTimeToday.getTime()
    : expirationTimeTomorrow.getTime();
};

export const millisecondsUntilTomorrow = () => {
  return expirationTimeTomorrow.getTime() - currentTime.getTime() > oneDayMS
    ? expirationTimeToday.getTime() - currentTime.getTime()
    : expirationTimeTomorrow.getTime() - currentTime.getTime();
};

export const returnExpirationTime = (millisecondsToParse: number) => {
  const expiration = new Date(millisecondsToParse).valueOf();
  const now = new Date().valueOf();
  const countdown = expiration - now;
  if (countdown < 0) return '0';
  const msToTime = (ms: number) => {
    let timeString = '';
    let milHolder = ms;
    // return ''
    let days = Math.floor(milHolder / 86400000);
    timeString += days === 0 ? '' : `${days > 9 ? days : `0${days}`}d `;
    milHolder = milHolder - days * 86400000;

    let hours = Math.floor(milHolder / (1000 * 60 * 60));
    timeString += hours === 0 ? '' : `${hours > 9 ? hours : `0${hours}`}h `;
    milHolder = milHolder - hours * 60 * 60 * 1000;

    let minutes = Math.floor(milHolder / (1000 * 60));
    timeString += minutes === 0 ? '' : `${minutes > 9 ? minutes : `0${minutes}`}m `;
    milHolder = milHolder - minutes * 60 * 1000;

    let seconds = Math.floor(milHolder / 1000);
    timeString += `${seconds > 9 ? seconds : `0${seconds}`}s`;

    return timeString;
  };
  return msToTime(countdown);
};

export const minutesToNextHour = () => {
  return 60 - (new Date().getMinutes() % 60);
};

export const millisecondsToNextMonth = () => {
  const date = new Date();
  return new Date(date.getFullYear(), date.getMonth() + 1).getTime();
};
