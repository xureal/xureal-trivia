export const getRandomNumberInclusive = (min: number, max: number) => {
  // returns a random number between min and max, inclusive
  return Math.floor(Math.random() * (max - min + 1) + min);
};
