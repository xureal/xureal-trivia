import { quiz } from '../../core/mockData';
// import { useCallback, useState } from "react";
import { useQuiz } from 'store/useQuiz';
// import { DataKeys } from "core/types";
import { Conditional } from 'common';
import { setQuiz } from 'store/actions';
import { Button } from '@mui/material';

const urlParams = new URLSearchParams(window.location.search);
const test = urlParams.get('test');
const xurealId = urlParams.get('pid');

const ClearData = () => {
  const { quizContainer, state } = useQuiz();
  const { answeredQuestions } = state;
  const repo = quizContainer.getRepo();
  const { dispatch } = useQuiz();

  // const [clear, setClear] = useState<(DataKeys | "")[]>([]);

  // const handleClear = useCallback(
  //   async (key: DataKeys) => {
  //     setTimeout(async () => {
  //       const data = quiz;
  //       dispatch(setQuiz(data));
  //       setClear((prev) => [...prev, key]);
  //     }, 500);
  //   },
  //   [dispatch]
  // );

  const handleResetProgress = async () => {
    await repo.postNewAnswer({
      xurealID: xurealId as string,
      triviaID: 'trivia-demo',
      topics: answeredQuestions,
      levels: [],
      points: 0,
      correctQuestions: [],
      timestamp: new Date(new Date().setDate(new Date().getDate() - 1)).toDateString(),
      reset: true
    });
    const data = quiz;
    dispatch(setQuiz(data));
    // window.location.reload();
    localStorage.removeItem('wait');
  };

  // This is a dev specific component. Normal users will not see it

  return (
    <Conditional cond={test}>
      {/* {quiz.topics.map((topic) => {
        return (
          <button
            onClick={() => handleClear(topic.key)}
            key={topic.key}
            disabled={clear.includes(topic.key)}
          >
            <Conditional
              cond={clear.includes(topic.key)}
              onFalse={<span>Clear {topic.key}</span>}
            >
              {topic.key} cleared
            </Conditional>
          </button>
        );
      })} */}
      <Button onClick={handleResetProgress} variant='contained'>
        Reset Progress
      </Button>

      {/* <button
        onClick={() => handleClear("OlympicsTriviaMedals")}
        disabled={clear.includes("OlympicsTriviaMedals")}
      >
        <Conditional
          cond={clear.includes("OlympicsTriviaMedals")}
          onFalse={<span>Clear OlympicsTriviaMedals</span>}
        >
          OlympicsTriviaMedals cleared
        </Conditional>
      </button>
      <button
        onClick={() => handleClear("OlympicsTriviaCorrectQuestions")}
        disabled={clear.includes("OlympicsTriviaCorrectQuestions")}
      >
        <Conditional
          cond={clear.includes("OlympicsTriviaCorrectQuestions")}
          onFalse={<span>Clear OlympicsTriviaCorrectQuestions</span>}
        >
          OlympicsTriviaCorrectQuestions cleared
        </Conditional>
      </button>
      <button
        onClick={() => handleClear("OlympicsTriviaTimestamp")}
        disabled={clear.includes("OlympicsTriviaTimestamp")}
      >
        <Conditional
          cond={clear.includes("OlympicsTriviaTimestamp")}
          onFalse={<span>Clear OlympicsTriviaTimestamp</span>}
        >
          OlympicsTriviaTimestamp cleared
        </Conditional>
      </button> */}
    </Conditional>
  );
};

export default ClearData;
