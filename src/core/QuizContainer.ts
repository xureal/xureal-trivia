import Repository from './Repository';
import Policies from './Policies';
import Specs from './Specs';
import Analytics from './Analytics';

class QuizContainer {
  constructor(
    private repo: Repository,
    private policies: Policies,
    private specs: Specs,
    private analytics: Analytics
  ) {
    this.getRepo = this.getRepo.bind(this);
    this.getPolicies = this.getPolicies.bind(this);
    this.getSpecs = this.getSpecs.bind(this);
    this.getAnalytics = this.getAnalytics.bind(this);
  }

  getRepo() {
    return this.repo;
  }

  getPolicies() {
    return this.policies;
  }
  getSpecs() {
    return this.specs;
  }

  /** @deprecated analytics are no longer used */
  getAnalytics() {
    return this.analytics;
  }
}

export default QuizContainer;
