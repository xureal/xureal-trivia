import { Answer, Medal, Question, Topic } from './types';
import { getRandomNumberInclusive } from 'utils/random';
import Specs from './Specs';

export const medalMap: Medal[] = ['Bronze', 'Silver', 'Gold'];

class Policies {
  private medalDefinitionAmount = 4; // Amount of questions needed to answer correctly to earn a medal

  constructor(private specs: Specs) {
    this.getProgressRequiredToEarMedal = this.getProgressRequiredToEarMedal.bind(this);
    this.getAnswered = this.getAnswered.bind(this);
    this.getCorrectAnswered = this.getCorrectAnswered.bind(this);
    this.answeredCorrect = this.answeredCorrect.bind(this);
    this.getQuestion = this.getQuestion.bind(this);
    this.setQuestionAnswered = this.setQuestionAnswered.bind(this);
    // this.getMedal = this.getMedal.bind(this)
    // this.bonusQuestion = this.bonusQuestion.bind(this)
    // this.getCurrentMedal = this.getCurrentMedal.bind(this)
    // this.getNextMedal = this.getNextMedal.bind(this)
    // this.getPlace = this.getPlace.bind(this)
    // this.getProgress = this.getProgress.bind(this)
  }

  getProgressRequiredToEarMedal() {
    return this.medalDefinitionAmount;
  }

  getAnswered(topics: Topic[]) {
    let answered = 0;
    topics.forEach((topic) => {
      Object.keys(topic.questions).forEach((medal) => {
        topic.questions[medal as Medal].forEach((question) => {
          if (question.answeredCorrect !== undefined) {
            answered++;
          }
        });
      });
    }, 0);

    return answered;
  }

  getCorrectAnswered(topics: Topic[], bonus?: boolean) {
    let progressCount = 0;
    topics.forEach((topic) => {
      Object.keys(topic.questions).forEach((medal) => {
        topic.questions[medal as Medal].forEach((question) => {
          if (bonus === false && question.bonus) {
            return;
          }
          if (question.answeredCorrect || (bonus === true && question.bonus)) {
            progressCount++;
          }
        });
      });
    });
    return progressCount;
  }

  answeredCorrect(answers: Answer[], id: Answer['id']) {
    return Boolean(answers.find((answer) => answer.id === id && answer.correct));
  }

  getQuestion(topic: Topic, medal: Medal, answeredQuestions: { topicID: string; progress: string[] }[]) {
    if (medal === 'None' || medal === '') medal = 'Bronze';

    const alreadyAnsweredIDs = answeredQuestions.find((el) => el.topicID === topic.key)?.progress;

    const allQuestions = topic.questions[medal];

    const availableQuestions = topic.questions[medal].filter(
      (question) => question.id && !alreadyAnsweredIDs?.includes(question.id)
    );

    return availableQuestions.length > 0
      ? availableQuestions[getRandomNumberInclusive(0, availableQuestions.length - 1)]
      : allQuestions[getRandomNumberInclusive(0, allQuestions.length - 1)];
  }

  /** @deprecated no longer needed, this logic is old and too complicated */
  setQuestionAnswered(
    topics: Topic[],
    topicId: Topic['id'],
    questionId: Question['id'],
    correct: boolean,
    bonus: boolean,
    medalLevel: string
  ) {
    if (medalLevel === 'None') medalLevel = 'Bronze';
    const match = (tId: Topic['id'], qId: Question['id']) => topicId === tId && qId === questionId;
    return topics
      .map((topic) => ({
        ...topic,
        questions: {
          ...topic.questions,
          [medalLevel]: topic.questions[medalLevel].map((question) => ({
            ...question,
            answeredCorrect: match(topic.id, question.id) ? correct : question.answeredCorrect,
            bonus: match(topic.id, question.id) ? bonus : question.bonus
          }))
        }
      }))
      .map((topic) => ({
        ...topic,
        finished: this.specs.topicFinished(topic)
      }));
  }

  // getMedal(topics: Topic[]): [Medal | undefined, number] {
  //     const medalIndex = Math.floor(this.getCorrectAnswered(topics, false) / this.medalDefinitionAmount - 1)
  //     return [medalMap[medalIndex], medalIndex]
  // }

  // bonusQuestion(topics: Topic[], prev ?: boolean) {
  //     const correctAnswered = this.getCorrectAnswered(topics, true)
  //     return correctAnswered ? correctAnswered % (this.medalDefinitionAmount + 1) === 0 : false
  // }

  // Calculates the medal the user currently holds by dividing the amount of correctly
  // answered questions with the defined amount to earn a medal and returns a string of the medal owned
  // getCurrentMedal(topics: Topic[]){
  //     const progress = this.getCorrectAnswered(topics, false)
  //     const offset = progress <= 4 ? 0 : progress <= 9 ? 1 : 2
  //     const place = (progress - offset) / this.medalDefinitionAmount
  //     let medal: Medal = "Bronze"

  //     if (place >= 1) {
  //         medal = "Bronze"
  //     }

  //     if (place >= 2) {
  //         medal = "Silver"
  //     }

  //     if (place >= 3) {
  //         medal = "Gold"
  //     }

  //     return medal
  // }

  // Calculates the medal the user will earn next by dividing the amount of correctly
  // answered questions with the defined amount to earn a medal and returns a string of the medal owned
  // getNextMedal(topics: Topic[]) {
  //     const progress = this.getCorrectAnswered(topics, true)
  //     const place = progress / (this.medalDefinitionAmount + 1)

  //     let medal: Medal = "Bronze"

  //     if (place >= 1) {
  //         medal = "Silver"
  //     }

  //     if (place >= 2) {
  //         medal = "Gold"
  //     }

  //     return medal
  // }

  // Function for Endscreen that calculates what place the user is in (bronze, silver, gold), and returns the integer
  // getPlace(topics: Topic[]): number {
  //     const progress = this.getCorrectAnswered(topics, false)
  //     const offset = progress <= 4 ? 0 : progress <= 9 ? 1 : 2
  //     return ((progress - offset) / this.medalDefinitionAmount)
  // }

  // getProgress(topics: Topic[], bonus : boolean = true) {
  //     return this.getCorrectAnswered(topics, bonus) % (this.medalDefinitionAmount + 1)
  // }
}

export default Policies;
