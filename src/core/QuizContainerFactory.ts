import Repository from './Repository';
import QuizContainer from './QuizContainer';
import Policies from './Policies';
import Specs from './Specs';
import Analytics from './Analytics';

class QuizContainerFactory {
  static create(repo: Repository, policies: Policies, specs: Specs, analytics: Analytics) {
    return new QuizContainer(repo, policies, specs, analytics);
  }
}

export default QuizContainerFactory;
