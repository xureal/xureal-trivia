import { State } from './types';

export const initialQuizData: State = {
  id: '',
  miles: 0,
  wait: false,
  answeredQuestions: [],
  answerCalculated: false,
  answerCorrect: false,
  currentTopic: {
    id: '1',
    key: 'OlympicsTriviaTopic1',
    name: '',
    bgImage: '',
    bgImageXs: '',
    questionBg: '',
    questions: {
      Bronze: [],
      Silver: [],
      Gold: []
    },
    finished: false
  },
  currentQuestion: {
    id: '',
    title: '',
    miles: 0,
    answeredCorrect: false,
    bonus: false,
    answers: [],
    description: ''
  },
  topics: [],
  timestamp: '',
  progress: 0,
  medals: ''
};

// {
//     id: string;
//     title: string;
//     miles: number;
//     answeredCorrect?: boolean;
//     bonus: boolean;
//     answers: Answer[];
//     description: string;
//   }
