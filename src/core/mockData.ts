import { Quiz } from './types';
import yellow from 'assets/activeStates/yellow-active.png';
import yellowXs from 'assets/activeStates/xs/yellow-xs-active.png';
import orange from 'assets/activeStates/orange-active.png';
import orangeXs from 'assets/activeStates/xs/orange-xs-active.png';
import purple from 'assets/activeStates/purple-active.png';
import purpleXs from 'assets/activeStates/xs/purple-xs-active.png';
import red from 'assets/activeStates/red-active.png';
import redXs from 'assets/activeStates/xs/red-xs-active.png';
import blue from 'assets/activeStates/blue-active.png';
import blueXs from 'assets/activeStates/xs/blue-xs-active.png';
import green from 'assets/activeStates/green-active.png';
import greenXs from 'assets/activeStates/xs/green-xs-active.png';

export const topicKeys = [
  'OlympicsTriviaTopic1',
  'OlympicsTriviaTopic2',
  'OlympicsTriviaTopic3',
  'OlympicsTriviaTopic4',
  'OlympicsTriviaTopic5',
  'OlympicsTriviaTopic6',
  'OlympicsTriviaTopic7'
] as const;

export const question = {
  id: '1',
  title: '',
  miles: 0,
  bonus: false,
  answers: [
    { id: '1', text: 'Lorem ipsum dolor sit amet cor', correct: true },
    { id: '2', text: 'Lorem ipsum dolor sit amet cor', correct: false },
    { id: '3', text: 'Lorem ipsum dolor sit amet cor', correct: false },
    { id: '4', text: 'Lorem ipsum dolor sit amet cor', correct: false }
  ],
  description: 'Test'
};

export const quiz2: Quiz = {
  id: 'trivia-demo',
  miles: 0,
  wait: false,
  progress: 0,
  medals: '',
  topics: [
    {
      id: 'Smart Home',
      key: 'OlympicsTriviaTopic1',
      name: 'Smart Home',
      bgImage: purple,
      bgImageXs: purpleXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [],
        Silver: [
          {
            id: 'Q233',
            title: 'What is the primary goal of the ADT Blueprint by 2025?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'To become the largest global security provider',
                correct: false
              },
              {
                id: 'A1',
                text: 'To achieve $10+ billion in revenue',
                correct: true
              },
              {
                id: 'A2',
                text: 'To expand into new international markets',
                correct: false
              },
              {
                id: 'A3',
                text: 'To launch a new product line every year',
                correct: false
              }
            ],
            description: 'B. To achieve $10+ billion in revenue'
          },
          {
            id: 'Q233',
            title: "According to the ADT Blueprint, what is ADT's mission?",
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'To be the leading provider in smart home technology',
                correct: false
              },
              {
                id: 'A1',
                text: 'To empower people to protect and connect what matters most',
                correct: true
              },
              {
                id: 'A2',
                text: 'To innovate the security industry with cutting-edge technology',
                correct: false
              },
              {
                id: 'A3',
                text: 'To ensure the highest level of customer service in the industry',
                correct: false
              }
            ],
            description: 'B. To empower people to protect and connect what matters most'
          },
          {
            id: 'Q233',
            title: "What is ADT's vision as stated in the Blueprint?",
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: "To be the world's most trusted security provider",
                correct: false
              },
              {
                id: 'A1',
                text: 'To become the #1 smart home provider, commercial integrator, and residential solar installer in America',
                correct: true
              },
              {
                id: 'A2',
                text: 'To double the company size by 2030',
                correct: false
              },
              {
                id: 'A3',
                text: 'To innovate security solutions for the digital age',
                correct: false
              }
            ],
            description:
              'B) To become the #1 smart home provider, commercial integrator, and residential solar installer in America'
          },
          {
            id: 'Q233',
            title:
              'What is one of the ways ADT plans to transform its business for its stakeholders according to the Blueprint?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'By becoming the lowest-cost provider in the market',
                correct: false
              },
              {
                id: 'A1',
                text: 'By making ADT the best place to work',
                correct: true
              },
              {
                id: 'A2',
                text: 'By exclusively focusing on residential security',
                correct: false
              },
              {
                id: 'A3',
                text: 'By discontinuing partnerships with dealers and suppliers',
                correct: false
              }
            ],
            description: 'B) By making ADT the best place to work'
          },
          {
            id: 'Q233',
            title: 'When was the term IoT created?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Christopher Epinette coiined the term in 2022',
                correct: false
              },
              {
                id: 'A1',
                text: 'Vinton Cerf and Bob Kahn invented IoT',
                correct: true
              },
              { id: 'A2', text: 'In 1994 by Al Gore', correct: false },
              {
                id: 'A3',
                text: 'When ChatGPT fist launched in November 2022',
                correct: false
              }
            ],
            description:
              'B) Computer scientists Vinton Cerf and Bob Kahn are credited with inventing the Internet communication protocols we use today and the system referred to as IoT'
          },
          {
            id: 'Q233',
            title: 'How big is the smart home industry?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Is est. to become the fastest growing job by 2025',
                correct: false
              },
              {
                id: 'A1',
                text: 'Was estimated at $13.98 billion in 2023',
                correct: false
              },
              {
                id: 'A2',
                text: 'The smart home market is worth over $32 trillion',
                correct: false
              },
              {
                id: 'A3',
                text: 'The smart home market was valued at 80.21B in 2022',
                correct: true
              }
            ],
            description:
              'D) The global smart home market size was valued at $80.21 billion in 2022 & is projected to grow from $93.98 billion in 2023 to $338.28 billion by 2030.'
          }
        ],
        Gold: []
      },
      finished: false
    },
    {
      id: 'Google Nest Cams',
      key: 'OlympicsTriviaTopic2',
      name: 'Google Nest Cams',
      bgImage: blue,
      bgImageXs: blueXs,
      questionBg: 'linear-gradient(90deg, #1F69FF 0%, #508AFE 100%)',
      questions: {
        Bronze: [],
        Silver: [],
        Gold: []
      },
      finished: false
    },
    {
      id: 'Security System Basics',
      key: 'OlympicsTriviaTopic3',
      name: 'Security System Basics',
      bgImage: green,
      bgImageXs: greenXs,
      questionBg: 'linear-gradient(90deg, #008558 0%, #30B689 100%)',
      questions: {
        Bronze: [],
        Silver: [],
        Gold: []
      },
      finished: false
    },
    {
      id: 'Basic Networking',
      key: 'OlympicsTriviaTopic4',
      name: 'Basic Networking',
      bgImage: yellow,
      bgImageXs: yellowXs,
      questionBg: 'linear-gradient(90deg, #FFAA00 0%, #F4BB47 100%)',
      questions: {
        Bronze: [],
        Silver: [],
        Gold: []
      },
      finished: false
    },
    {
      id: 'ADT+ and SHSS',
      key: 'OlympicsTriviaTopic5',
      name: 'ADT+ and SHSS',
      bgImage: orange,
      bgImageXs: orangeXs,
      questionBg: 'linear-gradient(90deg, #E64F00 0%, #ED6E2C 100%)',
      questions: {
        Bronze: [],
        Silver: [],
        Gold: []
      },
      finished: false
    },
    {
      id: 'Bonus Questions',
      key: 'OlympicsTriviaTopic6',
      name: 'Bonus Questions',
      bgImage: red,
      bgImageXs: redXs,
      questionBg: 'linear-gradient(90deg, #E6004D 0%, #DB346C 100%)',
      questions: {
        Bronze: [],
        Silver: [],
        Gold: []
      },
      finished: false
    }
  ]
};

export const quiz: Quiz = {
  id: 'trivia-demo',
  miles: 0,
  wait: false,
  progress: 0,
  medals: '',
  topics: [
    {
      id: 'Smart Home',
      key: 'OlympicsTriviaTopic1',
      name: 'Smart Home',
      bgImage: purple,
      bgImageXs: purpleXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '0',
            title: 'Which wireless Communication Protocol is typically used for smart home automation?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '5G LTE',
                correct: false
              },
              {
                id: 'A1',
                text: 'Z-Wave',
                correct: true
              },
              {
                id: 'A2',
                text: 'AM/FM',
                correct: false
              },
              {
                id: 'A3',
                text: 'Satelite',
                correct: false
              }
            ],
            description: 'B) Z-Wave is one of the most common communication protocols used in Smart Home Devices'
          },
          {
            id: '4',
            title: 'Which device can adjust room temperature?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Nest Cam',
                correct: false
              },
              {
                id: 'A1',
                text: 'Nest Protect',
                correct: false
              },
              {
                id: 'A2',
                text: 'Nest Thermostat',
                correct: true
              },
              {
                id: 'A3',
                text: 'Nest Hub',
                correct: false
              }
            ],
            description: 'C) The Nest Smart Learning Thermostat can adjust Temperature'
          },
          {
            id: '6',
            title: 'What does IoT stand for?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Internet of Things',
                correct: true
              },
              {
                id: 'A1',
                text: 'Information Operations Team',
                correct: false
              },
              {
                id: 'A2',
                text: 'Ideas of Thought',
                correct: false
              },
              {
                id: 'A3',
                text: 'Information of Technology',
                correct: false
              }
            ],
            description: 'A) Internet of Things'
          },
          {
            id: '10',
            title: 'Which type of light bulbs can be smart Lights?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Incandescent',
                correct: false
              },
              {
                id: 'A1',
                text: 'Flourescent',
                correct: false
              },
              {
                id: 'A2',
                text: 'Black Light',
                correct: false
              },
              {
                id: 'A3',
                text: 'LED',
                correct: true
              }
            ],
            description: 'D) LED Lights are the only type of smart bulbs available'
          }
        ],
        Silver: [
          {
            id: '2',
            title: 'What is the main purpose of a Smart Home Bridge/Hub? ',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Charge Devices Wirelessly',
                correct: false
              },
              {
                id: 'A1',
                text: 'Translate Data between Communication Protocols',
                correct: true
              },
              {
                id: 'A2',
                text: 'Play Music',
                correct: false
              },
              {
                id: 'A3',
                text: 'Monitor Outdoor Weather',
                correct: false
              }
            ],
            description:
              'B) A Bridge/Hub is used to connect devices from one Communication Protocol to another such as Zigbee to the WiFi Protocol'
          },
          {
            id: '5',
            title: 'Which Nest device offers gesture control for media playback?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Nest Protect',
                correct: false
              },
              {
                id: 'A1',
                text: 'Next Cam Outdoor',
                correct: false
              },
              {
                id: 'A2',
                text: 'Nest Hub Max',
                correct: true
              },
              {
                id: 'A3',
                text: 'Nest Learning Thermostat',
                correct: false
              }
            ],
            description:
              'C) When the feature is enabled in the Google Home App, The Nest Hub Max can recognize hand gestures.'
          },
          {
            id: '9',
            title: 'How big is the smart home industry?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Is est. to become the fastest growing job by 2025',
                correct: false
              },
              {
                id: 'A1',
                text: 'Was estimated at $13.98 billion in 2023',
                correct: false
              },
              {
                id: 'A2',
                text: 'The smart home market is worth over $32 trillion',
                correct: false
              },
              {
                id: 'A3',
                text: 'The smart home market was valued at 80.21B in 2022',
                correct: true
              }
            ],
            description:
              'D) The global smart home market size was valued at $80.21 billion in 2022 & is projected to grow from $93.98 billion in 2023 to $338.28 billion by 2030.'
          },
          {
            id: '11',
            title: 'Which Nest device features a Thread radio for smart home connectivity?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Google Nest Wifi',
                correct: true
              },
              {
                id: 'A1',
                text: 'Nest Mini',
                correct: false
              },
              {
                id: 'A2',
                text: 'Nest Hub',
                correct: false
              },
              {
                id: 'A3',
                text: 'Nest Doorbell',
                correct: false
              }
            ],
            description: 'A) The Google Nest Wifi Router and Points have Thread compatible Radios in them'
          }
        ],
        Gold: [
          {
            id: '1',
            title: "What's the maximum data rate of the Zigbee Communication Protocol?",
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '100 Mbps',
                correct: false
              },
              {
                id: 'A1',
                text: '1 Gbps',
                correct: false
              },
              {
                id: 'A2',
                text: '250 kbps',
                correct: true
              },
              {
                id: 'A3',
                text: '200 Mbps',
                correct: false
              }
            ],
            description: 'C) The Zigbee protocol communcates at speeds up to 250 kbps'
          },
          {
            id: '3',
            title: 'Whats the typical frequency range of Zigbee Devices?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '2.4 GHz',
                correct: true
              },
              {
                id: 'A1',
                text: '5 GHz',
                correct: false
              },
              {
                id: 'A2',
                text: '900 MHz',
                correct: false
              },
              {
                id: 'A3',
                text: '250 kbps',
                correct: false
              }
            ],
            description:
              'A) Zigbee Devices operate on the 2.4 GHz frequency similar to Wi-Fi however they cannot transfer data as fast as WiFi Devices'
          },
          {
            id: '7',
            title: 'When was the term IoT created?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Christopher Epinette coiined the term in 2022',
                correct: false
              },
              {
                id: 'A1',
                text: 'Vinton Cerf and Bob Kahn invented IoT',
                correct: true
              },
              {
                id: 'A2',
                text: 'In 1994 by Al Gore',
                correct: false
              },
              {
                id: 'A3',
                text: 'When ChatGPT fist launched in November 2022',
                correct: false
              }
            ],
            description:
              'B) Computer scientists Vinton Cerf and Bob Kahn are credited with inventing the Internet communication protocols we use today and the system referred to as IoT'
          },
          {
            id: '8',
            title: 'What was the first IoT device?  hint: it was created before the term IoT was created?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'The ARPANET router, Interface Message Processor',
                correct: false
              },
              {
                id: 'A1',
                text: 'Commodore PET (Personal Electronic Transactor)',
                correct: false
              },
              {
                id: 'A2',
                text: 'Internet service providers (ISPs) emerged in 1989',
                correct: false
              },
              {
                id: 'A3',
                text: 'In 1990 John Romkey introduced a toaster',
                correct: true
              }
            ],
            description:
              'D) In 1990, John Romkey first introduced a toaster that could be on or off with the help of the internet, which was the first IoT device.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Google Nest Cam',
      key: 'OlympicsTriviaTopic2',
      name: 'Google Nest Cam',
      bgImage: blue,
      bgImageXs: blueXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '100',
            title:
              'What is the maximum offline recording time for the Google Nest Cam (indoor, wired) during an internet or power outage?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '30 minutes',
                correct: false
              },
              {
                id: 'A1',
                text: '45 minutes',
                correct: false
              },
              {
                id: 'A2',
                text: '1 hour',
                correct: true
              },
              {
                id: 'A3',
                text: '2 hours',
                correct: false
              }
            ],
            description: 'C) 1 hour'
          },
          {
            id: '101',
            title:
              'Which feature is exclusive to the Google Nest Hub Max among the devices listed?How far can the built-in motion sensor detect movement in the Google Nest Cam (indoor, wired)?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Up to 15ft',
                correct: false
              },
              {
                id: 'A1',
                text: 'Up to 20ft',
                correct: false
              },
              {
                id: 'A2',
                text: 'Up to 25ft',
                correct: true
              },
              {
                id: 'A3',
                text: 'Up to 30ft',
                correct: false
              }
            ],
            description: 'C) Up to 25ft'
          },
          {
            id: '102',
            title: 'What is the field of view for the Google Nest Cam (indoor, wired)?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '100°',
                correct: false
              },
              {
                id: 'A1',
                text: '120°',
                correct: false
              },
              {
                id: 'A2',
                text: '130° diagonal',
                correct: true
              },
              {
                id: 'A3',
                text: '140°',
                correct: false
              }
            ],
            description: 'C) 130° diagonal'
          }
        ],
        Silver: [
          {
            id: '103',
            title: 'Which feature allows the Google Nest Cam (indoor, wired) to send alerts?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Light detection',
                correct: false
              },
              {
                id: 'A1',
                text: 'Sound recognition',
                correct: false
              },
              {
                id: 'A2',
                text: 'Motion detection',
                correct: true
              },
              {
                id: 'A3',
                text: 'Temperature change',
                correct: false
              }
            ],
            description: 'C) Motion detection'
          },
          {
            id: '104',
            title:
              'For how many days can the Google Nest Cam (indoor, wired) continuously record video with Nest Aware Plus?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '5 days',
                correct: false
              },
              {
                id: 'A1',
                text: '7 days',
                correct: false
              },
              {
                id: 'A2',
                text: '10 days',
                correct: true
              },
              {
                id: 'A3',
                text: '14 days',
                correct: false
              }
            ],
            description: 'C) 10 days'
          },
          {
            id: '106',
            title: 'What field of view are the wired and battery Google Doorbells?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '360°',
                correct: false
              },
              {
                id: 'A1',
                text: '98°',
                correct: false
              },
              {
                id: 'A2',
                text: '145°',
                correct: true
              },
              {
                id: 'A3',
                text: '451°',
                correct: false
              }
            ],
            description: 'C 145° diagonal'
          },
          {
            id: '107',
            title: 'What is the field of view for the Google Nest Floodlight Cam?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '130°',
                correct: true
              },
              {
                id: 'A1',
                text: '100°',
                correct: false
              },
              {
                id: 'A2',
                text: '120°',
                correct: false
              },
              {
                id: 'A3',
                text: '145° diagonal',
                correct: false
              }
            ],
            description: 'A) 130°'
          },
          {
            id: '108',
            title: 'Can a Google Doorbell (battery) be wired for power?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'No it is battery powered',
                correct: false
              },
              {
                id: 'A1',
                text: 'Yes, with a lamp cord',
                correct: false
              },
              {
                id: 'A2',
                text: 'No, it has a solar powered charger',
                correct: false
              },
              {
                id: 'A3',
                text: 'Yes, with 8-24VAC',
                correct: true
              }
            ],
            description: 'D)Yes, It has a rechargeable battery pack with option to wire – requires 8-24VAC'
          }
        ],
        Gold: [
          {
            id: '105',
            title: 'What is the night vision range of the Google Nest Cam (indoor, wired)?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '5 feet',
                correct: false
              },
              {
                id: 'A1',
                text: '8 feet',
                correct: false
              },
              {
                id: 'A2',
                text: '10 feet',
                correct: true
              },
              {
                id: 'A3',
                text: '12 feet',
                correct: false
              }
            ],
            description: 'C) 10 feet'
          },
          {
            id: '109',
            title:
              'What intelligent alert feature does a Google Doorbell have that Indoor and Outdoor cams do not have?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Motion sensing',
                correct: false
              },
              {
                id: 'A1',
                text: 'Packages',
                correct: true
              },
              {
                id: 'A2',
                text: 'Siri voice',
                correct: false
              },
              {
                id: 'A3',
                text: 'Phone notifications',
                correct: false
              }
            ],
            description: 'B) Packages'
          },
          {
            id: '110',
            title:
              "How long can Nest Cam (wired), Nest Cam (battery), Nest Doorbell (wired, 2nd gen), and Nest Doorbell (battery) record events when they're offline?",
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '24/7 recording up to 10 days',
                correct: false
              },
              {
                id: 'A1',
                text: 'They cannot record when offline',
                correct: false
              },
              {
                id: 'A2',
                text: 'Enough internal memory to store up to one hour',
                correct: true
              },
              {
                id: 'A3',
                text: 'They safely save 1 clip with no WiFi connection',
                correct: false
              }
            ],
            description:
              "C)The Nest Cam (wired), Nest Cam (battery), Nest Doorbell (wired, 2nd gen), and Nest Doorbell (battery) have enough internal memory to store up to one hour of recorded events when they're offline."
          }
        ]
      },
      finished: false
    },
    {
      id: 'Security System Basics',
      key: 'OlympicsTriviaTopic3',
      name: 'Security System Basics',
      bgImage: green,
      bgImageXs: greenXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '200',
            title: 'Can Google hub chime when a security sensor from ADT system on door or window opens?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'No',
                correct: true
              },
              {
                id: 'A1',
                text: 'Yes, It works like a secondary security panel',
                correct: false
              },
              {
                id: 'A2',
                text: 'Yes, with an automation set up in Google Home',
                correct: false
              },
              {
                id: 'A3',
                text: 'No, the hub has no speaker built in',
                correct: false
              }
            ],
            description:
              'a) No. The hub is not a secondary panel. It will chime when a Google doorbell is rang but not for door/window sensors'
          },
          {
            id: '202',
            title: 'Do you need to have a permit for a Home Security System?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: "Yes, If you don't have guns or dogs",
                correct: false
              },
              {
                id: 'A1',
                text: 'Only after your alarm goes off',
                correct: false
              },
              {
                id: 'A2',
                text: 'Yes, if the authorities are to be notified',
                correct: false
              },
              {
                id: 'A3',
                text: 'It depends on local ordinances',
                correct: true
              }
            ],
            description: 'D) Depends on local ordinances'
          },
          {
            id: '204',
            title: 'What are the benefits of wireless security systems vs wired ones?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Wireless offers easier installation and flexibility in placement',
                correct: true
              },
              {
                id: 'A1',
                text: 'Wireless systems have more over the air range',
                correct: false
              },
              {
                id: 'A2',
                text: 'Wireless systems are easier to sell due to cost',
                correct: false
              },
              {
                id: 'A3',
                text: 'Wired systems will always be better',
                correct: false
              }
            ],
            description:
              'A) Wireless security systems rely on radio frequency signals to communicate between components, offering easier installation and flexibility in placement. Wired systems, on the other hand, use physical wiring for connections, typically offering greater reliability but requiring more effort for installation.'
          },
          {
            id: '207',
            title: 'What does CCTV stand for? ',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Closed Circuit Television ',
                correct: true
              },
              {
                id: 'A1',
                text: 'Cool Cat TV',
                correct: false
              },
              {
                id: 'A2',
                text: 'Computer Control Tool',
                correct: false
              },
              {
                id: 'A3',
                text: 'Cordless Computing Television',
                correct: false
              }
            ],
            description: 'A) CCTV Stands for Closed Circuit Television. '
          },
          {
            id: '210',
            title: 'Where should you never install a Motion Detector?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Towards a heat source',
                correct: true
              },
              {
                id: 'A1',
                text: 'In a Living Room',
                correct: false
              },
              {
                id: 'A2',
                text: 'In a Bedroom',
                correct: false
              },
              {
                id: 'A3',
                text: 'In a common room',
                correct: false
              }
            ],
            description:
              'A) Motion Detectors should never be installed facing a Heat source such as a Vent or Radiator. These can cause False Alarms.'
          }
        ],
        Silver: [
          {
            id: '203',
            title: 'What is a reed switch in Door/Window sensors?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'The piece that beeps',
                correct: false
              },
              {
                id: 'A1',
                text: 'A mechanical switch like a light switch in sensor',
                correct: false
              },
              {
                id: 'A2',
                text: 'A magnetically operated contact inside a glass tube',
                correct: true
              },
              {
                id: 'A3',
                text: 'Can operate about 10^5 - 10^6 switching cycles with max power.',
                correct: false
              }
            ],
            description:
              'C) The reed switch was born in 1936. It was the brainchild of W.B. Ellwood, and it earned its patent in 1941. The switch looks like a small glass capsule with electrical leads poking out of each end.'
          },
          {
            id: '206',
            title: 'What types of alarms are commonly used in security systems?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Primary types are ADT+, Command and Pulse',
                correct: false
              },
              {
                id: 'A1',
                text: 'Audible and silent',
                correct: true
              },
              {
                id: 'A2',
                text: 'Sirens, and sometimes secondary keypads',
                correct: false
              },
              {
                id: 'A3',
                text: 'Klaxon',
                correct: false
              }
            ],
            description:
              'B) Audible alarms produce loud sounds to alert occupants and deter intruders. Silent alarms, on the other hand, send alerts to monitoring centers or designated contacts without producing audible signals.'
          },
          {
            id: '209',
            title: 'Whats the primary purpose of a security systems Control Panel?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Make Coffee',
                correct: false
              },
              {
                id: 'A1',
                text: 'Manage Security Devices',
                correct: true
              },
              {
                id: 'A2',
                text: 'Protect from Virusus',
                correct: false
              },
              {
                id: 'A3',
                text: 'Enter in Codes to Disarm',
                correct: false
              }
            ],
            description:
              'B) The control Panel Manages and Monitors incoming information from connected Security Devices and sends signals to the Monitoring station as needed.'
          }
        ],
        Gold: [
          {
            id: '201',
            title:
              'fill in the blank. The National Council for Home Safety and Security, a trade association, says that homes without a security system are (BLANK) more likely to be burglarized.',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '3',
                correct: true
              },
              {
                id: 'A1',
                text: 'infinitely',
                correct: false
              },
              {
                id: 'A2',
                text: '0.5',
                correct: false
              },
              {
                id: 'A3',
                text: '45293',
                correct: false
              }
            ],
            description:
              'A) The National Council for Home Safety and Security, a trade association, says that homes without a security system are 300% more likely to be burglarized.'
          },
          {
            id: '205',
            title: 'What is the purpose of surveillance cameras in a security system?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '24/7 recording and 10 day video history',
                correct: false
              },
              {
                id: 'A1',
                text: 'To provide visual monitoring of a property',
                correct: true
              },
              {
                id: 'A2',
                text: 'To see if the security system is armed',
                correct: false
              },
              {
                id: 'A3',
                text: 'To assist with installation of sensors and devices',
                correct: false
              }
            ],
            description:
              'B) Surveillance cameras provide visual monitoring of a property. They can record footage for later review, act as a deterrent to potential intruders, and allow remote viewing of the property in real-time via the internet.'
          },
          {
            id: '208',
            title: 'What does PIR stand for in Security Systems?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Passive Interference Range',
                correct: false
              },
              {
                id: 'A1',
                text: 'Passive Infrared',
                correct: true
              },
              {
                id: 'A2',
                text: 'Primary Intrusion Response',
                correct: false
              },
              {
                id: 'A3',
                text: 'Panic In-House Regulation',
                correct: false
              }
            ],
            description: 'B) "PIR" Stands for Passive Infrared and is used in Motion Detectors'
          },
          {
            id: '211',
            title: 'Whats the Primary Function of a "Duress Code"?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Actviate Night Mode',
                correct: false
              },
              {
                id: 'A1',
                text: 'Disarm only a portion of an Alarm System',
                correct: false
              },
              {
                id: 'A2',
                text: 'Adjust other User Codes',
                correct: false
              },
              {
                id: 'A3',
                text: 'Call Emergency Services',
                correct: true
              }
            ],
            description:
              'D) A Durress code is a code that when used on a keypad to disarm a system will appear to disarm the system but will also call emergency services'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Basic Networking',
      key: 'OlympicsTriviaTopic4',
      name: 'Basic Networking',
      bgImage: yellow,
      bgImageXs: yellowXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '300',
            title: 'What does ISP stand for?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '​Individual Support Plan',
                correct: false
              },
              {
                id: 'A1',
                text: 'Involuntary Separation Pay',
                correct: false
              },
              {
                id: 'A2',
                text: 'Indiana State Program',
                correct: false
              },
              {
                id: 'A3',
                text: 'Internet Service Provider',
                correct: true
              }
            ],
            description: 'D)Internet Service Provider'
          },
          {
            id: '303',
            title: 'Which Communication Protocol doesnt require a Hub?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Z-Wave',
                correct: false
              },
              {
                id: 'A1',
                text: 'Zigbee',
                correct: false
              },
              {
                id: 'A2',
                text: 'Zoro',
                correct: false
              },
              {
                id: 'A3',
                text: 'Wi-Fi',
                correct: true
              }
            ],
            description: 'D) Smart Devices using the WiFi Protocol doesnt require a Hub'
          },
          {
            id: '304',
            title: 'What are the 2 primary bands of WiFi?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '6GHz and 900MHz',
                correct: false
              },
              {
                id: 'A1',
                text: '5.2GHz and 2.4GHz',
                correct: false
              },
              {
                id: 'A2',
                text: '802.11b/g/n/ax and 803.22a/h/n/ac/ax',
                correct: false
              },
              {
                id: 'A3',
                text: '2.4GHz and 5 GHz',
                correct: true
              }
            ],
            description:
              'D)2.4GHz and 5 GHz, The 802.11 standard provides several radio frequency bands for use in Wi-Fi communications, each divided into a multitude of channels numbered at 5 MHz spacing'
          },
          {
            id: '307',
            title: 'What does "DNS" Stand for?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Domain Name Service',
                correct: false
              },
              {
                id: 'A1',
                text: 'Dynamic Name System',
                correct: false
              },
              {
                id: 'A2',
                text: 'Domain Name System',
                correct: true
              },
              {
                id: 'A3',
                text: 'Digital Network Server',
                correct: false
              }
            ],
            description:
              'C) "DNS" Stands for Domain Name System. It translates Domain Names like "www.adt.com" into Machine readable IP Addresses such as 192.02.0.44'
          },
          {
            id: '309',
            title: 'What does "FTP" Stand for? ',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'File Transfer Protocol',
                correct: true
              },
              {
                id: 'A1',
                text: 'File Transmission Protocol',
                correct: false
              },
              {
                id: 'A2',
                text: 'Fast Transfer Protocol',
                correct: false
              },
              {
                id: 'A3',
                text: 'File Transport Protocol',
                correct: false
              }
            ],
            description:
              'A) "FTP" Stands for File Transfer Protocol. Its used to transfer files from one host to another over a TCP-based network, such as the Internet'
          }
        ],
        Silver: [
          {
            id: '302',
            title: 'What is the difference wetween a WAN and a LAN?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'WAN is for private use, LAN for public',
                correct: false
              },
              {
                id: 'A1',
                text: 'LAN Uses Layer 2 of the network Segmentation',
                correct: false
              },
              {
                id: 'A2',
                text: 'LAN is Local Network and WAN is Wide Area',
                correct: true
              },
              {
                id: 'A3',
                text: 'The WAN show is more popular on Youtube',
                correct: false
              }
            ],
            description:
              'C)The main differences between a WAN (Wide Area Network) and a LAN (Local Area Network) lie in their scope, geographical coverage, and purpose'
          },
          {
            id: '305',
            title: 'How many devices can a Z-Wave network support?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Up to 232 Z-Wave devices',
                correct: true
              },
              {
                id: 'A1',
                text: 'Up to 300 Z-Wave devices',
                correct: false
              },
              {
                id: 'A2',
                text: 'Up to 600 Z-Wave devices',
                correct: false
              },
              {
                id: 'A3',
                text: 'Up to 150 Z-Wave devices',
                correct: false
              }
            ],
            description:
              'A)A Z-Wave network can handle a maximum of 232 devices (nodes), including the primary controller.'
          },
          {
            id: '306',
            title: 'What is the range of Z-Wave devices?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Up to 232 Z-Wave devices',
                correct: false
              },
              {
                id: 'A1',
                text: 'Z-Wave has a range of 100 meters in open air',
                correct: true
              },
              {
                id: 'A2',
                text: '80 meters across an entire Z-Wave network',
                correct: false
              },
              {
                id: 'A3',
                text: 'Small data packets at data rates up to 100kbit/s',
                correct: false
              }
            ],
            description:
              'B)Z-Wave has a range of 100 meters or 328 feet in open air, building materials reduce that range, it is recommended to have a Z-Wave device roughly every 30 feet, or closer for maximum efficiency'
          },
          {
            id: '310',
            title: 'What secures a network from threats?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Firewalls',
                correct: true
              },
              {
                id: 'A1',
                text: 'AntiVirus ',
                correct: false
              },
              {
                id: 'A2',
                text: 'Router',
                correct: false
              },
              {
                id: 'A3',
                text: 'Modem',
                correct: false
              }
            ],
            description:
              'A) A firewall is a network security device that monitors incoming and outgoing network traffic and decides whether to allow or block specific traffic based on a defined set of security rules.'
          }
        ],
        Gold: [
          {
            id: '301',
            title: 'Is SSID the same as Wi-Fi address?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Wi-Fi and SSID are the same',
                correct: true
              },
              {
                id: 'A1',
                text: 'WiFi is in your home, SSID is in public',
                correct: false
              },
              {
                id: 'A2',
                text: 'SSID is a client site name, WiFi is local',
                correct: false
              },
              {
                id: 'A3',
                text: 'Yes, your SSID is your WiFi',
                correct: false
              }
            ],
            description:
              'A)Wi-Fi and SSID are not the same, but they are related. Wi-Fi refers to the technology that allows devices to connect to a network wirelessly, while an SSID is the name assigned to a specific Wi-Fi network.'
          },
          {
            id: '308',
            title: 'What does "LAN" Stand for?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Large Access Network',
                correct: false
              },
              {
                id: 'A1',
                text: 'Local Area Network',
                correct: true
              },
              {
                id: 'A2',
                text: 'Long Area Network',
                correct: false
              },
              {
                id: 'A3',
                text: 'Limited Access Node',
                correct: false
              }
            ],
            description:
              'B) "LAN" Stands for Local Area Network. A good example of a LAN is a Home Network of Devices that is connected to a Homes Modem Router'
          },
          {
            id: '311',
            title: "What's the maximum length of an IPv4 address?",
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '16 Characters',
                correct: false
              },
              {
                id: 'A1',
                text: '32 Characters',
                correct: true
              },
              {
                id: 'A2',
                text: '64 Characters',
                correct: false
              },
              {
                id: 'A3',
                text: '128 Characters',
                correct: false
              }
            ],
            description: 'B) The maximum length of an IPv4 Address is 32 Characters'
          }
        ]
      },
      finished: false
    },
    {
      id: 'ADT+ and SHSS',
      key: 'OlympicsTriviaTopic5',
      name: 'ADT+ and SHSS',
      bgImage: orange,
      bgImageXs: orangeXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '402',
            title: 'Is it possible to view live video from Nest Cams and Doorbells through the ADT+ app?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'No, only saved clips are visible',
                correct: false
              },
              {
                id: 'A1',
                text: 'Yes from Home.Google.com',
                correct: false
              },
              {
                id: 'A2',
                text: 'Yes, you can stream live video through Nest Cams ',
                correct: true
              },
              {
                id: 'A3',
                text: 'Yes, on your Smart TV only',
                correct: false
              }
            ],
            description: 'C)Yes, you can stream live video through Nest Cams using the ADT+ app.'
          },
          {
            id: '404',
            title: 'What types of user roles can be added to the ADT+ app?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Primary, Admin, Standard, Basic, and Duress',
                correct: true
              },
              {
                id: 'A1',
                text: 'Master, Admin, Standard, Basic, and Duress',
                correct: false
              },
              {
                id: 'A2',
                text: 'Each role is assigned 1 key fob',
                correct: false
              },
              {
                id: 'A3',
                text: 'Primary, Master, Standard and Basic are the roles',
                correct: false
              }
            ],
            description: 'A) Primary, Admin, Standard, Basic, and Duress'
          },
          {
            id: '405',
            title: 'Can I manage my emergency contacts through the ADT+ app?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: "SSU doesn't have emergency contacts ",
                correct: false
              },
              {
                id: 'A1',
                text: 'Yes, you can manage emergency contacts ',
                correct: true
              },
              {
                id: 'A2',
                text: 'No they must call the 800#',
                correct: false
              },
              {
                id: 'A3',
                text: 'ADT+ can add contacts but not change phone #s',
                correct: false
              }
            ],
            description: 'B)Yes you can manage emergency contacts through the ADT+ App'
          },
          {
            id: '407',
            title: 'Does the ADT+ app offer a way to download my alarm monitoring certificate for insurance purposes?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'No, we do not provide a certificate',
                correct: false
              },
              {
                id: 'A1',
                text: 'No, ordinances do not cover DIY alarms',
                correct: false
              },
              {
                id: 'A2',
                text: 'It can only be mailed',
                correct: false
              },
              {
                id: 'A3',
                text: 'Yes, it can be downloaded from ADT+ App ',
                correct: true
              }
            ],
            description: 'D)Yes, it can be downloaded from the ADT+ App'
          },
          {
            id: '409',
            title: 'Are there any features related to Google integration available in the ADT+ app?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Of course',
                correct: false
              },
              {
                id: 'A1',
                text: 'No',
                correct: true
              },
              {
                id: 'A2',
                text: 'Those features are being developed now',
                correct: false
              },
              {
                id: 'A3',
                text: 'Yes, ADT+ uses your Google acct sign-in',
                correct: false
              }
            ],
            description:
              'B)No, The ADT+ app does not support Google integration features such as Account Linking, Google Sign In, and Manage Subscription Plan.'
          }
        ],
        Silver: [
          {
            id: '400',
            title: 'What is the ADT+ app designed for?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Integrates the security of ADT with google',
                correct: true
              },
              {
                id: 'A1',
                text: 'ADT+ will replace all other ADT apps',
                correct: false
              },
              {
                id: 'A2',
                text: ' ADT+ will remove the need for Roundsmen',
                correct: false
              },
              {
                id: 'A3',
                text: 'Homeowners, Renters and business owners',
                correct: false
              }
            ],
            description:
              'A)The ADT+ app integrates the security of ADT with the helpfulness of Nest Camsand Thermostats, allowing control over ADT Self Setup or smart home security systems, Google Nest devices, and other compatible smart devices.'
          },
          {
            id: '403',
            title: 'How can I manage alerts and notifications for my ADT+ system?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: "By calling ADT's direct number 1-800-ADT-ASAP",
                correct: false
              },
              {
                id: 'A1',
                text: 'From your alarm.com portal',
                correct: false
              },
              {
                id: 'A2',
                text: "You can manage alert in the Alarm Hub's menu",
                correct: false
              },
              {
                id: 'A3',
                text: 'ADT+ App or plus.adt.com',
                correct: true
              }
            ],
            description: 'D) ADT+ App or plus.adt.com'
          },
          {
            id: '406',
            title: 'Is it possible to pay my ADT bill and enroll in AutoPay via the ADT+ app?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'No, billing handled by ADT at 888-392-2039',
                correct: false
              },
              {
                id: 'A1',
                text: 'No, billing is mailed ',
                correct: false
              },
              {
                id: 'A2',
                text: 'Yes, you can pay your bill and enroll in AutoPay',
                correct: true
              },
              {
                id: 'A3',
                text: 'It cannot take CC information on your phone',
                correct: false
              }
            ],
            description:
              'C)Yes, you can pay your bill and enroll in AutoPay directly through the ADT+ app or at plus.adt.com.'
          },
          {
            id: '410',
            title: 'What is the primary function of the ADT Base in the ADT Smart Home Security system?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'To connect to customers phones',
                correct: false
              },
              {
                id: 'A1',
                text: 'To bypass any open doors and still arm system',
                correct: false
              },
              {
                id: 'A2',
                text: 'The brains of the security system',
                correct: true
              },
              {
                id: 'A3',
                text: 'The main Thread border router',
                correct: false
              }
            ],
            description:
              'C)The ADT Base acts as the brain of the security system, providing connectivity for sensors and automation devices through the ADT+ app'
          }
        ],
        Gold: [
          {
            id: '401',
            title: 'Can I arm and disarm my ADT system remotely using the ADT+ app?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Yes by asking ADT+ to disarm',
                correct: false
              },
              {
                id: 'A1',
                text: 'Yes, you can arm and disarm using the ADT+ app.',
                correct: true
              },
              {
                id: 'A2',
                text: 'No remote access to arm/disarm',
                correct: false
              },
              {
                id: 'A3',
                text: 'No, only from the keypad',
                correct: false
              }
            ],
            description: 'B)Yes, you can arm and disarm the ADT system from virtually anywhere using the ADT+ app.'
          },
          {
            id: '408',
            title: 'Can I control smart locks and thermostats through the ADT+ app?',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Yes, you can control smart locks and thermostats',
                correct: true
              },
              {
                id: 'A1',
                text: 'No, you can only control lighting',
                correct: false
              },
              {
                id: 'A2',
                text: 'No, It is a feature coming soon',
                correct: false
              },
              {
                id: 'A3',
                text: 'No, they would be controlled with Control App',
                correct: false
              }
            ],
            description: 'A)Yes, you can control door locks and thermostats'
          },
          {
            id: '411',
            title: "How does the ADT Base indicate the system's status?",
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Rapid beeping and siren',
                correct: false
              },
              {
                id: 'A1',
                text: 'Built-in speaker emits a 85db siren',
                correct: false
              },
              {
                id: 'A2',
                text: 'A strobe built in with 9V AC power',
                correct: false
              },
              {
                id: 'A3',
                text: 'Uses a repsonsive LED Halo',
                correct: true
              }
            ],
            description: 'D) uses a responsive LED halo '
          }
        ]
      },
      finished: false
    },
    {
      id: 'General ADT',
      key: 'OlympicsTriviaTopic6',
      name: 'General ADT',
      bgImage: red,
      bgImageXs: redXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: '500',
            title: 'ADT was founded in what year?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '1874',
                correct: true
              },
              {
                id: 'A1',
                text: '1784',
                correct: false
              },
              {
                id: 'A2',
                text: '1487',
                correct: false
              },
              {
                id: 'A3',
                text: '1847',
                correct: false
              }
            ],
            description: 'A)1874'
          },
          {
            id: '501',
            title: 'ADT secures how many homes world-wide?',
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Over 203 million and is #1 choice amongst consumer',
                correct: false
              },
              {
                id: 'A1',
                text: 'Over 102 million and is #1 choice amongst consumer',
                correct: false
              },
              {
                id: 'A2',
                text: 'Over 28 million and is #1 choice amongst consumers',
                correct: false
              },
              {
                id: 'A3',
                text: 'Over 25 million and is #1 choice amongst consumers',
                correct: true
              }
            ],
            description: 'D)Over 25 million and is the #1 choice amongst consumers'
          },
          {
            id: '505',
            title: "What is the moon's favorite type of gum?",
            miles: 5,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Big Red',
                correct: false
              },
              {
                id: 'A1',
                text: 'Fruit Stripe',
                correct: false
              },
              {
                id: 'A2',
                text: 'Big League Chew',
                correct: false
              },
              {
                id: 'A3',
                text: 'Eclipse',
                correct: true
              }
            ],
            description: 'D)Eclipse'
          }
        ],
        Silver: [
          {
            id: '502',
            title: 'Homes without security systems are...?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Not protected to the fullest',
                correct: false
              },
              {
                id: 'A1',
                text: 'Common in the U.S.',
                correct: false
              },
              {
                id: 'A2',
                text: '3 times more likely to be burglarized',
                correct: true
              },
              {
                id: 'A3',
                text: 'Not safe for friends or family',
                correct: false
              }
            ],
            description: 'C)3 times more likely to be burglarized'
          },
          {
            id: '503',
            title: 'ADT secures over ...?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Garages',
                correct: false
              },
              {
                id: 'A1',
                text: '90% of the Fortune 500 companies',
                correct: true
              },
              {
                id: 'A2',
                text: 'All of Puerto Rico',
                correct: false
              },
              {
                id: 'A3',
                text: '75% of American housholds',
                correct: false
              }
            ],
            description: 'B)90% of the Fortune 500 companies'
          },
          {
            id: '504',
            title: 'ADT has how many monitoring centers?',
            miles: 10,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '12 monitoring centers located across North America',
                correct: true
              },
              {
                id: 'A1',
                text: 'Over 200 locations throughout the U.S',
                correct: false
              },
              {
                id: 'A2',
                text: 'At least 1 in each state',
                correct: false
              },
              {
                id: 'A3',
                text: '6 in North America; 4 in the US and 2 in Canada',
                correct: false
              }
            ],
            description:
              'A)We own 12 state-of-the-art monitoring centers located across North America—including Network Operations Centers'
          }
        ],
        Gold: [
          {
            id: '506',
            title: 'What is replacing Google Mesh WiFi in our product line-up',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Asus ROG Rapture GT6',
                correct: false
              },
              {
                id: 'A1',
                text: 'eero Pro 6E',
                correct: false
              },
              {
                id: 'A2',
                text: 'Netgear Nighthawk',
                correct: true
              },
              {
                id: 'A3',
                text: 'TP-Link Deco W7200',
                correct: false
              }
            ],
            description: 'C)Netgear Nighthawk Mesh Wi-Fi 6 Router MR70'
          },

          {
            id: '507',
            title: 'What is replacing Google Mesh WiFi in our product line-up',
            miles: 20,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Asus ROG Rapture GT6',
                correct: false
              },
              {
                id: 'A1',
                text: 'eero Pro 6E',
                correct: false
              },
              {
                id: 'A2',
                text: 'Netgear Nighthawk',
                correct: true
              },
              {
                id: 'A3',
                text: 'TP-Link Deco W7200',
                correct: false
              }
            ],
            description: 'C)Netgear Nighthawk Mesh Wi-Fi 6 Router MR70'
          }
        ]
      },
      finished: false
    }
  ]
};
