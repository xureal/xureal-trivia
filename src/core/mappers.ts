import { DataKeys, AllUserData, Result } from './types';
import { topicKeys } from './mockData';

const questionAnswerMap = {
  C: true,
  I: false
};

export const answerResultMap = new Map<boolean, Result>([
  [true, 'CORRECT'],
  [false, 'INCORRECT']
]);

export const answerResultApiMap = new Map<boolean, 'C' | 'I'>([
  [true, 'C'],
  [false, 'I']
]);

export const userDataToAnsweredQuestionsIds = (allUserData: AllUserData) => {
  const answeredQuestionsIds = new Map<DataKeys, { id: string; correct: boolean | undefined }[]>();
  allUserData.forEach((topic) => {
    const topicKey = topic.id;

    if (
      topicKeys.includes(
        topicKey as Exclude<
          DataKeys,
          'OlympicsTriviaTimestamp' | 'OlympicsTriviaCorrectQuestions' | 'OlympicsTriviaMedals'
        >
      ) &&
      topic?.data?.Value
    ) {
      const topicQuestion = topic.data.Value.split(',');
      topicQuestion.forEach((questionIds) => {
        const [id, correct] = questionIds.split('_');
        const topicData = {
          id,
          correct: questionAnswerMap[correct as 'C' | 'I']
        };
        if (answeredQuestionsIds.has(topicKey)) {
          answeredQuestionsIds.get(topicKey)!.push(topicData);
        } else {
          answeredQuestionsIds.set(topicKey, [topicData]);
        }
      });
    }
  });

  return answeredQuestionsIds;
};
