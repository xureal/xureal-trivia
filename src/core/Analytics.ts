import { Answer, Api, DataKeys, Medal, Question, Topic } from './types';
import { answerResultApiMap, answerResultMap } from './mappers';

/** @deprecated analytics are no longer used */
class Analytics {
  constructor(private api: Api) {
    this.gameStarted = this.gameStarted.bind(this);
    this.answerQuestion = this.answerQuestion.bind(this);
    this.topicSelected = this.topicSelected.bind(this);
    this.gameOver = this.gameOver.bind(this);
  }

  /** @deprecated analytics are no longer used */
  gameStarted() {
    this.api.gameEventStart();
  }

  /** @deprecated analytics are no longer used */
  async answerQuestion(topicKey: DataKeys, qId: Question['id'], aId: Answer['id'], correct: boolean) {
    this.api.gameEventQuestion(
      topicKey,
      `${qId}_${answerResultApiMap.get(correct)}`,
      aId,
      answerResultMap.get(correct)!
    );
  }

  /** @deprecated analytics are no longer used */
  topicSelected(topicName: Topic['name']) {
    this.api.gameEventCategory(topicName);
  }

  /** @deprecated analytics are no longer used */
  gameOver(medal: Medal | '' | string, completedQuestions: number, totalQuestion: number) {
    this.api.gameEventOver(medal, completedQuestions, totalQuestion);
  }
}

export default Analytics;
