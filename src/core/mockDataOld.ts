import { Quiz } from './types';
import yellow from 'assets/activeStates/yellow-active.png';
import yellowXs from 'assets/activeStates/xs/yellow-xs-active.png';
import orange from 'assets/activeStates/orange-active.png';
import orangeXs from 'assets/activeStates/xs/orange-xs-active.png';
import purple from 'assets/activeStates/purple-active.png';
import purpleXs from 'assets/activeStates/xs/purple-xs-active.png';
import red from 'assets/activeStates/red-active.png';
import redXs from 'assets/activeStates/xs/red-xs-active.png';
import blue from 'assets/activeStates/blue-active.png';
import blueXs from 'assets/activeStates/xs/blue-xs-active.png';
import green from 'assets/activeStates/green-active.png';
import greenXs from 'assets/activeStates/xs/green-xs-active.png';

export const topicKeys = [
  'OlympicsTriviaTopic1',
  'OlympicsTriviaTopic2',
  'OlympicsTriviaTopic3',
  'OlympicsTriviaTopic4',
  'OlympicsTriviaTopic5',
  'OlympicsTriviaTopic6'
] as const;

export const question = {
  id: '1',
  title: '',
  miles: 0,
  bonus: false,
  answers: [
    { id: '1', text: 'Lorem ipsum dolor sit amet cor', correct: true },
    { id: '2', text: 'Lorem ipsum dolor sit amet cor', correct: false },
    { id: '3', text: 'Lorem ipsum dolor sit amet cor', correct: false },
    { id: '4', text: 'Lorem ipsum dolor sit amet cor', correct: false }
  ],
  description: 'Test'
};

export const quiz: Quiz = {
  id: 'trivia-demo',
  miles: 0,
  wait: false,
  progress: 0,
  medals: '',
  topics: [
    {
      id: 'Employee Engagement',
      key: 'OlympicsTriviaTopic1',
      name: 'Employee Engagement',
      bgImage: purple,
      bgImageXs: purpleXs,
      questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q169',
            title: 'What is the percentage of engaged employees in the US today?',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '50%', correct: false },
              { id: 'A1', text: '80%', correct: false },
              { id: 'A2', text: '36%', correct: true },
              { id: 'A3', text: '73%', correct: false }
            ],
            description:
              'The best workplace is the one that cares for its employees, then positions employee engagement as the catalyst for improving important business outcomes.'
          },
          {
            id: 'Q170',
            title: 'Which of the following is a strategy to improve employee engagement?',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'Free food', correct: false },
              { id: 'A1', text: 'Offer excessive over time', correct: false },
              {
                id: 'A2',
                text: 'Empower managers to drive enegagement',
                correct: true
              },
              { id: 'A3', text: 'Work weekends', correct: false }
            ],
            description:
              'The best workplace is the one that cares for its employees, then positions employee engagement as the catalyst for improving important business outcomes.'
          }
        ],
        Silver: [
          {
            id: 'Q171',
            title: 'How much do disengaged employees cost the American Economy?',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '$200 Million', correct: false },
              { id: 'A1', text: '$350 Billion', correct: true },
              { id: 'A2', text: '$50 Billion', correct: false },
              { id: 'A3', text: '$700 Million', correct: false }
            ],
            description:
              'The best workplace is the one that cares for its employees, then positions employee engagement as the catalyst for improving important business outcomes.'
          }
        ],
        Gold: [
          {
            id: 'Q172',
            title: 'Highly engaged employees are ________less likely to leave the organization',
            miles: 100,
            bonus: false,
            answers: [
              { id: 'A0', text: '10%', correct: false },
              { id: 'A1', text: '66%', correct: false },
              { id: 'A2', text: '50%', correct: false },
              { id: 'A3', text: '87%', correct: true }
            ],
            description:
              'The best workplace is the one that cares for its employees, then positions employee engagement as the catalyst for improving important business outcomes.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Training and Retention',
      key: 'OlympicsTriviaTopic2',
      name: 'Training and Retention',
      bgImage: blue,
      bgImageXs: blueXs,
      questionBg: 'linear-gradient(90deg, #1F69FF 0%, #508AFE 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q183',
            title: '85% of employees want to choose training times that fit their schedule',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'The Xureal platform can be accessed from anywhere and anytime, givin employees the freedom to learn and grow at their own pace. '
          },
          {
            id: 'Q184',
            title: '1 in 3 employees says their organization’s training is out-of-date.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Outdated and boring training applications can leave your employees feeling inadequate. Xureal leverages all of your existing training content and maps it into an immersive virtual experience. '
          }
        ],
        Silver: [
          {
            id: 'Q185',
            title:
              '_______of workers believe regular and frequent training is more important than formal workplace training.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '20%', correct: false },
              { id: 'A1', text: '45%', correct: false },
              { id: 'A2', text: '80%', correct: true },
              { id: 'A3', text: '12%', correct: false }
            ],
            description:
              'Xureal makes employee training possible on a daily basis through its edutainment and engagement features.'
          }
        ],
        Gold: [
          {
            id: 'Q186',
            title: 'Retention rates rise 30-50% for companies with strong learning cultures.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'A learning culture is crucial for retaining talent. Xureal ensures your teams are always learning without the training burnout.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Employee Onboarding',
      key: 'OlympicsTriviaTopic3',
      name: 'Employee Onboarding',
      bgImage: green,
      bgImageXs: greenXs,
      questionBg: 'linear-gradient(90deg, #008558 0%, #30B689 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q201',
            title: "88% of employees don't believe their organizations do a great job of onboarding.",
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Given that a strong onboarding process can result in higher retention and productivity, Xureal was designed to provide a unique onboarding journey to new employees. '
          },
          {
            id: 'Q202',
            title: 'Organizations with poor onboarding processes are twice as likely to experience employee turnover.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Given that a strong onboarding process can result in higher retention and productivity, Xureal was designed to provide a unique onboarding journey to new employees. '
          }
        ],
        Silver: [
          {
            id: 'Q203',
            title: 'Addressing development during onboarding increases satisfaction by as much as_____',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '2X', correct: false },
              {
                id: 'A1',
                text: '1.5X',
                correct: false
              },
              {
                id: 'A2',
                text: '3.5X',
                correct: true
              }
            ],
            description:
              'Career development continues to be the leading reason people leave their jobs. Learning and development are at the heart of the Xureal platform, making it easy for employees to stay on a growth path.'
          }
        ],
        Gold: [
          {
            id: 'Q204',
            title: 'Great employee onboarding can improve employee retention by as much as',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '15%', correct: false },
              { id: 'A1', text: '37%', correct: false },
              { id: 'A2', text: '60%', correct: false },
              { id: 'A3', text: '82%', correct: true }
            ],
            description:
              'Talent scarcity and high turnover are among the biggest challenges organizations will face in 2022—so it’s important to engage and retain employees from day 1.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Xureal Fun Facts',
      key: 'OlympicsTriviaTopic4',
      name: 'Xureal Fun Facts',
      bgImage: yellow,
      bgImageXs: yellowXs,
      questionBg: 'linear-gradient(90deg, #FFAA00 0%, #F4BB47 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q211',
            title: 'How many unique user sessions were hosted on XUREAL during 2021?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '500K',
                correct: false
              },
              { id: 'A1', text: '1.3M', correct: true },
              {
                id: 'A2',
                text: '800K',
                correct: false
              },
              { id: 'A3', text: '150K', correct: false }
            ],
            description:
              'Xureal is a growing platform on a mission to define the future of employee engagement and training.'
          },
          {
            id: 'Q212',
            title: 'How many times training videos have been watched on the Xureal platform?',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '2.5 million',
                correct: true
              },
              { id: 'A1', text: '280,000', correct: false },
              {
                id: 'A2',
                text: '750,000',
                correct: false
              },
              { id: 'A3', text: '1 million', correct: false }
            ],
            description:
              "Engaged employees are more likely to participate and grow within an organization. Xureal's immersive gamification features make content consumption more attractive to employees. "
          }
        ],
        Silver: [
          {
            id: 'Q213',
            title: 'How many training assets have been uploaded to the XUREAL Application? ',
            miles: 90,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: '75k',
                correct: false
              },
              { id: 'A1', text: '5K', correct: false },
              {
                id: 'A2',
                text: '100K+',
                correct: true
              },
              { id: 'A3', text: '20K', correct: false }
            ],
            description:
              'Our entreprise-level content management system allows our customers to upload and maintain their traning content easily.'
          }
        ],
        Gold: [
          {
            id: 'Q214',
            title: "Xureal's Metaverse worlds can be accessed from....",
            miles: 100,
            bonus: false,
            answers: [
              {
                id: 'A0',
                text: 'Smart Phones',
                correct: false
              },
              { id: 'A1', text: 'Tablets', correct: false },
              {
                id: 'A2',
                text: 'Desktops',
                correct: false
              },
              { id: 'A3', text: 'All of the above', correct: true }
            ],
            description:
              'The Xureal platform is entirely web-based and does not require the download or installation of applications or plugins, saving time and resources on IT and app deployments.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Diversity, Equity and Inclusion',
      key: 'OlympicsTriviaTopic5',
      name: 'Diversity, Equity and Inclusion',
      bgImage: orange,
      bgImageXs: orangeXs,
      questionBg: 'linear-gradient(90deg, #E64F00 0%, #ED6E2C 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q220',
            title: '65% of employees think they would be more productive at home than in the office.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'By redesigning the virtual and hybrid workplace with humans at the center, organizations can improve the worker experience and deliver real business results.'
          },
          {
            id: 'Q221',
            title: "A strong organizational culture can increase an organization's profit by as much as ______",
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '85%', correct: true },
              { id: 'A1', text: '47%', correct: false },
              { id: 'A2', text: '65%', correct: false },
              { id: 'A3', text: '20%', correct: false }
            ],
            description:
              'Highly engaged employees make a strong organizational culture. Xureal is designed to keep your teams connected to your brand at all times.'
          }
        ],
        Silver: [
          {
            id: 'Q222',
            title: 'How much of Generation Z are racial or ethnic minorities?',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '10%', correct: false },
              { id: 'A1', text: '22%', correct: false },
              { id: 'A2', text: '36%', correct: false },
              { id: 'A3', text: '48%', correct: true }
            ],
            description:
              'Elevate your diversity and inclusion efforts by bringing all your initiatives into a permanent virtual experience.'
          }
        ],
        Gold: [
          {
            id: 'Q223',
            title: 'More than 3 out of 4 workers prefer diverse companies',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Show your employees how much you care about diversity by promoting virtual experiences around DEI.'
          }
        ]
      },
      finished: false
    },
    {
      id: 'Wellness',
      key: 'OlympicsTriviaTopic6',
      name: 'Wellness',
      bgImage: red,
      bgImageXs: redXs,
      questionBg: 'linear-gradient(90deg, #E6004D 0%, #DB346C 100%)',
      questions: {
        Bronze: [
          {
            id: 'Q231',
            title:
              '77% of employees in the U.S. disagree that corporate fitness programs are excellent for a company’s culture.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: false },
              { id: 'A1', text: 'FALSE', correct: true }
            ],
            description:
              'According to Virgin HealthMiles Inc. and Workforce Management Magazine, 77% of workers believe that following workplace wellness trends positively affects the company’s culture.'
          },
          {
            id: 'Q232',
            title: '63% of companies with wellness programs have reported better financial sustainability and growth.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Employee wellness statistics from 2017 revealed that more than 60% of companies that implemented wellness programs improved their sustainability and growth.'
          }
        ],
        Silver: [
          {
            id: 'Q233',
            title:
              'More than _____ of employees appreciate their leaders’ support in pursuing health and wellness programs in the workplace.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: '10%', correct: false },
              { id: 'A1', text: '40%', correct: false },
              { id: 'A2', text: '90%', correct: true },
              { id: 'A3', text: '100%', correct: false }
            ],
            description:
              'Workplace wellness statistics show that over nine out of ten employees are more motivated to do their duties when they know that their managers support their wellbeing efforts.'
          }
        ],
        Gold: [
          {
            id: 'Q234',
            title: '25% of workers feel drained after an average day at work.',
            miles: 90,
            bonus: false,
            answers: [
              { id: 'A0', text: 'TRUE', correct: true },
              { id: 'A1', text: 'FALSE', correct: false }
            ],
            description:
              'Fatigue and exhaustion at the end of an average working day are the direct consequences of stress and pressure at work.'
          }
        ]
      },
      finished: false
    }
  ]
};
