import BlackBackground from 'assets/images/Loading.png';
import styled from 'styled-components';

type Props = {
  slantHeight: number;
};

const BackgroundStyles = {
  Container: styled.div`
    position: absolute;
    top: 0;
    height: 100vh;
    width: 100vw;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    z-index: -999;
  `,
  BackgroundImage: styled.img`
    height: 100%;
    width: 100vw;
    z-index: -950;
  `,
  BackgroundSlant: styled.div<{ $slantHeight: number }>`
    position: absolute;
    bottom: ${(props) => props.$slantHeight}px;
    height: 55%;
    width: 200vw;
    background-color: #e2e6ff;
    z-index: -800;
    transform: skewY(4deg);

    @media (max-width: 768px) {
      transform: skewY(6deg);
    }
  `
};

const SlantedBackground = ({ slantHeight }: Props) => {
  return (
    <BackgroundStyles.Container>
      <BackgroundStyles.BackgroundImage src={BlackBackground} alt='background' />
      <BackgroundStyles.BackgroundSlant $slantHeight={slantHeight} />
    </BackgroundStyles.Container>
  );
};

export default SlantedBackground;
