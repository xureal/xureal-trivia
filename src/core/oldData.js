// export const quiz_r1: Quiz = {
//     id: "comcast_olympics_trivia",
//     miles: 0,
//     wait: false,
//     progress: 0,
//     medals: '',
//     topics: [
//         {
//             id: "Control",
//             key: "OlympicsTriviaTopic1",
//             name: "Control",
//             bgImage: purple,
//             bgImageXs: purpleXs,
//             questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q80",
//                         title: "True or False: With Xfinity xFi you’ll know which devices are online",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "You can even get notifications when a new device joins your network"
//                     },
//                     {
//                         id: "Q81",
//                         title: "You can assign Devices names on your network with the Xfinity app",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "True. You can even create profiles for the people who use your WiFi most"
//                     },
//                     {
//                         id: "Q82",
//                         title: "You can assign devices you don’t want on your home network to one profile",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "True. You can then pause that profile indefinitely to block WiFi access."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q30",
//                         title: "What xFi feature detects how long you are active on the network",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Xfinity Assistant", correct: false },
//                             { id: "A1", text: "Personalize", correct: false },
//                             { id: "A2", text: "Speed Test", correct: false },
//                             { id: "A3", text: "Active time alert", correct: true }
//                         ],
//                         description: "With Active Time Alert, a primary user will receive a text message/email."
//                     },
//                     {
//                         id: "Q31",
//                         title: "Following connected devices are protected with xFi Advanced Security EXCEPT",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Wi-Fi speakers", correct: false },
//                             { id: "A1", text: "The garage door", correct: true },
//                             { id: "A2", text: "Gaming consoles and tablets", correct: false },
//                             { id: "A3", text: "Computers and Laptops", correct: false }
//                         ],
//                         description: "xFi Adv Security protects all connected devices. Garage door - not a device"
//                     },
//                     {
//                         id: "Q32",
//                         title: "What happens when the active time limit is reached?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Devices are paused", correct: true },
//                             { id: "A1", text: "Device are powered off", correct: false },
//                             { id: "A2", text: "Devices are disabled for 1 wk", correct: false },
//                             { id: "A3", text: "None of the above", correct: false }
//                         ],
//                         description: "Devices in that profile will be paused for the rest of the day"
//                     },
//                     {
//                         id: "Q33",
//                         title: "How many security threats should I expect to see with Adv Security",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "10", correct: false },
//                             { id: "A1", text: "50", correct: false },
//                             { id: "A2", text: "100", correct: false },
//                             { id: "A3", text: "None of the above", correct: true }
//                         ],
//                         description: "It's difficult to estimate a threat number, since each home is different"
//                     },
//                     {
//                         id: "Q34",
//                         title: "A category of threats that need immediate action is called",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Attention Required", correct: true },
//                             { id: "A1", text: "Awareness Only", correct: false },
//                             { id: "A2", text: "SOS", correct: false },
//                             { id: "A3", text: "Help!", correct: false }
//                         ],
//                         description: "All known threats are immediately blocked. Some require your attention"
//                     },
//                     {
//                         id: "Q35",
//                         title: "An xFi parental control feature that allow scheduling of your kids usage is",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Bedtime mode", correct: true },
//                             { id: "A1", text: "Active time alert", correct: false },
//                             { id: "A2", text: "Watchdog", correct: false },
//                             { id: "A3", text: "xFi secure", correct: false }
//                         ],
//                         description: "Bedtime mode allows parents to schedule pause of a child's access to Wi-Fi"
//                     },
//                     {
//                         id: "Q36",
//                         title: "An outside device tries to access another device connected to home network:",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Unauthorized access attempts", correct: true },
//                             { id: "A1", text: "Suspicious Site Visit", correct: false },
//                             { id: "A2", text: "IP Reputation Threats", correct: false },
//                             { id: "A3", text: "Targeted Network Attack", correct: false }
//                         ],
//                         description: "This is called an Unauthorized Access Attempt"
//                     },
//                     {
//                         id: "Q37",
//                         title: "Can I add or remove specific websites from Parental Controls for xFi?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Yes", correct: false },
//                             { id: "A1", text: "No", correct: true },
//                         ],
//                         description: "Not at this time. To access blocked content, turn off Parental Controls"
//                     },
//                     {
//                         id: "Q38",
//                         title: "Will my activity be tracked w/ active time when I use a private browser tab",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Yes, Active Time Details is still able to monitor activity on websites"
//                     },
//                     {
//                         id: "Q39",
//                         title: "Evan has Xfinity Services, can he correct/update Personally Identifiable?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Evan can correct or update information about his account at any time."
//                     },
//                     {
//                         id: "Q40",
//                         title: "As an xFi customer, Jay can get an alert when people connect to his network",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "xFi App alerts Jay to important events on his home network"
//                     }
//                 ],
//                 Gold: [
//                     {
//                         id: "Q59",
//                         title: "All of the following are key xFi features EXCEPT:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Pause WiFi", correct: false },
//                             { id: "A1", text: "Voice Remote", correct: true },
//                             { id: "A2", text: "Device Monitoring", correct: false },
//                             { id: "A3", text: "Bedtime Mode", correct: false }
//                         ],
//                         description: "Voice Remote comes with X1 & Flex which are available to all xFi customers."
//                     },
//                     {
//                         id: "Q60",
//                         title: "On average, how many devices do xFi customers connect in their homes?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "1", correct: false },
//                             { id: "A1", text: "7", correct: true },
//                             { id: "A2", text: "10", correct: false },
//                             { id: "A3", text: "14", correct: false }
//                         ],
//                         description: "xFi has the power to handle all those devices +Gaming, Thermostats + others"
//                     },
//                     {
//                         id: "Q61",
//                         title: "This Browsing type reduces devices from accessing malicious content",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Malware", correct: false },
//                             { id: "A1", text: "Safe", correct: false },
//                             { id: "A2", text: "Protected", correct: true },
//                             { id: "A3", text: "xFi", correct: false }
//                         ],
//                         description: "Protected Browsing is an opt-in service to help safeguard home network."
//                     }
//                 ]

//             },
//             finished: false
//         },
//         {
//             id: "Speed",
//             key: "OlympicsTriviaTopic2",
//             name: "Speed",
//             bgImage: blue,
//             bgImageXs: blueXs,
//             questionBg: 'linear-gradient(90deg, #1F69FF 0%, #508AFE 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q77",
//                         title: "How much Speed does a device need for Streaming 4K Video",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "1Mbps", correct: false },
//                             { id: "A1", text: "3 Mbps", correct: false },
//                             { id: "A2", text: "5 Mbps", correct: false },
//                             { id: "A3", text: "25 Mbps", correct: true }
//                         ],
//                         description: "Depending on activity, stream 4K video typically takes 25 Mbps"
//                     },
//                     {
//                         id: "Q78",
//                         title: "How much Speed does a device need for Streaming HD Video",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "1Mbps", correct: false },
//                             { id: "A1", text: "3 Mbps", correct: false },
//                             { id: "A2", text: "5 Mbps", correct: true },
//                             { id: "A3", text: "25 Mbps", correct: false }
//                         ],
//                         description: "Depending on activity, stream HD Video typically takes 5 Mbps"
//                     },
//                     {
//                         id: "Q79",
//                         title: "How much Speed does a device need for sending/receiving email",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "1Mbps", correct: true },
//                             { id: "A1", text: "3 Mbps", correct: false },
//                             { id: "A2", text: "5 Mbps", correct: true },
//                             { id: "A3", text: "25 Mbps", correct: false }
//                         ],
//                         description: "Depending on activity, send/receive email typically takes 1 Mbps"
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q52",
//                         title: "What Xfinity Internet speed do you need to utilize xFi Adv Gateway(XB6)",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Any speed", correct: false },
//                             { id: "A1", text: "200 Mbps or faster", correct: true },
//                             { id: "A2", text: "100 Bits per second", correct: false },
//                             { id: "A3", text: "Internet Essentials", correct: false }
//                         ],
//                         description: "The XB6 isn’t available to all customers - only with speeds of 200Mbps +"
//                     },
//                     {
//                         id: "Q53",
//                         title: "When you BYOD for Xfinity Internet - you miss all the smart features EXCEPT",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Tech Support", correct: false },
//                             { id: "A1", text: "Parental Controls", correct: false },
//                             { id: "A2", text: "Advanced Security", correct: false },
//                             { id: "A3", text: "Speed", correct: true }
//                         ],
//                         description: "With their own equipment, customers will still get fast Internet speeds"
//                     },
//                     {
//                         id: "Q54",
//                         title: "All of the following affect Internet speeds EXCEPT:",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Number of devices", correct: false },
//                             { id: "A1", text: "Internet usage", correct: false },
//                             { id: "A2", text: "Weather", correct: true },
//                             { id: "A3", text: "Equipment", correct: false }
//                         ],
//                         description: "Weather doesn't affect speeds. Internet usage, age/quipment quality does."
//                     },
//                     {
//                         id: "Q55",
//                         title: "Which does NOT need to be considered to determine Internet speed needs?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Types of devices", correct: false },
//                             { id: "A1", text: "Number of devices", correct: false },
//                             { id: "A2", text: "Websites visited", correct: true },
//                             { id: "A3", text: "Streaming habits", correct: false }
//                         ],
//                         description: "Number and type of devices & streaming habits help determine speed needs."
//                     },
//                     {
//                         id: "Q56",
//                         title: "What is the best way to explain the benefits of xFi?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Connected Everywhere", correct: false },
//                             { id: "A1", text: "Speed, Coverage, Control", correct: true },
//                             { id: "A2", text: "Ultimate Security", correct: false },
//                             { id: "A3", text: "Speed, Simple, Digital", correct: false }
//                         ],
//                         description: "Xfinity has the Fastest Internet, largest Gig-speed network & WiFi control"
//                     },
//                     {
//                         id: "Q57",
//                         title: "Xfinity has offered just 3 speed increases in the past 17 years.",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "False. Xfinity has had 17 speed increases in the past 17 years!"
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q70",
//                         title: "What speed would you recommend to Ron, a customer who only has six devices?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "1 Gig", correct: false },
//                             { id: "A1", text: "500 bps", correct: false },
//                             { id: "A2", text: "150 to 200 Mbps", correct: true },
//                             { id: "A3", text: "Dial up", correct: false }
//                         ],
//                         description: "Recommend 150 to 200 Mbps. Ron can calculate his own speed using speed test"
//                     },
//                     {
//                         id: "Q71",
//                         title: "Recommendation to connect more than 12 devices at once to your Network:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Less devices", correct: false },
//                             { id: "A1", text: "Gig Internet", correct: true },
//                             { id: "A2", text: "xFi Pods", correct: false },
//                             { id: "A3", text: "Flex", correct: false }
//                         ],
//                         description: "Ultra-fast 1000Mbps , Gig Internet can handle 12 devices and more."
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Advanced Security",
//             key: "OlympicsTriviaTopic3",
//             name: "Advanced Security",
//             bgImage: green,
//             bgImageXs: greenXs,
//             questionBg: 'linear-gradient(90deg, #008558 0%, #30B689 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q0",
//                         title: "Xfinity Advanced Security benefits are the following",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Real-time threat notifications", correct: false },
//                             { id: "A1", text: "Block malware and spyware", correct: false },
//                             { id: "A2", text: "Protects devices in network", correct: false },
//                             { id: "A3", text: "All of the Above", correct: true }
//                         ],
//                         description: "Advanced Security is free for Internet customers who lease an xFi Gateway"
//                     },
//                     {
//                         id: "Q1",
//                         title: "Advanced Security only protects threats to customers’ smartphones & laptops",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "False. The xFi Gateway Advanced Security protects all connected devices."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q29",
//                         title: "What are the different types of threats prevented with xFi Adv Security?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Unauthorized access attempts", correct: false },
//                             { id: "A1", text: "Suspicious device/site visit", correct: false },
//                             { id: "A2", text: "IP Reputation Threats", correct: false },
//                             { id: "A3", text: "All of the Above", correct: true }
//                         ],
//                         description: "Advanced security also protects from targeted network attack"
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q74",
//                         title: "xFi dashboard shows devices that have been impacted by threats for the past",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "5", correct: false },
//                             { id: "A1", text: "7", correct: true },
//                             { id: "A2", text: "14", correct: false },
//                             { id: "A3", text: "21", correct: false }
//                         ],
//                         description: "xFi Dashboard provides a 7 day view."
//                     },
//                     {
//                         id: "Q75",
//                         title: "What xFi security threats does not require customers to take further action",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Targeted Network Attack", correct: false },
//                             { id: "A1", text: "Suspicious Device Activity", correct: false },
//                             { id: "A2", text: "Unauthorized", correct: false },
//                             { id: "A3", text: "IP Reputation Threats", correct: true }
//                         ],
//                         description: "xFi automatically blocks IP Reputation threat access attempts"
//                     },
//                     {
//                         id: "Q76",
//                         title: "How are threats detected with xFi Advanced Security",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Monitors Gateway activity", correct: false },
//                             { id: "A1", text: "Packet header data", correct: false },
//                             { id: "A2", text: "Metadata analysis", correct: false },
//                             { id: "A3", text: "Personal PII information", correct: true }
//                         ],
//                         description: "For your privacy, we don't gather personal information during this analysis"
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Xfinity App",
//             key: "OlympicsTriviaTopic4",
//             name: "Xfinity App",
//             bgImage: yellow,
//             bgImageXs: yellowXs,
//             questionBg: 'linear-gradient(90deg, #FFAA00 0%, #F4BB47 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q17",
//                         title: "Customers can stay connected & monitor their home with the what app?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "xFi Ap", correct: false },
//                             { id: "A1", text: "My Account App", correct: false },
//                             { id: "A2", text: "Xfinity App", correct: true },
//                             { id: "A3", text: "Xfinity Mobile App", correct: false }
//                         ],
//                         description: "Xfinity App is a single destination for all of your Xfinity Internet needs"
//                     },
//                     {
//                         id: "Q18",
//                         title: "Where can you check your Data usage",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "xFi App using Advanced Setting", correct: true },
//                             { id: "A1", text: "Xfinity App using Overview tab", correct: false },
//                             { id: "A2", text: "xFi App using Connect tab", correct: false },
//                             { id: "A3", text: "xFi App using Home tab", correct: false }
//                         ],
//                         description: "Xfinity app allows you see your monthly data usage"
//                     },
//                     {
//                         id: "Q19",
//                         title: "What are the benefits to using xFi services",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Schedule Bedtime Music", correct: false },
//                             { id: "A1", text: "Pause Wi-Fi at Hug Time", correct: true },
//                             { id: "A2", text: "Send SMS with vacation plans", correct: false },
//                             { id: "A3", text: "Set YouTube settings for kids", correct: false }
//                         ],
//                         description: "xFi App is the single destination to manage all of your WiFi Network"
//                     },
//                     {
//                         id: "Q20",
//                         title: "This Xfinity App character helps protect customers from cyber threats",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "xFi Knight", correct: true },
//                             { id: "A1", text: "xFi Robot", correct: false },
//                             { id: "A2", text: "xFi Superman", correct: false },
//                             { id: "A3", text: "xFi Cyberman", correct: false }
//                         ],
//                         description: "xFi Knight is the character symbolizing protection from cyber threats"
//                     },
//                     {
//                         id: "Q21",
//                         title: "What self service options are available in the Xfinity App?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Xfinity Bot", correct: false },
//                             { id: "A1", text: "Xfinity Assistant", correct: true },
//                             { id: "A2", text: "My xFi", correct: false },
//                             { id: "A3", text: "Xfinity Knight", correct: false }
//                         ],
//                         description: "Xfinity Assistant is a virtual assistant that gives you personalized help."
//                     },
//                     {
//                         id: "Q22",
//                         title: "Xfinity App allows you to pause your WiFi and check your data usage",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Xfinity App gives customers control of their home and manage WiFi network"
//                     },
//                     {
//                         id: "Q23",
//                         title: "True or False: Advanced security can be enabled thru MyAccount App",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "Advanced Security must be activated through the Xfinity app."
//                     },
//                     {
//                         id: "Q24",
//                         title: "xFi experience can be controlled via Xfinity app, website + X1 voice remote",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "xFi gives visibility & control over your home WiFi with any of these tools"
//                     },
//                     {
//                         id: "Q25",
//                         title: "True or False, Xfinity App allows you to check monthly data usage",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Check monthly data usage under 'View Network Details, See Network'"
//                     },
//                     {
//                         id: "Q26",
//                         title: "True or False, Xfinity App only blocks threats on your PC/Laptop",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "All devices connected to the network are protected including mobile phones"
//                     },
//                     {
//                         id: "Q27",
//                         title: "True or False, You cannot run a speed test using Xfinity App",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "Run a speed test for your whole home network using Xfinity App"
//                     },
//                     {
//                         id: "Q28",
//                         title: "With the Xfinity App, you can select any device to test a connection",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Select any device connected to the network to run a speed test"
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q58",
//                         title: "With Xfinity Mobile App, Joe can see what devices are on his network",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "False. Xfinity App allows Joe to see who is connected to his network."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q72",
//                         title: "What is the best way for customers to get answers to FAQs & more about xFi?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Xfinity On Demand", correct: false },
//                             { id: "A1", text: "Xfinity app", correct: true },
//                             { id: "A2", text: "Device settings", correct: false },
//                             { id: "A3", text: "Phone-A-Friend", correct: false }
//                         ],
//                         description: "The Xfinity app guides customers about managing their home network."
//                     },
//                     {
//                         id: "Q73",
//                         title: "What Xfinity product must you have to use the Xfinity app?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Voice", correct: false },
//                             { id: "A1", text: "Home", correct: false },
//                             { id: "A2", text: "TV", correct: false },
//                             { id: "A3", text: "Internet", correct: true }
//                         ],
//                         description: "Customers can set up home WiFi, troubleshoot, and even pause WiFi access"
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Coverage",
//             key: "OlympicsTriviaTopic5",
//             name: "Coverage",
//             bgImage: orange,
//             bgImageXs: orangeXs,
//             questionBg: 'linear-gradient(90deg, #E64F00 0%, #ED6E2C 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q2",
//                         title: "The best way to manage home WiFi network is through",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Xfinity App", correct: true },
//                             { id: "A1", text: "My Account App", correct: false },
//                             { id: "A2", text: "Xfinity Home", correct: false },
//                             { id: "A3", text: "None of the above", correct: false }
//                         ],
//                         description: "Xfinity App is the single destination to manage your home WiFi network"
//                     },
//                     {
//                         id: "Q3",
//                         title: "What does WiFi stand for?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Wireless Federation", correct: false },
//                             { id: "A1", text: "Wireless Fit", correct: false },
//                             { id: "A2", text: "Wireless First", correct: false },
//                             { id: "A3", text: "Wireless Fidelity", correct: true }
//                         ],
//                         description: "Wireless Fidelity, you can access or connect to a network using radio waves"
//                     },
//                     {
//                         id: "Q4",
//                         title: "For Xfinity’s best whole home WiFi coverage, Unlimited data and an xFi Pod",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Upgrade to Flex", correct: false },
//                             { id: "A1", text: "Upgrade to xFi Complete", correct: true },
//                             { id: "A2", text: "Upgrade to X1", correct: false },
//                             { id: "A3", text: "Upgrade to Self Protection", correct: false }
//                         ],
//                         description: "Xfinity’s best in-home WiFi can upgrade to xFi Complete for peace of mind"
//                     },
//                     {
//                         id: "Q5",
//                         title: "What year did Xfinity xFi Launch",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "2015", correct: false },
//                             { id: "A1", text: "2016", correct: false },
//                             { id: "A2", text: "2017", correct: true },
//                             { id: "A3", text: "2018", correct: false }
//                         ],
//                         description: "xFi was launched in 2017"
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q41",
//                         title: "Internet Essentials is available to customers who qualify for a low rate of",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "$2.99 per month", correct: false },
//                             { id: "A1", text: "$9.95 per month", correct: true },
//                             { id: "A2", text: "$50 per month", correct: false },
//                             { id: "A3", text: "Free", correct: false }
//                         ],
//                         description: "Internet Essentials includes 25 Mbps, No Term Contract & In-Home WiFi"
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q62",
//                         title: "How does WiFi work?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Connect to modem w/ Ethernet", correct: false },
//                             { id: "A1", text: "Via radio transmitters", correct: true },
//                             { id: "A2", text: "Magic", correct: false },
//                             { id: "A3", text: "All of the above", correct: false }
//                         ],
//                         description: "WiFi uses radio frequencies to send signals between devices."
//                     },
//                     {
//                         id: "Q63",
//                         title: "Jim is an xFi customer, is he eligible for the Internet Essentials program?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Yes", correct: false },
//                             { id: "A1", text: "No", correct: true },
//                         ],
//                         description: "Internet Essentials is $9.95/mo for families that meet income eligibility"
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Equipment",
//             key: "OlympicsTriviaTopic6",
//             name: "Equipment",
//             bgImage: red,
//             bgImageXs: redXs,
//             questionBg: 'linear-gradient(90deg, #E6004D 0%, #DB346C 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q6",
//                         title: "What is the cost for xFi Adv. Security if a customer leases an xFi Gateway?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "$4.99", correct: false },
//                             { id: "A1", text: "$11.99", correct: false },
//                             { id: "A2", text: "Free w/ leasing of xFi Gateway", correct: true },
//                             { id: "A3", text: "Free w/ Xfinity Internet", correct: false }
//                         ],
//                         description: "xFi Gateway also activates Advanced Security for extra layer of protection"
//                     },
//                     {
//                         id: "Q7",
//                         title: "Why should the customer lease an xFi gateway? ",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Best-in-class WiFi coverage", correct: false },
//                             { id: "A1", text: "Pause WiFi access to your home", correct: false },
//                             { id: "A2", text: "Protect in-network devices", correct: false },
//                             { id: "A3", text: "All of the above  ", correct: true }
//                         ],
//                         description: "xFi Gateway is an all-in-one modem + router for ease and simplicit"
//                     },
//                     {
//                         id: "Q8",
//                         title: "How are Pods different from WiFi extenders?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "An adaptive WiFi network", correct: false },
//                             { id: "A1", text: "Simple activation & management", correct: false },
//                             { id: "A2", text: "A and B Only", correct: true },
//                             { id: "A3", text: "No difference", correct: false }
//                         ],
//                         description: "Pods create a mesh WiFi network, which provides several key benefits"
//                     },
//                     {
//                         id: "Q9",
//                         title: "What is the cost for xFi Advanced Security if you rent the xFi Gateway?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "FREE", correct: true },
//                             { id: "A1", text: "$5.00", correct: false },
//                             { id: "A2", text: "$5.00 per month", correct: false },
//                             { id: "A3", text: "$7.99 per month", correct: false }
//                         ],
//                         description: "With xFi Gateway, security comes standard at no additional cost."
//                     },
//                     {
//                         id: "Q10",
//                         title: "These devices enhance WiFi connectivity throughout the home:",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "WiFi Antenna", correct: false },
//                             { id: "A1", text: "xFi Pods", correct: true },
//                             { id: "A2", text: "Voice Remote", correct: false },
//                             { id: "A3", text: "Xfinity App", correct: false }
//                         ],
//                         description: "xFi pods help to enhance connectivity & eliminate dead zones"
//                     },
//                     {
//                         id: "Q11",
//                         title: "What type of network does an xFi Pod provide?",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "5G", correct: false },
//                             { id: "A1", text: "4G LTE", correct: false },
//                             { id: "A2", text: "Mesh", correct: true },
//                             { id: "A3", text: "2G", correct: false }
//                         ],
//                         description: "xFi Pods create a mesh WiFi network= greater coverage & central management"
//                     },
//                     {
//                         id: "Q12",
//                         title: "xFi Gateway + xFi Pod works to extends WiFi networks & eliminate dead spots",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "xFi Gateway + xFi Pod provides wall-to-wall coverage throughout the home"
//                     },
//                     {
//                         id: "Q13",
//                         title: "xFi Gateway includes 24/7 device protection and automatic security updates.",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "True. The xFi Gateway Advanced Security protects all connected devices."
//                     },
//                     {
//                         id: "Q14",
//                         title: "xFi Gateway & Advanced Security work with xFi Pods to eliminate deadspots.",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "xFi Gateway + Pod= create a mesh WiFi network - helps eliminates dead zones"
//                     },
//                     {
//                         id: "Q15",
//                         title: "xFi protects all devices that are connected to the xFi Gateway.",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "True. The xFi Gateway Advanced Security protects all connected devices."
//                     },
//                     {
//                         id: "Q16",
//                         title: "xFi Gateway requires a technician to come to your home to install.",
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "It's quick & easy to self-Install Xfinity Internet with an xFi Gateway."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q42",
//                         title: "What counts as 'active time'?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Streaming Music/Downloading", correct: false },
//                             { id: "A1", text: "Shopping", correct: false },
//                             { id: "A2", text: "Gaming", correct: false },
//                             { id: "A3", text: "All of the Above", correct: true }
//                         ],
//                         description: "Active time also includes downloading books, pictures and sreaming videos"
//                     },
//                     {
//                         id: "Q43",
//                         title: "Xfinity Wireless Gateway is composed of all of the following items EXCEPT:",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "WiFi Extender", correct: true },
//                             { id: "A1", text: "Cable Modem", correct: false },
//                             { id: "A2", text: "Wireless Router", correct: false },
//                             { id: "A3", text: "EMTA", correct: false }
//                         ],
//                         description: "xFi Pods are WiFi extenders, purchased separately from the xFi Gateway."
//                     },
//                     {
//                         id: "Q44",
//                         title: "What is the default encryption option for Xfinity Wireless Gateways?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "WEP", correct: false },
//                             { id: "A1", text: "WPA", correct: false },
//                             { id: "A2", text: "WAP", correct: false },
//                             { id: "A3", text: "WPA2", correct: true }
//                         ],
//                         description: "WPA2 uses Advanced Encryption Standard to provide a stronger encryption."
//                     },
//                     {
//                         id: "Q45",
//                         title: "Which of these wireless bandwidths is NOT on the Xfinity Wireless Gateway?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "2.4GHz", correct: false },
//                             { id: "A1", text: "xfinitywifi", correct: false },
//                             { id: "A2", text: "XFINITY", correct: true },
//                             { id: "A3", text: "5.0GHz", correct: false }
//                         ],
//                         description: "Access secure XFINITY SSID, install Xfinity WiFi secure profile on a device"
//                     },
//                     {
//                         id: "Q46",
//                         title: "Can an xFi pod help increase Wi-Fi signal outside the home?",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Yes", correct: true },
//                             { id: "A1", text: "No", correct: false },
//                         ],
//                         description: "xFi Pods help keep Kelly connected throughout her home, both in & outside."
//                     },
//                     {
//                         id: "Q47",
//                         title: "Protected Browsing protects devices connected to a personal home network",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Protected Browsing does not protect devices when using a public XfinityWiFi"
//                     },
//                     {
//                         id: "Q48",
//                         title: "Customers can share their xFi Pod(s) with friends and family.",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: true },
//                             { id: "A1", text: "FALSE", correct: false },
//                         ],
//                         description: "Pods can be shared with others. Xfinity Internet service +xFi Gateway req'd"
//                     },
//                     {
//                         id: "Q49",
//                         title: "All xFi Gateways are compatible w/ xFi Adv Security including Cisco DPC3939",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "Cisco DPC3939 does not support the latest xFi features such as Adv Security"
//                     },
//                     {
//                         id: "Q50",
//                         title: "An xFi Pod allows more people to connect to Kurt's home network.",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "xFi Pods do not control the number of guests that can connect to network."
//                     },
//                     {
//                         id: "Q51",
//                         title: "xFi Wireless Gateway(XB3) & xFi Adv. Gateway(XB6) can go up to 1 Gig speed.",
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "False. Only the xFi Advanced Gateway (XB6) supports 1 Gig Speed!"
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q64",
//                         title: "What is the color of our new XB7 modem?",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Pink", correct: false },
//                             { id: "A1", text: "White", correct: true },
//                             { id: "A2", text: "Silver", correct: false },
//                             { id: "A3", text: "Yellow", correct: false }
//                         ],
//                         description: "xFi Gateway 3rd Generation offers a new color (white/gray)"
//                     },
//                     {
//                         id: "Q65",
//                         title: "xFi Pods do NOT:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Create a mesh network", correct: false },
//                             { id: "A1", text: "Increase Wi-Fi signal outside", correct: false },
//                             { id: "A2", text: "Extend WiFi coverage", correct: false },
//                             { id: "A3", text: "Allow more guests to connect", correct: true }
//                         ],
//                         description: "xFi Pods provide greater coverage to the areas they are placed."
//                     },
//                     {
//                         id: "Q66",
//                         title: "Following are benefits when xFi Gateway is leased EXCEPT:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Parental Controls", correct: false },
//                             { id: "A1", text: "Custom Profiles", correct: false },
//                             { id: "A2", text: "Threat Detection", correct: false },
//                             { id: "A3", text: "Need Multiple xFi Modems", correct: true }
//                         ],
//                         description: "Customers can rent one xFi Gateway for whole home WiFi management!"
//                     },
//                     {
//                         id: "Q67",
//                         title: "By leasing an xFi Gateway, customers can do the following EXCEPT:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "Secure WiFi hotspot for guests", correct: false },
//                             { id: "A1", text: "Connect computers and printers", correct: false },
//                             { id: "A2", text: "Get 24/7 support", correct: false },
//                             { id: "A3", text: "Earn free xFi Pods", correct: true }
//                         ],
//                         description: "Customers must purchase xFi pods separately."
//                     },
//                     {
//                         id: "Q68",
//                         title: "Customers can gain access to all of the following with xFi Gateway EXCEPT:",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "xFi Pods", correct: false },
//                             { id: "A1", text: "Digital Home Automation", correct: false },
//                             { id: "A2", text: "X1 Cloud DVR", correct: true },
//                             { id: "A3", text: "Free Advanced Security", correct: false }
//                         ],
//                         description: "Customers must subscribe to X1 DVR service to access the X1 Cloud DVR."
//                     },
//                     {
//                         id: "Q69",
//                         title: "An xFi Pod is the same as WiFi extenders.",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: "TRUE", correct: false },
//                             { id: "A1", text: "FALSE", correct: true },
//                         ],
//                         description: "xFi Pods create a mesh WiFi network=better coverage. WiFi extenders do not"
//                     },
//                 ]
//             },
//             finished: false
//         },
//     ]
// }



// export const quiz: Quiz = {
//     id: "comcast_olympics_trivia_r2",
//     miles: 0,
//     wait: false,
//     progress: 0,
//     medals: '',
//     topics: [
//         {
//             id: "Flex",
//             key: "OlympicsTriviaTopic1",
//             name: "Flex",
//             bgImage: purple,
//             bgImageXs: purpleXs,
//             questionBg: 'linear-gradient(90.87deg, #6138F5 0%, #805EFA 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q83",
//                         title: 'What is the monthly cost of the first Flex box?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '$4.99', correct: false },
//                             { id: "A1", text: '$10.00', correct: false },
//                             { id: "A2", text: 'Free', correct: true },
//                             { id: "A3", text: '$7.00', correct: false },
//                         ],
//                         description: "The first box is free for internet customers."
//                     },
//                     {
//                         id: "Q84",
//                         title: 'Which apps are accessible through the Flex box?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Netflix', correct: false },
//                             { id: "A1", text: 'Hulu', correct: false },
//                             { id: "A2", text: 'Disney+', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "Flex aggegrates mulitple apps."
//                     },
//                     {
//                         id: "Q86",
//                         title: 'Xfinity Flex is for customers who...',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Like to watch live TV', correct: false },
//                             { id: "A1", text: 'Watch local news', correct: false },
//                             { id: "A2", text: "Don't want/need traditional TV", correct: true },
//                             { id: "A3", text: 'Do not own a TV', correct: false },
//                         ],
//                         description: "Flex is a great way to simplify TV watching for Internet-only customers"
//                     },
//                     {
//                         id: "Q85",
//                         title: 'Flex is a 4k streaming box.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Flex is 4k."
//                     },
//                     {
//                         id: "Q87",
//                         title: 'Flex allows switching between streaming apps without changing inputs.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Flex makes viewing easy because programming is all one place."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q88",
//                         title: 'The following are FREE apps customers can watch with Flex EXCEPT:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Pluto', correct: false },
//                             { id: "A1", text: 'Xumo', correct: false },
//                             { id: "A2", text: 'Netflix', correct: true },
//                             { id: "A3", text: 'Tubi', correct: false },
//                         ],
//                         description: "Netflix is subscription based & not free with X1 or Flex."
//                     },
//                     {
//                         id: "Q90",
//                         title: 'Which streaming services come with Flex?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'HBO Max', correct: false },
//                             { id: "A1", text: 'Peacock Premium', correct: true },
//                             { id: "A2", text: 'Netflix', correct: false },
//                             { id: "A3", text: 'Disney+', correct: false },
//                         ],
//                         description: "Peacock Premium is included with Xfinity Flex, a $4.99/month value"
//                     },
//                     {
//                         id: "Q91",
//                         title: 'Which of these is NOT a benefit of Flex?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Peacock Premium included', correct: false },
//                             { id: "A1", text: 'Must change apps/inputs to find shows', correct: true },
//                             { id: "A2", text: 'Voice Remote included', correct: false },
//                             { id: "A3", text: 'Search all apps with a single command', correct: false },
//                         ],
//                         description: "Flex lets you search across all your content with a single voice command"
//                     },
//                     {
//                         id: "Q89",
//                         title: 'Xfinity Flex also gives customers free access to HBO, Showtime, Cinemax, and Epix.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "They still need to pay for their subscriptions."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q93",
//                         title: 'Once connected, where do customers find activation instructions for Flex?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'On the Xfinity App', correct: false },
//                             { id: "A1", text: 'visiting flex.com', correct: false },
//                             { id: "A2", text: 'A technician has to activate the device', correct: false },
//                             { id: "A3", text: 'Onscreen prompts show activation steps', correct: true },
//                         ],
//                         description: "Once connected, onscreen instructions help customers activate Flex"
//                     },
//                     {
//                         id: "Q94",
//                         title: 'Customers should have a internet download speed of at least ___ Mbps+.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '100', correct: false },
//                             { id: "A1", text: '25', correct: true },
//                             { id: "A2", text: '75', correct: false },
//                             { id: "A3", text: '150', correct: false },
//                         ],
//                         description: "Customers should have an Internet download speed of at least 25 Mbps+"
//                     },
//                     {
//                         id: "Q95",
//                         title: 'What is the cost for a 2nd Flex box?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '$10.00', correct: false },
//                             { id: "A1", text: 'Free', correct: false },
//                             { id: "A2", text: '$5.00', correct: true },
//                             { id: "A3", text: '$7.99', correct: false },
//                         ],
//                         description: "First box is free, second box is $5.00"
//                     },
//                     {
//                         id: "Q92",
//                         title: 'As a new Flex customer, your first 10 new movie rentals are free.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Flex customers can purchase/rent new release movies for an additional fee."
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Peacock",
//             key: "OlympicsTriviaTopic2",
//             name: "Peacock",
//             bgImage: blue,
//             bgImageXs: blueXs,
//             questionBg: 'linear-gradient(90deg, #1F69FF 0%, #508AFE 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q96",
//                         title: 'What do the Today Show, Shrek, and Saved by the Bell have in common?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'They are available to watch on Peacock', correct: true },
//                             { id: "A1", text: 'They all feature Mario Lopez', correct: false },
//                             { id: "A2", text: 'They aired the same year', correct: false },
//                             { id: "A3", text: 'They are all movies', correct: false },
//                         ],
//                         description: "Peacock Premium features these titles + thousands of hours of iconic shows."
//                     },
//                     {
//                         id: "Q97",
//                         title: 'Customers can learn how to paint watching the Bob Ross channel on Peacock',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "The Bob Ross channel features his show and painting lessons."
//                     },
//                     {
//                         id: "Q98",
//                         title: 'Peacock has the best of the past, present, and future entertainment. ',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Peacock has hundred of movies, TV, sports, and always-on channels."
//                     },
//                     {
//                         id: "Q99",
//                         title: 'Premier League Soccer is not available on Peacock.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Watch Premier League Soccer live on Peacock."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q102",
//                         title: 'How many Harry Potter movies can you watch on Peacock?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '6', correct: false },
//                             { id: "A1", text: '7', correct: false },
//                             { id: "A2", text: '8', correct: true },
//                             { id: "A3", text: '9', correct: false },
//                         ],
//                         description: "Stream all 8 Harry Potter movies on Peacock today."
//                     },
//                     {
//                         id: "Q103",
//                         title: 'What do all Internet customers that have X1 or Flex get for free?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Tickets to Universal Studios', correct: false },
//                             { id: "A1", text: 'Peacock Premium', correct: true },
//                             { id: "A2", text: 'Hulu', correct: false },
//                             { id: "A3", text: 'Raptor', correct: false },
//                         ],
//                         description: "Peacock has a library of movies, classic tv, sports and news broadcasts."
//                     },
//                     {
//                         id: "Q104",
//                         title: 'How often is Peacock content updated?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Once a month', correct: false },
//                             { id: "A1", text: 'When the customer asks', correct: false },
//                             { id: "A2", text: 'Daily', correct: true },
//                             { id: "A3", text: 'Every 6 months', correct: false },
//                         ],
//                         description: "Hours of iconic shows, movies, trending news & pop culture is updated daily"
//                     },
//                     {
//                         id: "Q100",
//                         title: 'Peacock has a Rotten Tomatoes channel.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Rotten Tomatoes is one of many channels available on Peacock."
//                     },
//                     {
//                         id: "Q101",
//                         title: 'Peacock has a Today All Day channel featuring Today Show content.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Catch up on Today Show content with the Today Show channel on Peacock."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q105",
//                         title: 'What is the monthly value of Peacock Premium?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Free', correct: false },
//                             { id: "A1", text: '$4.99', correct: true },
//                             { id: "A2", text: '$10.00', correct: false },
//                             { id: "A3", text: '$15.00', correct: false },
//                         ],
//                         description: "Question asked value not price"
//                     },
//                     {
//                         id: "Q107",
//                         title: 'Peacock Premium has all of the following EXCEPT:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Live sports', correct: false },
//                             { id: "A1", text: 'Popular NBC series', correct: false },
//                             { id: "A2", text: 'Early access to late night tv', correct: false },
//                             { id: "A3", text: 'Movies to rent or purchase', correct: true },
//                         ],
//                         description: "It doesn't have movies to rent/purchase but does have a movie library."
//                     },
//                     {
//                         id: "Q108",
//                         title: 'The following are Peacock Originals except:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Rutherford Falls', correct: false },
//                             { id: "A1", text: 'Girls5Eva', correct: false },
//                             { id: "A2", text: 'Transplant', correct: true },
//                             { id: "A3", text: 'Intergalactic', correct: false },
//                         ],
//                         description: "Peacock has several Original shows - discover under Original"
//                     },
//                     {
//                         id: "Q109",
//                         title: 'You can watch Sky News on Peacock.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Sky New is one of many channels available through Peacock."
//                     },
//                     {
//                         id: "Q106",
//                         title: 'Bob is an Xfinity customer, Bill is not. Peacock costs the same for both.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Peacock Premium is included. For non-customers services begin@ $4.99/mo."
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Stream",
//             key: "OlympicsTriviaTopic3",
//             name: "Stream",
//             bgImage: green,
//             bgImageXs: greenXs,
//             questionBg: 'linear-gradient(90deg, #008558 0%, #30B689 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q110",
//                         title: 'You can schedule DVR recordings from anywhere.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Customers can schedule recordings from anywhere."
//                     },
//                     {
//                         id: "Q111",
//                         title: 'What is the cost of the Stream App?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '$5.00', correct: false },
//                             { id: "A1", text: '$8.00', correct: false },
//                             { id: "A2", text: '$10.00', correct: false },
//                             { id: "A3", text: 'Free', correct: true },
//                         ],
//                         description: "The app is included at no additional cost."
//                     },
//                     {
//                         id: "Q112",
//                         title: 'How can you take Xfinity TV on the go?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Xfinity Stream', correct: true },
//                             { id: "A1", text: 'Xfinity Watch-on-the-go', correct: false },
//                             { id: "A2", text: 'Xfinity GO GO', correct: false },
//                             { id: "A3", text: 'Xfinity WiFi Hotspot', correct: false },
//                         ],
//                         description: "Xfinity Stream makes is super easy to take Xfinity video on the go!"
//                     },
//                     {
//                         id: "Q113",
//                         title: 'How many channels of live TV are available on the go?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '10', correct: false },
//                             { id: "A1", text: '200+', correct: true },
//                             { id: "A2", text: '4', correct: false },
//                             { id: "A3", text: '5,000', correct: false },
//                         ],
//                         description: "Watch up to 200+ channels of live TV on the go."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q114",
//                         title: 'How many screens can you stream on in your house?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '3', correct: false },
//                             { id: "A1", text: '5', correct: true },
//                             { id: "A2", text: '7', correct: false },
//                             { id: "A3", text: '2', correct: false },
//                         ],
//                         description: "You can stream up to five screens in your house."
//                     },
//                     {
//                         id: "Q115",
//                         title: 'How many screens can you stream on outside your house?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '7', correct: false },
//                             { id: "A1", text: '5', correct: false },
//                             { id: "A2", text: '2', correct: false },
//                             { id: "A3", text: '3', correct: true },
//                         ],
//                         description: "You can stream up to 3 screens outside your house."
//                     },
//                     {
//                         id: "Q116",
//                         title: 'Customers can access these features using the Stream App EXCEPT:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Schedule DVR Recording', correct: false },
//                             { id: "A1", text: 'Watch On Demand', correct: false },
//                             { id: "A2", text: 'Access Guide Information', correct: false },
//                             { id: "A3", text: 'Manage xFi', correct: true },
//                         ],
//                         description: "The best way to manage xFi is through the xFi mobile app."
//                     },
//                     {
//                         id: "Q117",
//                         title: 'Can you watch content on Stream before your X1 equipment is installed?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Yes', correct: true },
//                             { id: "A1", text: 'No', correct: false },
//                             { id: "A2", text: '', correct: false },
//                             { id: "A3", text: '', correct: false },
//                         ],
//                         description: "Yes! Watch anywhere, on any device, any time you like."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q118",
//                         title: 'How can Bob watch a show on his iPad while Sue watches a game on TV?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Coin toss to determine winner', correct: false },
//                             { id: "A1", text: 'Xfinity Stream', correct: true },
//                             { id: "A2", text: 'Other things to watch', correct: false },
//                             { id: "A3", text: 'Separate Xfinity accounts', correct: false },
//                         ],
//                         description: "Xfinity Stream allows access their video subscription on multiple devices."
//                     },
//                     {
//                         id: "Q119",
//                         title: 'Movies you buy w/Xfinity are always w/you even if you move or disconnect.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Buy on one device and watch on another."
//                     },
//                     {
//                         id: "Q120",
//                         title: 'Premium networks have 1000s downloadable shows/movies for offline viewing.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "SHOWTIME, STARZ, EPIX offer downloadable shows/movies for offline viewing."
//                     },
//                     {
//                         id: "Q121",
//                         title: 'You need internet connection to watch content on the Stream App.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Download you DVR recordings, rentals, purchases and more to watch offline."
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Video",
//             key: "OlympicsTriviaTopic4",
//             name: "Video",
//             bgImage: yellow,
//             bgImageXs: yellowXs,
//             questionBg: 'linear-gradient(90deg, #FFAA00 0%, #F4BB47 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q123",
//                         title: 'Voice commands can be said in either English or Spanish.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Voice commands, TV guide, and Voice Guidance are accessible in English and Spanish."
//                     },
//                     {
//                         id: "Q124",
//                         title: 'SportsZone helps fans find trending sports documentaries.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "SportsZone offers trending sports documentaries, movies, and series."
//                     },
//                     {
//                         id: "Q125",
//                         title: 'Customers can connect their Bluetooth headphones to X1.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Connect Bluetooth Devices to X1 simply by going to device settings."
//                     },
//                     {
//                         id: "Q122",
//                         title: 'Content in KidsZone cannot be restricted by age rage.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "KidsZone allows parents to restrict content by age range."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q126",
//                         title: 'When applied to Xfinity streaming services and apps, “OTT” stands for:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Out There Together', correct: false },
//                             { id: "A1", text: 'Over-The-Top', correct: true },
//                             { id: "A2", text: 'On-Time TV', correct: false },
//                             { id: "A3", text: 'Oops, That’s Tough', correct: false },
//                         ],
//                         description: "Over the top (OTT) refers to content provided via high-speed Internet."
//                     },
//                     {
//                         id: "Q127",
//                         title: 'Where can sports fans find the latest league standings, player stats, and highlights?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TeamZone', correct: false },
//                             { id: "A1", text: 'ESPNZone', correct: false },
//                             { id: "A2", text: 'SportsZone', correct: true },
//                             { id: "A3", text: 'AthleteZone', correct: false },
//                         ],
//                         description: "Personalized for the teams you love, SportsZone is a must-have for die-hards."
//                     },
//                     {
//                         id: "Q128",
//                         title: 'KidsZone offers all of the following for kids except:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Web Browsing', correct: true },
//                             { id: "A1", text: 'Movies/Shows', correct: false },
//                             { id: "A2", text: 'Educational Programming', correct: false },
//                             { id: "A3", text: "Kids' Music Videos", correct: false },
//                         ],
//                         description: "KidsZone offers kid-friendly live TV, On Demand content, and DVR recordings."
//                     },
//                     {
//                         id: "Q129",
//                         title: 'Which button on the Xfinity Voice Remote shows sports fans scores and game info?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'A', correct: false },
//                             { id: "A1", text: 'B', correct: false },
//                             { id: "A2", text: 'C', correct: true },
//                             { id: "A3", text: 'D', correct: false },
//                         ],
//                         description: "You can press the C button on the Voice Remote to get all the game info you need."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q133",
//                         title: 'Where can customers find parent approved programming?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'KidsZone', correct: true },
//                             { id: "A1", text: 'School at home', correct: false },
//                             { id: "A2", text: 'KidsContent', correct: false },
//                             { id: "A3", text: 'ParentApproved', correct: false },
//                         ],
//                         description: "KidsZone contains parent approved content in Spanish and English."
//                     },
//                     {
//                         id: "Q132",
//                         title: 'The following editorial collections are available except:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'LGBTQ+', correct: false },
//                             { id: "A1", text: 'UFO Sightings', correct: true },
//                             { id: "A2", text: 'Black Experience', correct: false },
//                             { id: "A3", text: 'Latino', correct: false },
//                         ],
//                         description: "LGBTQ+, Black Experience, and Latino are all editorial collections."
//                     },
//                     {
//                         id: "Q134",
//                         title: 'KidsZone content is available in both English and Spanish.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "KidsZone contains parent approved content in Spanish and English."
//                     },
//                     {
//                         id: "Q131",
//                         title: "Watching HBOMax through Flex or X1, doesn't use data for internet service.",
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "HBO Max is streamed & will add to a customer’s monthly data usage."
//                     },
//                     {
//                         id: "Q130",
//                         title: 'Xfinity Sports Pay-Per-View only lets you watch on one screen.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Customers can watch on every TV Box connected to their Xfinity account."
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "Voice Remote",
//             key: "OlympicsTriviaTopic5",
//             name: "Voice Remote",
//             bgImage: orange,
//             bgImageXs: orangeXs,
//             questionBg: 'linear-gradient(90deg, #E64F00 0%, #ED6E2C 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q135",
//                         title: 'What show will appear if you say Dunder Mifflin into voice remote?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Paper Factory', correct: false },
//                             { id: "A1", text: 'The Office', correct: true },
//                             { id: "A2", text: 'Friends', correct: false },
//                             { id: "A3", text: 'Parks & Recreation', correct: false },
//                         ],
//                         description: "Loads of content from the Office on Peacock!"
//                     },
//                     {
//                         id: "Q136",
//                         title: 'If you say, "yeehaw" into the voice remote, what will appear as content?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Cowboys', correct: false },
//                             { id: "A1", text: 'Horses', correct: false },
//                             { id: "A2", text: 'Westerns', correct: true },
//                             { id: "A3", text: 'Clowns', correct: false },
//                         ],
//                         description: "Yeehaw will show available Westerns."
//                     },
//                     {
//                         id: "Q137",
//                         title: 'If you say, "Bazinga" into the voice remote, what will appear as content?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Magic shows', correct: false },
//                             { id: "A1", text: 'Science shows', correct: false },
//                             { id: "A2", text: 'Shazam', correct: false },
//                             { id: "A3", text: 'Big Bang Theory', correct: true },
//                         ],
//                         description: "Big Bang Theory is associated with Bazinga."
//                     },
//                     {
//                         id: "Q138",
//                         title: 'If you say, "Great Scott" into the voice remote, which movie will appear?',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Back to the Future', correct: false },
//                             { id: "A1", text: 'Back to the Future II', correct: false },
//                             { id: "A2", text: 'Back to the Future III', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "Great Scott will make the entire Back to the Future franchise appear."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q140",
//                         title: 'Which video platforms include a Voice Remote & allows integrated search?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Xfinity X1', correct: false },
//                             { id: "A1", text: 'Xfinity Flex', correct: false },
//                             { id: "A2", text: 'Both', correct: true },
//                             { id: "A3", text: 'Neither', correct: false },
//                         ],
//                         description: "Both Xfinity X1 and Flex include a Voice Remote."
//                     },
//                     {
//                         id: "Q142",
//                         title: 'Which video platforms include a Voice Remote and allows integrated search?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Xfinity X1', correct: false },
//                             { id: "A1", text: 'Xfinity Flex', correct: false },
//                             { id: "A2", text: 'Both', correct: true },
//                             { id: "A3", text: 'Neither', correct: false },
//                         ],
//                         description: "Both Xfinity X1 and Flex include a Voice Remote."
//                     },
//                     {
//                         id: "Q143",
//                         title: 'Which button on the remote accesses help features?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'A', correct: true },
//                             { id: "A1", text: 'B', correct: false },
//                             { id: "A2", text: 'C', correct: false },
//                             { id: "A3", text: 'D', correct: false },
//                         ],
//                         description: "Using the A button on the remote accesses the help features."
//                     },
//                     {
//                         id: "Q144",
//                         title: 'The following features are available with the X1 Voice Remote EXCEPT:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Watch country music videos', correct: false },
//                             { id: "A1", text: 'Backlit keypad', correct: false },
//                             { id: "A2", text: 'Access accessibility features', correct: false },
//                             { id: "A3", text: 'Customized colors', correct: true },
//                         ],
//                         description: "Customers aren't able to customize colors with X1 Voice Remote."
//                     },
//                     {
//                         id: "Q145",
//                         title: 'The last button on the X1 remote will show your last __ shows.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '6', correct: false },
//                             { id: "A1", text: '9', correct: true },
//                             { id: "A2", text: '5', correct: false },
//                             { id: "A3", text: '3', correct: false },
//                         ],
//                         description: "The last 9 shows watched will appear."
//                     },
//                     {
//                         id: "Q139",
//                         title: 'Flex does not come with a voice remote.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: false },
//                             { id: "A1", text: 'FALSE', correct: true },
//                         ],
//                         description: "Flex does come with a voice remote."
//                     },
//                     {
//                         id: "Q141",
//                         title: 'Customers can pull up 4K content by saying, “4K” into the Voice Remote.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: 'Saying "4K" will take customers to "Experience 4K UHD" on X1 and Flex.'
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q147",
//                         title: 'Will a purchase made with HSN Shop by remote appear on Comcast bill?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Yes', correct: false },
//                             { id: "A1", text: 'No', correct: true },
//                         ],
//                         description: "Won't appear on Comcast bill. HSN charges pymt on file w/HSN"
//                     },
//                     {
//                         id: "Q146",
//                         title: 'Saying "what should I watch?" into remote will generate recommendations.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Recommendations based on previously watched, trending & editors picks."
//                     },
//                     {
//                         id: "Q148",
//                         title: 'You can ask the voice remote what song is playing on the TV.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: 'Next time you are stumped, try asking, "what song is this?"'
//                     },
//                     {
//                         id: "Q149",
//                         title: 'The X1 voice remote is bilingual.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Understands English and Spanish"
//                     },
//                 ]
//             },
//             finished: false
//         },
//         {
//             id: "X1",
//             key: "OlympicsTriviaTopic6",
//             name: "X1",
//             bgImage: red,
//             bgImageXs: redXs,
//             questionBg: 'linear-gradient(90deg, #E6004D 0%, #DB346C 100%)',
//             questions: {
//                 Bronze: [
//                     {
//                         id: "Q150",
//                         title: 'You can pause, rewind, and record live TV with X1.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "All are available with X1"
//                     },
//                     {
//                         id: "Q151",
//                         title: 'You can access Netflix & Amazon Prime using X1 the shortcut Last 9.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Last 9 and Favorites make Netflix and Amazon Prime easily accessible."
//                     },
//                     {
//                         id: "Q152",
//                         title: 'With X1 you can receive alerts when your favorite team is playing.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "X1 will notify you when your favorite team has a game."
//                     },
//                     {
//                         id: "Q153",
//                         title: 'Comcast has their own employee channel.',
//                         miles: 90,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Simply say Comcast Employee channel into voice remote to access."
//                     },
//                 ],
//                 Silver: [
//                     {
//                         id: "Q154",
//                         title: 'What are some of the added benefits of X1?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Custom viewing experience', correct: false },
//                             { id: "A1", text: 'Editorial collections', correct: false },
//                             { id: "A2", text: 'Sports Zone', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "X1 provides the ultimate personalized viewing experience"
//                     },
//                     {
//                         id: "Q155",
//                         title: 'The following options are available within the X1 Settings menu EXCEPT:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Parental Controls', correct: false },
//                             { id: "A1", text: 'Shop for accessories', correct: true },
//                             { id: "A2", text: 'My Account', correct: false },
//                             { id: "A3", text: 'Device Settings', correct: false },
//                         ],
//                         description: "Parental Controls, My Account & Device Settings are all in Settings"
//                     },
//                     {
//                         id: "Q156",
//                         title: 'How can a customer access the Sports Zone App on X1?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Press the “C” button', correct: true },
//                             { id: "A1", text: 'Upgrade their subscription', correct: false },
//                             { id: "A2", text: 'SportsZone App/mobile device', correct: false },
//                             { id: "A3", text: 'Press the info button', correct: false },
//                         ],
//                         description: 'The "C" button opens an entire world of sports!'
//                     },
//                     {
//                         id: "Q157",
//                         title: 'The landing page for your favorite team has everything EXCEPT:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Upcoming Games', correct: false },
//                             { id: "A1", text: 'Team News', correct: false },
//                             { id: "A2", text: 'Game replays', correct: false },
//                             { id: "A3", text: 'Mentions on Twitter', correct: true },
//                         ],
//                         description: "There's a lot of great info for fans to love on their team landing pages."
//                     },
//                     {
//                         id: "Q159",
//                         title: 'What is the most complete video experience?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Xfinity X1', correct: true },
//                             { id: "A1", text: 'Xfinity Home', correct: false },
//                             { id: "A2", text: 'Xfinity Stream', correct: false },
//                             { id: "A3", text: 'Xfinity Flex', correct: false },
//                         ],
//                         description: "X1 is the most complete way to access all entertainment on all screens."
//                     },
//                     {
//                         id: "Q160",
//                         title: 'Where can X1 customers view their Internet data usage?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Account', correct: true },
//                             { id: "A1", text: 'Search', correct: false },
//                             { id: "A2", text: 'Watch', correct: false },
//                             { id: "A3", text: 'Apps', correct: false },
//                         ],
//                         description: "Customers can always track their data usage through their Account Profile."
//                     },
//                     {
//                         id: "Q161",
//                         title: 'With X1 you need _ passwords to access Netflix, Hulu & Prime Video?',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: '2', correct: false },
//                             { id: "A1", text: '3', correct: false },
//                             { id: "A2", text: '1', correct: true },
//                             { id: "A3", text: '4', correct: false },
//                         ],
//                         description: "Only 1 password is needed-Single-sign on is an advantage of Streaming w/X1"
//                     },
//                     {
//                         id: "Q162",
//                         title: 'The following are voice commands for X1 except:',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Pause', correct: false },
//                             { id: "A1", text: 'Fast forward 3 minutes', correct: false },
//                             { id: "A2", text: 'Make me a snack', correct: true },
//                             { id: "A3", text: 'Turn on captions', correct: false },
//                         ],
//                         description: "Unfortunately, the X1 box nor voice remote can make you a snack."
//                     },
//                     {
//                         id: "Q158",
//                         title: 'With X1, a customer can get alerts when their favorite team is playing.',
//                         miles: 100,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "Team alerts are just one more way we keep sports fans in the know."
//                     },
//                 ],
//                 Gold: [
//                     {
//                         id: "Q163",
//                         title: 'Sports Zone includes:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Stats', correct: false },
//                             { id: "A1", text: 'Scores', correct: false },
//                             { id: "A2", text: 'Schedules', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "All are part of the Sports Zone"
//                     },
//                     {
//                         id: "Q164",
//                         title: 'The following sports leagues are in the X1 Sports Guide EXCEPT:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'NFL', correct: false },
//                             { id: "A1", text: 'Premier League', correct: false },
//                             { id: "A2", text: 'College Sports', correct: false },
//                             { id: "A3", text: 'XFL', correct: true },
//                         ],
//                         description: "Sports Zone has a robust menu including the NFL, Premier League & College"
//                     },
//                     {
//                         id: "Q165",
//                         title: 'Which can be watched in 4K with X1?',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'PrimeVideo', correct: false },
//                             { id: "A1", text: 'YouTube', correct: false },
//                             { id: "A2", text: 'Netflix', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "All have 4K content"
//                     },
//                     {
//                         id: "Q167",
//                         title: 'Options for Guide View on X1 include:',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'Free to Me', correct: false },
//                             { id: "A1", text: 'HD Channels', correct: false },
//                             { id: "A2", text: 'Trending', correct: false },
//                             { id: "A3", text: 'All', correct: true },
//                         ],
//                         description: "In addition Guide can be filtered by Favorites, Movies, Sports, Kids"
//                     },
//                     {
//                         id: "Q166",
//                         title: 'The X1 Sports App is bilingual.',
//                         miles: 110,
//                         bonus: false,
//                         answers: [
//                             { id: "A0", text: 'TRUE', correct: true },
//                             { id: "A1", text: 'FALSE', correct: false },
//                         ],
//                         description: "The X1 Sports App is bilingual."
//                     },
//                 ]
//             },
//             finished: false
//         },
//     ]
// }