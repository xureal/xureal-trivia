import { topicKeys } from './mockData';

export interface Answer {
  id: string;
  text: string;
  correct: boolean;
}

export interface Question {
  id: string;
  title: string;
  miles: number;
  answeredCorrect?: boolean;
  bonus: boolean;
  answers: Answer[];
  description: string;
}

export interface Topic {
  id: string;
  key: (typeof topicKeys)[number];
  name: string;
  bgImage: string;
  bgImageXs: string;
  questionBg: string;
  questions: Record<Medal, Question[]>;
  finished: boolean;
}

export interface Quiz {
  id: string;
  miles: number;
  wait: boolean;
  topics: Topic[];
  progress: number;
  medals: string;
}

export type Progress = {
  correctQuestions: string[];
  date_created: string;
  levels: string[];
  timestamp: string;
  topics: {
    topicID: string;
    progress: string[];
  }[];
  triviaID: string;
  xurealID: string;
  _id: string;
};

export type ProgressCallObj = {
  xurealID: string;
  triviaID: string;
  topics: JSON[];
  levels: string[];
  points: number;
  correctQuestions: string[];
  timestamp: string;
  reset: boolean;
};

export type State = Quiz & {
  currentTopic: Topic;
  answerCalculated: boolean;
  answerCorrect: boolean;
  currentQuestion: Question;
  timestamp: string;
  answeredQuestions: {
    topicID: string;
    progress: string[];
  }[];
};
export type Medal = 'Gold' | 'Silver' | 'Bronze' | string;

type Message = 'SUCCESS' | 'FAIL';
export type Result = 'CORRECT' | 'INCORRECT';

type ApiCallBack<T> = (msg: Message, data: T) => void;

export type DataKeys =
  | 'OlympicsTriviaTopic1'
  | 'OlympicsTriviaTopic2'
  | 'OlympicsTriviaTopic3'
  | 'OlympicsTriviaTopic4'
  | 'OlympicsTriviaTopic5'
  | 'OlympicsTriviaTopic6'
  | 'OlympicsTriviaTopic7'
  | 'OlympicsTriviaTimestamp'
  | 'OlympicsTriviaMedals'
  | 'OlympicsTriviaCorrectQuestions';

export type AllUserData = {
  id: DataKeys;
  data: {
    Value: string;
  };
}[];

export interface UserData {
  Value: string;
}

declare global {
  interface Window {
    foatOlympicsTriviaAPI: Api;
  }
}

export interface Api {
  getUserData: (topicKey: string, cb: ApiCallBack<UserData>) => void;
  getUserDataAll: (cb: ApiCallBack<AllUserData>) => void;
  gameEventQuestion: (topicKey: DataKeys, qID: Question['id'], aID: Answer['id'], result: Result) => void;
  gameEventMedal: (medalTitle: 'OlympicsTriviaMedals', medalID: string) => void;
  clearUserData: (topicKey: DataKeys, cb: (msg: Message) => void) => void;
  gameEventTimestamp: (timestampKey: 'OlympicsTriviaTimestamp', timestamp: any) => void;
  gameEventStart: () => void;
  gameEventCategory: (topicName: Topic['name']) => void;
  gameEventOver: (medal: Medal | '' | string, completedQuestions: number, totalQuestion: number) => void;
  gameEventCorrectAnswers: (answerID: string) => void;
  transferMiles: (value: number, cb: (msg: Message, data: any) => void) => void;
  initTransfer(cb: ApiCallBack<unknown>): void;
  getTriviaCompletionProgress(cb: ApiCallBack<unknown>): void;
}
