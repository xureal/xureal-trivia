import { Medal, Topic } from './types';
import Policies from './Policies';
import * as DateUtil from 'utils/date';

class Specs {
  constructor(private dateUtils: typeof DateUtil) {
    this.topicFinished = this.topicFinished.bind(this);
    this.finished = this.finished.bind(this);
    this.allFinished = this.allFinished.bind(this);
    this.shouldWait = this.shouldWait.bind(this);
    this.startOfTheRound = this.startOfTheRound.bind(this);
    this.roundCompleted = this.roundCompleted.bind(this);
  }

  topicFinished(topic: Topic) {
    for (const medal in topic.questions) {
      const notFinished = topic.questions[medal as Medal].find((question) => question.answeredCorrect === undefined);
      if (notFinished) {
        return false;
      }
    }

    return true;
  }

  finished(topics: Topic[], policies: Policies) {
    return (
      this.allFinished(topics) ||
      policies.getCorrectAnswered(topics, true) === (policies.getProgressRequiredToEarMedal() + 1) * 3
    );
  }

  allFinished(topics: Topic[]) {
    const finished = topics.reduce((finished, topic) => (topic.finished ? finished + 1 : finished), 0);
    return finished === topics.length;
  }

  shouldWait(timestamp: Date) {
    const midnightFollowingDay = new Date(timestamp);
    midnightFollowingDay.setDate(midnightFollowingDay.getDate() + 1);
    midnightFollowingDay.setHours(0, 0, 0, 0);
    return new Date().valueOf() < midnightFollowingDay.valueOf();
  }

  startOfTheRound(progress: number) {
    return progress === 0;
  }

  roundCompleted(topics: Topic[], policies: Policies) {
    const progressCount = policies.getCorrectAnswered(topics, false);
    const medalDefAmount = policies.getProgressRequiredToEarMedal();
    return progressCount % medalDefAmount === 0;
  }
}

export default Specs;
