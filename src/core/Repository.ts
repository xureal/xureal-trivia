import { Progress } from './types';

declare var xurealAPI: any;
declare var trainingAPI: any;

const API_SOURCE = process.env.REACT_APP_API_SOURCE;
const API_KEY = process.env.REACT_APP_APP_ID;

class Repository {
  constructor() {
    this.initApiClient = this.initApiClient.bind(this);
    this.initApiTraining = this.initApiTraining.bind(this);
    this.postNewAnswer = this.postNewAnswer.bind(this);
  }

  async initApiClient() {
    return xurealAPI.init(API_SOURCE, API_KEY).catch((e: any) => {
      console.error('Init API Client error: ', e);
    });
  }

  async initApiTraining() {
    return trainingAPI.init(API_SOURCE, API_KEY).catch((e: any) => {
      console.error('Init Training API Client error: ', e);
    });
  }

  async getProgress(xurealId: string): Promise<Progress> {
    let returnedProgress = await trainingAPI
      .getTriviaCompletionProgress({
        xurealID: xurealId,
        triviaID: 'trivia-demo'
      })
      .then((res: { result: string; data: Progress }) => {
        if (res.result === 'SUCCESS') {
          console.log('[getProgress] res.data: ', res.data);
          return res.data;
        }
        return {};
      })
      .catch((err: any) => {
        console.error('[getTriviaCompletionProgress] Error: ', err);
        return {};
      });

    return returnedProgress;
  }

  async postNewAnswer(answerObj: {
    xurealID: string;
    triviaID: string;
    topics: { topicID: string; progress: string[] }[];
    levels: string[];
    points: number;
    correctQuestions: string[];
    timestamp: string;
    reset: boolean;
  }) {
    return trainingAPI.logTriviaCompletionProgress(answerObj);
  }
}

export default Repository;
