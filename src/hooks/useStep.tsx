import React, { useCallback, useEffect, useState } from 'react';

export interface StepComponentProps {
  next: () => void;
  prev: () => void;
  getStep: () => number;
  goTo: (index: number) => void;
  children?: React.ReactNode;
}

interface Options {
  initialIndex?: number;
}

const useStep = (components: React.FC<StepComponentProps>[], { initialIndex = 0 }: Options = {}) => {
  const [currentIndex, setCurrentIndex] = useState(initialIndex);

  useEffect(() => {
    setCurrentIndex(initialIndex);
  }, [initialIndex]);

  const Component = components[currentIndex];
  const last = currentIndex + 1 === components.length;

  const getStep = useCallback(() => currentIndex, [currentIndex]);

  const next = useCallback(() => {
    if (last) {
      return;
    }

    setCurrentIndex((prev) => ++prev);
  }, [last]);

  const goTo = useCallback((index: number) => {
    setCurrentIndex(index);
  }, []);

  const prev = useCallback(() => {
    if (currentIndex === 0) {
      return;
    }
    setCurrentIndex((prev) => --prev);
  }, [currentIndex]);

  return {
    StepComponent: <Component next={next} prev={prev} goTo={goTo} getStep={getStep} />,
    length: components.length,
    currentIndex
  };
};

export default useStep;
