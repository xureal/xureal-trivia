import { MutableRefObject, useCallback, useEffect, useRef, useState } from 'react';

interface UseTimerProps {
  initialTime: number;
  interval: number;
  onEnd?: () => void;
}

interface ReturnValue {
  reset: () => void;
  start: () => void;
  stop: () => void;
  time: MutableRefObject<number>;
  currentTime: number;
}

const useTimer = ({ initialTime = 0, interval = 1, onEnd }: UseTimerProps): ReturnValue => {
  const [started, setStarted] = useState(false);
  const [time, setTime] = useState(initialTime);
  const timeRef = useRef(initialTime);

  const reset = useCallback(() => {
    timeRef.current = initialTime;
    setTime(initialTime);
    setStarted(false);
  }, [initialTime]);

  const start = useCallback(() => {
    setStarted(true);
  }, []);

  const stop = useCallback(() => {
    setStarted(false);
  }, []);

  useEffect(() => {
    let intervalId: NodeJS.Timeout | null = null;

    if (started) {
      intervalId = setInterval(() => {
        if (timeRef.current - interval < 0) {
          setStarted(false);
          if (onEnd) {
            onEnd();
          }
          return intervalId && clearInterval(intervalId);
        }
        timeRef.current = timeRef.current - interval;
        setTime(timeRef.current);
      }, interval * 1000);
    } else if (intervalId) {
      clearInterval(intervalId);
    }

    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
    };
  }, [interval, started, onEnd]);

  return {
    reset,
    start,
    stop,
    time: timeRef,
    currentTime: time
  };
};

export default useTimer;
