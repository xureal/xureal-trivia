import useSound from 'use-sound';
import IntroTriviaMusic from 'assets/sounds/olympics-game-music.mp3';
import QuestionMusic from 'assets/sounds/music-during-question.mp3';
import OlympicTriviaAtlas from 'assets/sounds/atlas-olympics-trivia.mp3';

const useSFX = () => {
  const [playAtlas, atlasObj] = useSound(OlympicTriviaAtlas, {
    sprite: {
      spin: [0, 4500],
      spinStop: [5000, 1500],
      correct: [7000, 1900],
      wrong: [9000, 500],
      select: [10000, 525],
      bonus: [11000, 1950]
    },
    interrupt: true,
    volume: 0.2
  });

  const [playQuestion, playQuestionObj] = useSound(QuestionMusic, {
    loop: true,
    onend: () => {
      console.info('Audio ENDED');
    },
    volume: 0.5
  });

  const [playIntro, { stop: stopIntro }] = useSound(IntroTriviaMusic);

  const stopAllSounds = () => {
    atlasObj.stop();
    playQuestionObj.stop();
    stopIntro();
    Howler.stop();
  };

  return {
    playAtlas,
    atlasObj,
    playQuestion,
    playQuestionObj,
    playIntro,
    stopIntro,
    stopAllSounds
  };
};

export default useSFX;
