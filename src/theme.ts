import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  typography: {
    fontFamily: 'Poppins, PoppinsBold'
  },
  palette: {
    primary: {
      main: '#ffffff'
    },
    secondary: {
      main: '#ffffff'
    },
    success: {
      main: '#00D267'
    },
    error: {
      main: '#E01E4A'
    }
  },
  spacing: 8
});
