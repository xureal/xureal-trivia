import React from 'react';
import ReactDOM from 'react-dom/client';

import './index.css';

import { theme } from './theme';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/material/styles';
import AppRouter from 'routes/AppRouter';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
  // <React.StrictMode>
  <ThemeProvider theme={theme}>
    <BrowserRouter basename='/trivia'>
      <AppRouter />
    </BrowserRouter>
  </ThemeProvider>
  // </React.StrictMode>
);
