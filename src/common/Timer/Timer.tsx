import React, { useEffect } from 'react';
import TimerIcon from '@mui/icons-material/Timer';
import useStyles from './styles';
import { Grid, useTheme, useMediaQuery } from '@mui/material';
import { Text } from 'common';
import { useTimer } from '../../hooks';
import { TIMER_DURATION } from 'utils/constants';

interface TimerProps {
  play: boolean;
  onEnd: () => void;
}

const Timer: React.FC<TimerProps> = ({ play, onEnd }) => {
  const classes = useStyles();
  const theme = useTheme();
  const sm = useMediaQuery(theme.breakpoints.down('sm'));

  const { currentTime, start, stop } = useTimer({
    initialTime: TIMER_DURATION,
    interval: 1,
    onEnd
  });

  useEffect(() => {
    if (!play) {
      stop();
    }
  }, [play, stop]);

  useEffect(() => {
    start();
  }, [start]);

  return (
    <Grid
      container
      justifyContent={'center'}
      alignItems={'center'}
      wrap='nowrap'
      direction={'row'}
      margin={'auto'}
      textAlign={'center'}
      minWidth={'72px'}
    >
      <Grid
        xs={4}
        container
        item
        sx={{
          marginRight: '24px',
          [theme.breakpoints.down('sm')]: {
            marginRight: '12px'
          }
        }}
        justifyContent={'flex-end'}
        alignItems={'center'}
      >
        <TimerIcon
          sx={{
            fontSize: sm ? 30 : 42,
            color: 'white'
          }}
        />
      </Grid>
      <Grid item xs={6}>
        <Text size={sm ? 25 : 42} weight={'bold'} className={classes.time} xs={25}>
          {currentTime}
          {/* {currentTime.toString().slice(0, 2)} */}
        </Text>
      </Grid>
    </Grid>
  );
};

export default Timer;
