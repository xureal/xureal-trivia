import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(() => ({
  time: {
    maxWidth: '64px',
    fontFeatureSettings: 'tnum',
    fontVariantNumeric: 'tabular-nums'
  }
}));

export default useStyles;
