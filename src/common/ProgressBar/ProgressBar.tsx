import { useMediaQuery } from '@mui/material';
import { ProgressStyles } from './styles';
import { useQuiz } from 'store/useQuiz';
import goldMedalIcon from 'assets/gold-medal.png';
import silverMedalIcon from 'assets/silver-medal.png';
import bronzeMedalIcon from 'assets/bronze-medal.png';
// import { getMedalFromString } from "utils/medal";
import ProgressOverlayImage from 'assets/progress-overlay.png';
import MobileProgressOverlayImage from 'assets/progress-overlay-mobile.png';
import { parseMedalString } from 'utils/medal';

const Progress: React.FC = () => {
  const { state, quizContainer } = useQuiz();
  const { medals } = state;
  const matches = useMediaQuery('(max-width:820px)');
  const currentMedal = parseMedalString(medals);
  const policies = quizContainer.getPolicies();
  const progress = state.progress;

  const medalDefAmount = policies.getProgressRequiredToEarMedal();

  const bonus = progress >= medalDefAmount;

  const medalMap = {
    Gold: {
      current: goldMedalIcon,
      next: goldMedalIcon
    },
    Silver: {
      current: silverMedalIcon,
      next: goldMedalIcon
    },
    Bronze: {
      current: bronzeMedalIcon,
      next: silverMedalIcon
    }
  };

  return (
    <ProgressStyles.Root>
      <ProgressStyles.Overlay src={matches ? MobileProgressOverlayImage : ProgressOverlayImage} />
      <ProgressStyles.Background>
        {Array(medalDefAmount)
          .fill(0)
          .map((_, index) => {
            return (
              // <Grid key={index} item className={cl(bonus ? classes.progressItem + ' color-change' : classes.progressItem, { [classes.finished]: !bonus && progress > index })} >
              <ProgressStyles.ProgressItem key={index} $attained={progress > index} $index={index}>
                {progress > index && (
                  <ProgressStyles.Highlight
                    $first={index === 0}
                    $last={index !== 0 && index + 1 === progress}
                  ></ProgressStyles.Highlight>
                )}
              </ProgressStyles.ProgressItem>
            );
          })}

        <ProgressStyles.Medal
          src={
            !currentMedal
              ? bronzeMedalIcon
              : progress < 4
                ? medalMap[currentMedal as keyof typeof medalMap].next
                : medalMap[currentMedal as keyof typeof medalMap].current
          }
        />
      </ProgressStyles.Background>
      <ProgressStyles.BonusContainer>
        <ProgressStyles.BonusHighlight />
        <ProgressStyles.BonusFill className={bonus ? 'color-change' : ''}>
          {matches ? '?' : 'BONUS QUESTION'}
        </ProgressStyles.BonusFill>
      </ProgressStyles.BonusContainer>
    </ProgressStyles.Root>
  );
};

export default Progress;
