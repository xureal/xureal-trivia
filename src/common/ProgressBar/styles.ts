import styled from 'styled-components';

export const ProgressStyles = {
  Root: styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 1240px;
    height: 40px;
    padding: 8px 0;
    @media (max-width: 1400px) {
      width: 960px;
    }
    @media (max-width: 1080px) {
      width: 800px;
      height: 32px;
    }
    @media (max-width: 820px) {
      width: 540px;
      height: 60px;
    }
    @media (max-width: 600px) {
      width: 400px;
      height: 46px;
    }
    @media (max-width: 490px) {
      width: 325px;
      height: 36px;
    }
    @media (max-width: 320px) {
      width: 280px;
      height: 32px;
    }
  `,
  Overlay: styled.img`
    width: 100%;
    height: auto;
    position: absolute;
    z-index: 10;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  `,
  Background: styled.div`
    border-radius: 64px;
    height: 100%;
    background: linear-gradient(to bottom, #000000, #2e2e2e);
    display: flex;
    align-content: center;
    width: 972px;
    margin: 0 -4px 0 6px;
    justify-content: flex-start;

    @media (max-width: 1400px) {
      margin: 0 -4px 0 4px;
      height: 90%;
    }

    @media (max-width: 1080px) {
      margin: 0 -6px 0 4px;
      width: 626px;
    }

    @media (max-width: 820px) {
      margin: 0 -12px 0 4px;
      height: 100%;
      width: 420px;
    }

    @media (max-width: 600px) {
      margin: 0px -8px 0 4px;
      width: 316px;
    }

    @media (max-width: 490px) {
      margin: 0 -12px 0 4px;
      width: 252px;
    }

    @media (max-width: 320px) {
      margin: 0 -12px 0 0px;
      width: 220px;
    }
  `,
  ProgressItem: styled.div<{ $attained: boolean; $index: number }>`
    position: relative;
    width: 25%;
    height: 100%;
    ${(props) =>
      props.$index === 0
        ? `border-radius: 64px 0 0 64px;`
        : props.$index === 3
          ? `border-radius: 0 64px 64px 0;`
          : `border-radius: 0;`}
    ${(props) => props.$attained && `background: linear-gradient(to top, #1F69FF 0%, #508AFE 100%);`}
      
    @media (max-width: 820px) {
    }
  `,
  Highlight: styled.div<{
    $first: boolean;
    $last: boolean;
  }>`
    position: absolute;
    top: 8px;
    ${({ $first }) => `left: ${$first ? '18px' : '0'}`};
    ${({ $first, $last }) => `width: ${$first ? '90%' : $last ? '90%' : '97.5%'}`};

    opacity: 0.9;
    filter: blur(4px);
    background: white;

    padding: 2px;
    z-index: 5;

    @media (max-width: 1080px) {
      ${({ $first, $last }) => `width: ${$first ? '87.5%' : $last ? '90%' : '97.5%'}`};
    }
    @media (max-width: 820px) {
      top: 12px;
      ${({ $first }) => `left: ${$first ? '24px' : '0'}`};
      ${({ $first, $last }) => `width: ${$first ? '80%' : $last ? '90%' : '97.5%'}`};
    }
    @media (max-width: 600px) {
      top: 12px;
      ${({ $first }) => `left: ${$first ? '24px' : '0'}`};
      ${({ $first, $last }) => `width: ${$first ? '60%' : $last ? '90%' : '97.5%'}`};
    }
    @media (max-width: 490px) {
      top: 12px;
      opacity: 0.7;

      ${({ $first }) => `left: ${$first ? '24px' : '0'}`};
      ${({ $first, $last }) => `width: ${$first ? '60%' : $last ? '90%' : '97.5%'}`};
    }
  `,
  Medal: styled.img`
    z-index: 10;
    position: absolute;
    top: -18px;
    right: 224px;
    width: 61px;
    height: auto;

    @media (max-width: 1400px) {
      top: -16px;
      right: 166px;
      width: 56px;
    }

    @media (max-width: 1080px) {
      top: -10px;
      right: 140px;
      width: 46px;
    }

    @media (max-width: 820px) {
      top: -8px;
      right: 75px;
      width: 64px;
    }

    @media (max-width: 600px) {
      top: 0px;
      right: 60px;
      width: 42px;
    }

    @media (max-width: 490px) {
      top: 0px;
      right: 47px;
      width: 36px;
    }

    @media (max-width: 320px) {
      top: 4px;
      right: 42px;
      width: 28px;
    }
  `,
  BonusContainer: styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background: linear-gradient(to bottom, #000000, #2e2e2e);
    width: 17%;
    height: 100%;
    margin: 0 4px 0 58px;
    border-radius: 64px;
    @media (max-width: 1400px) {
      width: 216px;
      margin: 0 0 0 42px;
      font-size: 14px;
    }
    @media (max-width: 1080px) {
      width: 136px;
      margin: 0 4px 0 38px;
      font-size: 12px;
    }
    @media (max-width: 820px) {
      border-radius: 50%;
      margin: 0px 4px 0 64px;
      width: 60px;
      height: 60px;
    }
    @media (max-width: 600px) {
      margin: 0px 4px 0 46px;
      width: 42px;
      height: 42px;
    }
    @media (max-width: 490px) {
      margin: 0px 0px 0 44px;
      width: 36px;
      height: 36px;
    }
    @media (max-width: 320px) {
      margin: 0px 0px 0 38px;
      width: 32px;
      height: 32px;
    }
  `,
  BonusHighlight: styled.div`
    position: absolute;
    top: 10px;
    left: 20px;
    opacity: 0.9;
    filter: blur(4px);
    background: white;
    width: 75%;
    padding: 2px;
    /* max-width: */
    z-index: 5;
    @media (max-width: 820px) {
      width: 20%;
      left: 50%;
      top: 12px;
      transform: translateX(-50%);
    }
  `,
  BonusFill: styled.div`
    height: 100%;
    width: 100%;
    border-radius: 64px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-weight: bold;
    color: white;
    text-align: center;
    white-space: nowrap;
    @media (max-width: 820px) {
      font-size: 16px;
    }
  `
};
