import React from 'react';
import styled from 'styled-components';

interface Props {
  children: React.ReactNode;
  className?: string;
}

const StyledContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  padding: 0;
  margin: 0;
  border: 0;
`;

const CenteredContainer = ({ children, className, ...props }: Props) => {
  return (
    <StyledContainer className={className} {...props}>
      {children}
    </StyledContainer>
  );
};

export default CenteredContainer;
