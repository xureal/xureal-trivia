import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  backIcon: {
    padding: 0,
    position: 'absolute',
    top: theme.spacing(8),
    left: theme.spacing(8),
    [theme.breakpoints.down('xs')]: {
      top: '30px',
      left: '13px'
    },
    '&:hover': {
      cursor: 'pointer'
    }
  }
}));

export default useStyles;
