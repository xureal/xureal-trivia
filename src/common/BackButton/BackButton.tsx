import { IconButton } from '@mui/material';
// import { Img } from "Shared";
import backIcon from 'assets/back.png';
import useStyles from './styles';

interface Props {
  back: () => void;
}

const BackButton = ({ back }: Props) => {
  const classes = useStyles();

  return (
    <IconButton className={classes.backIcon} onClick={back}>
      <img src={backIcon} alt='' />
    </IconButton>
  );
};

export default BackButton;
