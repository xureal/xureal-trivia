import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Type } from './Text';

const typeXsMap = {
  info: 16,
  large: 20,
  titleS: 18,
  titleL: 32,
  xLarge: 48,
  normal: 16,
  fluid: 'clamp(14px, 1.5vw, 18px)',
  fluidL: 'clamp(16px, 2vw, 26px)',
  sm: 16,
  xs: 14
};

const useStyles = (type: Type, xs?: number) =>
  makeStyles((theme: Theme) => ({
    root: {
      color: 'white',
      [theme.breakpoints.down('xs')]: {
        fontSize:
          type === 'fluid' || type === 'fluidL'
            ? `${typeXsMap['fluid']}!important`
            : `${xs ? xs : typeXsMap[type]}px!important`
      }
    }
  }));

export default useStyles;
