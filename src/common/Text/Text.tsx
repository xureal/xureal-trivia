import { Typography } from '@mui/material';
import useStyles from './styles';
import cl from 'classnames';
import { Weight } from './types';
// import {Property.TextTransform} from "csstype";

export type Type = 'info' | 'normal' | 'sm' | 'xs' | 'large' | 'xLarge' | 'fluid' | 'fluidL' | 'titleS' | 'titleL';

type TextProps = {
  className?: string;
  weight?: Weight;
  type?: Type;
  component?: React.ElementType;
  color?: 'dark' | 'light' | 'grey';
  size?: number;
  textTransform?: any;
  center?: boolean;
  lineHeight?: number;
  sm?: number;
  xs?: number;
  children?: React.ReactNode;
  style?: any;
};

const typeMap = {
  info: 16,
  titleS: 18,
  titleL: 30,
  large: 24,
  xLarge: 48,
  normal: 14,
  fluid: 'clamp(12px, 1.5vw, 18px)',
  fluidL: 'clamp(14px, 2vw, 22px)',
  sm: 16,
  xs: 14
};

const colorMap = {
  dark: 'black',
  light: 'white',
  grey: '#8B8B97'
};

const Text: React.FC<TextProps> = (props) => {
  const {
    children,
    className,
    type = 'normal',
    weight = 'normal',
    component = 'span',
    color = 'light',
    size,
    textTransform,
    center,
    xs,
    lineHeight,
    style
  } = props;

  const classes = useStyles(type, xs)();

  return (
    <Typography
      component={component}
      sx={{
        fontSize: size || typeMap[type],
        fontWeight: weight,
        color: colorMap[color],
        textAlign: center ? 'center' : undefined,
        textTransform,
        lineHeight: `${lineHeight}px`,
        ...style
      }}
      className={cl(classes.root, className)}
    >
      {children}
    </Typography>
  );
};

export default Text;
