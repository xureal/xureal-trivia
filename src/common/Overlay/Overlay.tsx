import useStyles from './styles';

const Overlay: React.FC<{ children?: React.ReactNode }> = ({ children }) => {
  const classes = useStyles();

  return <div className={classes.root}>{children}</div>;
};

export default Overlay;
