import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    position: 'fixed',
    width: '100%',
    height: '100vh',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: 'grey',
    opacity: 0.5
  }
}));

export default useStyles;
