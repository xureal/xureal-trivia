type ConditionalProps = {
  cond: any;
  onFalse?: JSX.Element;
  children: React.ReactNode;
};

const Conditional = (props: ConditionalProps) => {
  const { children, cond, onFalse } = props;

  if (cond) {
    return children;
  }

  if (onFalse) {
    return onFalse;
  }

  return null;
};

export default Conditional;
