import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    background: 'black',
    position: 'absolute',
    top: theme.spacing(4),
    left: theme.spacing(2),
    maxWidth: theme.spacing(15),
    borderRadius: theme.spacing(4),
    padding: theme.spacing(1)
  },
  img: {
    marginRight: theme.spacing(1)
  }
}));

export default useStyles;
