import { Grid } from '@mui/material';
import useStyles from './styles';
import { Text } from 'common';
import tourMilesIcon from 'assets/tourmiles.png';
import useSelector from 'store/useSelector';
import { getTourMiles } from 'store/selectors';
import { Img } from 'common/Image';

const TourMiles: React.FC = () => {
  const classes = useStyles();
  const miles = useSelector(getTourMiles);

  return (
    <Grid
      container
      className={classes.root}
      wrap='nowrap'
      direction={'row'}
      justifyContent={'center'}
      alignItems={'center'}
    >
      <Grid item className={classes.img}>
        <Img src={tourMilesIcon} width={40} height={25} />
      </Grid>
      <Grid item>
        <Text type={'info'} weight={'bold'}>
          {miles}
        </Text>
      </Grid>
    </Grid>
  );
};

export default TourMiles;
