import useStyles from './styles';
import cl from 'classnames';

type ImageProps = {
  size?: number;
  src: string;
  Component?: any;
  className?: string;
  width?: number;
  height?: number;
  xs?: number;
  xsWidth?: number;
  xsHeight?: number;
  [key: string]: any;
};

export const Img = ({
  src,
  size,
  Component,
  className,
  width,
  height,
  xs,
  xsWidth,
  xsHeight,
  ...props
}: ImageProps) => {
  const classes = useStyles({ size, width, height, xs, xsWidth, xsHeight })();

  if (!Component) {
    return (
      <div className={cl(classes.imageContainer, className)} {...props}>
        <img src={src} alt='' width='100%' height='auto' />
      </div>
    );
  }

  return (
    <Component className={cl(classes.imageContainer, className)} {...props}>
      <img src={src} alt='' />
    </Component>
  );
};
