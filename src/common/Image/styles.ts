import { makeStyles } from '@mui/styles';

interface StyleProps {
  size?: number;
  width?: number;
  height?: number;
  xsWidth?: number;
  xsHeight?: number;
  xs?: number;
}

const useStyles = ({ size, width, height, xs, xsWidth, xsHeight }: StyleProps) =>
  makeStyles(() => ({
    imageContainer: {
      width: width + 'px' ?? size + 'px' ?? '16px',
      height: height + 'px' ?? size + 'px' ?? '16px'
    }
  }));

export default useStyles;
