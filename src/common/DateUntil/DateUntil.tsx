import React, { useEffect, useState } from 'react';
import { Grid } from '@mui/material';
import { useQuiz } from 'store/useQuiz';
import { date } from 'utils';
import { Text } from 'common';
import TimerIcon from '@mui/icons-material/Timer';
import useStyles from './styles';
import { getMedalFromString } from 'utils/medal';
import { setWait } from 'store/actions';

interface DateUntilProps {
  goBack: () => void;
}

const DateUntil: React.FC<DateUntilProps> = ({ goBack }) => {
  const classes = useStyles();
  const [expirationTimer, setExpirationTimer] = useState(
    new Date(
      new Date(new Date().setHours(27, 0, 0, 0)).toLocaleString('en-US', {
        timeZone: 'America/New_York'
      })
    ).getTime() + ''
  );
  const [countdownTimer, setCountdownTimer] = useState('');
  const { quizContainer, state, dispatch } = useQuiz();
  const repo = quizContainer.getRepo();
  const medal = getMedalFromString(state.medals);

  useEffect(() => {
    if (state.timestamp === '') return;
    Promise.resolve()
      .then(async () => {
        const midnightFollowingDay = new Date(state.timestamp);
        midnightFollowingDay.setDate(midnightFollowingDay.getDate() + 1);
        midnightFollowingDay.setHours(0, 0, 0, 0);
        const timeUntil = midnightFollowingDay.valueOf().toString();
        setExpirationTimer(timeUntil);
      })
      .catch(console.error);
  }, [repo, state.timestamp]);

  useEffect(() => {
    const timer = setTimeout(() => {
      if (expirationTimer !== '' && expirationTimer !== '0') {
        setCountdownTimer(date.returnExpirationTime(parseInt(expirationTimer)));
        if (date.returnExpirationTime(parseInt(expirationTimer)) === '0') {
          dispatch(setWait(false));
          goBack();
        }
      }
    }, 1000);
    return () => clearTimeout(timer);
  });

  return (
    <Grid container justifyContent={'center'} alignItems={'center'} spacing={1} direction={'row'}>
      {!(medal === 'Gold') && countdownTimer !== '' && (
        <Grid
          item
          container
          direction={'row'}
          justifyContent={'center'}
          alignItems={'center'}
          spacing={1}
          className='fade-in'
        >
          <Grid>
            <Text type={'large'}>Next Game in:</Text>
          </Grid>
          <Grid container item justifyContent={'center'} alignItems={'center'} style={{ maxWidth: '64px' }}>
            <TimerIcon className={classes.timerIcon} />
          </Grid>
          <Grid item>
            <Text weight={'bold'} size={60} xs={30}>
              {countdownTimer}
            </Text>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

export default DateUntil;
