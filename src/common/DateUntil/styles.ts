import { makeStyles } from '@mui/styles';
import { Theme } from '@mui/material';

const useStyles = makeStyles((theme: Theme) => ({
  timerIcon: {
    fontSize: 56,
    color: 'white',
    [theme.breakpoints.down('xs')]: {
      fontSize: 40
    }
  },
  nextGame: {
    textAlign: 'center',
    [theme.breakpoints.down('xs')]: {
      paddingBottom: `${theme.spacing(0)}px!important`
    }
  }
}));

export default useStyles;
