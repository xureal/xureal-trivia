import * as types from './types';
import { Question, Quiz, Topic } from '../core/types';

export const setQuiz = (quiz: Quiz) =>
  ({
    type: types.SET_QUIZ,
    payload: quiz
  }) as const;

export const setCurrentMedals = (medals: string) =>
  ({
    type: types.SET_CURRENT_MEDALS,
    payload: medals
  }) as const;

export const setCurrentTopic = (topic: Topic) =>
  ({
    type: types.SET_CURRENT_TOPIC,
    payload: topic
  }) as const;

export const setTopicFinished = (topicId: string, finished: boolean) =>
  ({
    type: types.SET_TOPIC_FINISHED,
    payload: { topicId, finished }
  }) as const;

export const setTopics = (topics: Topic[]) =>
  ({
    type: types.SET_TOPICS,
    payload: topics
  }) as const;

export const setTimestamp = (newTimestamp: string) =>
  ({
    type: types.SET_TIMESTAMP,
    payload: newTimestamp
  }) as const;

export const addTourMiles = (miles: number) =>
  ({
    type: types.ADD_TOUR_MILES,
    payload: miles
  }) as const;

export const setWait = (wait: boolean) =>
  ({
    type: types.SET_WAIT,
    payload: { wait }
  }) as const;

export const setProgressLevel = (correct: number) =>
  ({
    type: types.SET_PROGRESS_LEVEL,
    payload: correct
  }) as const;

export const setInitialQuestionStructure = (initProgress: { topicID: string; progress: string[] }[]) =>
  ({
    type: types.SET_INITIAL_QUESTION_STRUCTURE,
    payload: initProgress
  }) as const;

export const setAnswerCalculated = (isAnswered: boolean, isCorrect: boolean) =>
  ({
    type: types.SET_ANSWER_CALCULATED,
    payload: {
      answerCalculated: isAnswered,
      answerCorrect: isCorrect
    }
  }) as const;

export const setAnsweredQuestions = (newQuestionArray: { topicID: string; progress: string[] }[]) => {
  return {
    type: types.SET_ANSWERED_QUESTIONS,
    payload: newQuestionArray
  } as const;
};

export const setCurrentQuestion = (newCurrQuestion: Question) =>
  ({
    type: types.SET_CURRENT_QUESTION,
    payload: newCurrQuestion
  }) as const;

export type Action =
  | ReturnType<typeof setQuiz>
  | ReturnType<typeof setCurrentTopic>
  | ReturnType<typeof setTopicFinished>
  | ReturnType<typeof setTopics>
  | ReturnType<typeof addTourMiles>
  | ReturnType<typeof setWait>
  | ReturnType<typeof setProgressLevel>
  | ReturnType<typeof setCurrentMedals>
  | ReturnType<typeof setInitialQuestionStructure>
  | ReturnType<typeof setAnswerCalculated>
  | ReturnType<typeof setCurrentQuestion>
  | ReturnType<typeof setAnsweredQuestions>
  | ReturnType<typeof setTimestamp>;
