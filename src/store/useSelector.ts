import { useQuiz } from './useQuiz';
import { State } from '../core/types';

export type useSelectorT = <TSelected = unknown>(selector: (state: State) => TSelected) => TSelected;

const useSelector: useSelectorT = (selector) => {
  const { state } = useQuiz();
  return selector(state);
};

export default useSelector;
