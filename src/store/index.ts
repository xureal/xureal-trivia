import * as types from './types';
import { Action } from './actions';
import { initialQuizData } from 'core/initialQuizData';

const quizReducer = (state = initialQuizData, action: Action) => {
  switch (action.type) {
    case types.SET_QUIZ:
      return {
        ...state,
        ...action.payload
      };
    case types.SET_CURRENT_TOPIC:
      return {
        ...state,
        currentTopic: { ...action.payload }
      };
    case types.SET_CURRENT_MEDALS:
      return {
        ...state,
        medals: action.payload
      };
    case types.SET_TOPIC_FINISHED:
      const { topicId, finished } = action.payload;
      return {
        ...state,
        topics: state.topics.map((topic) => ({
          ...topic,
          finished: topicId === topic.id ? finished : topic.finished
        }))
      };
    case types.SET_TOPICS:
      return {
        ...state,
        topics: action.payload
      };
    case types.ADD_TOUR_MILES:
      return {
        ...state,
        miles: state.miles + action.payload
      };
    case types.SET_WAIT:
      return {
        ...state,
        wait: action.payload.wait
      };
    case types.SET_PROGRESS_LEVEL:
      return {
        ...state,
        progress: action.payload
      };
    case types.SET_ANSWERED_QUESTIONS:
      return {
        ...state,
        answeredQuestions: action.payload
      };
    case types.SET_TIMESTAMP:
      return {
        ...state,
        timestamp: action.payload
      };
    case types.SET_INITIAL_QUESTION_STRUCTURE:
      return {
        ...state,
        answeredQuestions: action.payload
      };
    case types.SET_CURRENT_QUESTION:
      return {
        ...state,
        currentQuestion: action.payload
      };
    case types.SET_ANSWER_CALCULATED:
      return {
        ...state,
        answerCalculated: action.payload.answerCalculated,
        answerCorrect: action.payload.answerCorrect
      };
    default:
      return state;
  }
};

export default quizReducer;
