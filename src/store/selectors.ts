import { State } from '../core/types';

export const getTopics = (state: State) => {
  return state.topics;
};

export const getCurrentTopic = (state: State) => {
  return state.currentTopic;
};

export const getCurrentTopicColor = (state: State) => {
  return state.currentTopic;
};

export const getWait = (state: State) => {
  return state.wait;
};

export const getTourMiles = (state: State) => {
  return state.miles;
};

export const getCorrectList = (state: State) => {
  return state.progress;
};
