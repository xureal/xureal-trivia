import { createContext, useContext } from 'react';
import { State, Topic } from '../core/types';
import { Action } from './actions';
import QuizContainer from 'core/QuizContainer';

interface QuizData {
  state: State;
  quizContainer: QuizContainer;
  dispatch: (data: Action) => void;
  getNewQuestion: (topic: Topic) => void;
}

export const QuizCtx = createContext({});

export const useQuiz = () => {
  return useContext(QuizCtx) as QuizData;
};
